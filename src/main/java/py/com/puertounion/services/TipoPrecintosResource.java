/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.Productos;
import py.com.puertounion.pojos.Tipos_precinto;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author monica
 */
@Path("gettipoprecintos")
public class TipoPrecintosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TipoPrecintosResource
     */
    public TipoPrecintosResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.TipoPrecintosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        Genson gen = new Genson();
        String sql = "select  codigo, descripcion, comentarios, identificador from tipos_precinto ";
        List<String[]> lista = DBTrasactions.retList(sql);
        List<Tipos_precinto> listaProductos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            Tipos_precinto p = new Tipos_precinto();
            p.setCodigo(get[0]);
            p.setDescripcion(get[1]);
            p.setComentarios(get[2]);
            p.setIdentificador((int)Double.parseDouble(get[3]));
            listaProductos.add(p);
        }
        return gen.serialize(listaProductos);
    }

    /**
     * PUT method for updating or creating an instance of TipoPrecintosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
