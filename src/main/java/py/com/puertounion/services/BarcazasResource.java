/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.Barcazas;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author monica
 */
@Path("getbarcazas")
public class BarcazasResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProductosResource
     */
    public BarcazasResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.ProductosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "select distinct a.nro_chapa, coalesce(a.id_bandera,0), a.identificador, 0 as peso from barcazas a, movimientos_cab b "+
                     " where a.identificador = b.id_barcaza ";
        List<String[]> lista = DBTrasactions.retList(sql);
        List<Barcazas> listaBarcaza = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            Barcazas p = new Barcazas();
            p.setNro_chapa(get[0]);
            //p.setCapacidad((int)Double.parseDouble(get[1]));
            p.setId_bandera((int)Integer.parseInt(get[1]));
            p.setIdentificador((int)Integer.parseInt(get[2]));
            p.setPeso((long)Long.parseLong(get[3]));
            listaBarcaza.add(p);
        }
        

        return  gen.serialize(listaBarcaza);
    }

    /**
     * PUT method for updating or creating an instance of ProductosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
