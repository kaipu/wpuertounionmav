/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.google.gson.Gson;
import com.owlike.genson.Genson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.beans.WebBean;
import py.com.puertounion.pojos.ActividadesBarcaza;
import py.com.puertounion.pojos.RelatoriosEmbarqueCab;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("nuevasactividades")
public class NuevasActividadesResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of NuevasActividadesResource
     */
    public NuevasActividadesResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.NuevasActividadesResource
     * @return an instance of java.lang.String
     */
   
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String putJson(String content) {
         Genson gen = new Genson();
        Gson   gson = new Gson();
        try {
            System.out.println("Actividades content:"+content);
            
            if(content.length()<3){
                return gen.serialize("failed!");
            }
            content = java.net.URLDecoder.decode(content);
            ActividadesBarcaza[] relatorios = gson.fromJson(content, ActividadesBarcaza[].class);

            for (int i = 0; i < relatorios.length; i++) {
                ActividadesBarcaza relat = relatorios[i];
                int error = WebBean.insertActividades(relat);
                
            }
            return gen.serialize("ok");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
