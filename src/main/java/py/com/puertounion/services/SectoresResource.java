/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.Barcazas;
import py.com.puertounion.pojos.SectoresBarcaza;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("getsectores")
public class SectoresResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of SectoresResource
     */
    public SectoresResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.SectoresResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        Genson gen = new Genson();
        String sql = "select identificador, descripcion, comentarios, codigo from sectores_barcaza ";
        List<String[]> lista = DBTrasactions.retList(sql);
        List<SectoresBarcaza> listaBarcaza = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            SectoresBarcaza p = new SectoresBarcaza();
            p.setIdentificador((int)Double.parseDouble(get[0]));
            p.setDescripcion(get[1]);
            p.setComentarios(get[2]);
            p.setCodigo(get[3]);
            
            listaBarcaza.add(p);
        }
        

        return  gen.serialize(listaBarcaza);
    }

    /**
     * PUT method for updating or creating an instance of SectoresResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
