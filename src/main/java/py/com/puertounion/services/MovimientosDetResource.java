/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.MovimientosDet;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author monica
 */

@Path("getmovimientosdet")
public class MovimientosDetResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProductosResource
     */
    public MovimientosDetResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.ProductosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "select id_movimiento_cab, nro_linea, coalesce(cantidad,0), coalesce(id_precinto,0)"
                + ", comentarios, coalesce(id_tipo_precinto,0), identificador "
                + " from movimientos_det where id_movimiento_cab in (select identificador from movimientos_cab where estado = 'AS' )";
        List<String[]> lista = DBTrasactions.retList(sql);
        List<MovimientosDet> listaMovimientos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            MovimientosDet p = new MovimientosDet();
            p.setId_movimiento_cab(Integer.parseInt(get[0]));
            p.setNro_linea(Integer.parseInt(get[1]));
            p.setCantidad(Double.parseDouble(get[2]));
            p.setId_precinto(Integer.parseInt(get[3]));
            p.setComentarios(get[4]);
            p.setId_tipo_precinto(Integer.parseInt(get[5]));
            p.setIdentificador(Integer.parseInt(get[6]));
            listaMovimientos.add(p);
        }
        return gen.serialize(listaMovimientos);
    }

    /**
     * PUT method for updating or creating an instance of ProductosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
