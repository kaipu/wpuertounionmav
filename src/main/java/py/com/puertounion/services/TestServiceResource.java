/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("testservice")
public class TestServiceResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TestServiceResource
     */
    public TestServiceResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.TestServiceResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "select  distinct 1  from tipos_precinto ";
        List<String[]> lista = DBTrasactions.retList(sql);
        
        if(lista!=null && lista.size()>0)
            return gen.serialize("ok");
        else
            return gen.serialize("fail");
        
    }

    /**
     * PUT method for updating or creating an instance of TestServiceResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
