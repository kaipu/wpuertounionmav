/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.MovimientosCab;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author monica
 */

@Path("getmovimientoscab")
public class MovimientosCabResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProductosResource
     */
    public MovimientosCabResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.ProductosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "select numero, fecha, id_cliente, id_almacen, id_barcaza, destino, "
                + " comentarios, id_operario, coalesce(id_tipo_movimiento,0), coalesce(id_producto,0), coalesce(peso,0), identificador, remolcador  "
                + " from movimientos_cab where estado = 'AS' ";
       
        List<String[]> lista = DBTrasactions.retList(sql);
        List<MovimientosCab> listaMovimientos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            MovimientosCab p = new MovimientosCab();
            p.setNumero(get[0]);
            p.setFecha(get[1]);
            p.setId_cliente((int)Double.parseDouble(get[2]));
            p.setId_almacen((int)Double.parseDouble(get[3]));
            p.setId_barcaza((int)Double.parseDouble(get[4]));
            p.setDestino(get[5]);
            p.setComentarios(get[6]);
            p.setId_operario((int)Double.parseDouble(get[7]));
            p.setId_tipo_movimiento((int)Double.parseDouble(get[8]));
            p.setId_producto((int)Double.parseDouble(get[9]));
            p.setPeso(Double.parseDouble(get[10]));
            p.setIdentificador(Integer.parseInt(get[11]));
            p.setRemolcador((get[12]));
            listaMovimientos.add(p);
        }
        return gen.serialize(listaMovimientos);
    }

    /**
     * PUT method for updating or creating an instance of ProductosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
