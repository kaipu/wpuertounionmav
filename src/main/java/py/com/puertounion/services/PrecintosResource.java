/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.Precintos;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author monica
 */

@Path("getprecintos")
public class PrecintosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProductosResource
     */
    public PrecintosResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.ProductosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "select a.numero, a.codigo_barras, a.observacion, coalesce(a.id_tipo_precinto,0)"
                + ", a.estado, a.identificador from precintos a "
                + " where a.identificador in (select z.id_precinto from movimientos_cab y, movimientos_det z where y.identificador = z.id_movimiento_cab and y.estado = 'AS')";
        List<String[]> lista = DBTrasactions.retList(sql);
        List<Precintos> listaPrecintos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            Precintos p = new Precintos();
            p.setNumero(Double.parseDouble(get[0]));
            p.setCodigo_barras(get[1]);
            p.setObservacion(get[2]);
            p.setId_tipo_precinto(Integer.parseInt(get[3]));
            p.setEstado(get[4]);
            p.setIdentificador(Integer.parseInt(get[5]));
            listaPrecintos.add(p);
        }
        return gen.serialize(listaPrecintos);
    }

    /**
     * PUT method for updating or creating an instance of ProductosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
