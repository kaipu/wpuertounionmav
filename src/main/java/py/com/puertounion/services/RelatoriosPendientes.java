/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.MovimientosCab;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("relatoriospendientes")
public class RelatoriosPendientes {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RelatoriosPendientes
     */
    public RelatoriosPendientes() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.RelatoriosPendientes
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "select identificador, id_barcaza, peso, id_producto, remolcador "
                + " from movimientos_cab where identificador not in (select id_movimiento_cab from relatorios_embarque_cab )  and identificador > 600";
       
        List<String[]> lista = DBTrasactions.retList(sql);
        
        String[] result = new String[lista.size()];
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            result[i]=get[0]+";"+get[1]+";"+get[2]+";"+get[3]+";"+get[4]+"";
        }
        return gen.serialize(result);
    
    }

    /**
     * PUT method for updating or creating an instance of RelatoriosPendientes
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
