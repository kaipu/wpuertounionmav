/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.google.gson.Gson;
import com.owlike.genson.Genson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.beans.WebBean;
import py.com.puertounion.pojos.Precintado;
import py.com.puertounion.pojos.RelatoriosEmbarqueCab;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("relatorio")
public class RelatorioResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RelatorioResource
     */
    public RelatorioResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.RelatorioResource
     * @return an instance of java.lang.String
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String putJson(String content) {
        Genson gen = new Genson();
        Gson   gson = new Gson();
        try {
            System.out.println("Relatorio content:"+content);
            
            if(content.length()<3){
                return gen.serialize("failed!");
            }
            content = java.net.URLDecoder.decode(content);
            RelatoriosEmbarqueCab[] relatorios = gson.fromJson(content, RelatoriosEmbarqueCab[].class);

            for (int i = 0; i < relatorios.length; i++) {
                RelatoriosEmbarqueCab relat = relatorios[i];
                int error = 0;
                error = WebBean.insertRelatorioCab(relat);
                
            }
            return gen.serialize("ok");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
