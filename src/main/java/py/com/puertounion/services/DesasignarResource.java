/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.beans.WebBean;
import py.com.puertounion.pojos.Precintado;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("desasignar")
public class DesasignarResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of DesasignarResource
     */
    public DesasignarResource() {
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String putJson(String content) {
        Genson gen = new Genson();
        
        try {
            if(content.length()<3){
                return gen.serialize("failed!");
            }
            
            System.out.println("content:"+content);
            content = java.net.URLDecoder.decode(content);
            
            //String qry = " delete from  movimientos_det where id_precinto in ("+content.replace('"', ' ')+")  " ;
            //DBTrasactions.modDb(qry);
            
            String qry = " update precintos set estado = 'AC' where identificador in ("+ content.replace('"', ' ')+ " ) and estado = 'AS' " ;
            DBTrasactions.modDb(qry);    
            qry = " delete from  movimientos_det where id_precinto in ("+ content.replace('"', ' ')+ " )  " ;
            DBTrasactions.modDb(qry);    
            
            return gen.serialize("ok");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
