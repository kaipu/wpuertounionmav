/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author user
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(org.glassfish.jersey.server.wadl.internal.WadlResource.class);
        resources.add(py.com.puertounion.services.ActividadesResource.class);
        resources.add(py.com.puertounion.services.BarcazasResource.class);
        resources.add(py.com.puertounion.services.DesasignarResource.class);
        resources.add(py.com.puertounion.services.MovimientosCabResource.class);
        resources.add(py.com.puertounion.services.MovimientosDetResource.class);
        resources.add(py.com.puertounion.services.NuevasActividadesResource.class);
        resources.add(py.com.puertounion.services.OperariosResource.class);
        resources.add(py.com.puertounion.services.PrecintadosResource.class);
        resources.add(py.com.puertounion.services.PrecintosResource.class);
        resources.add(py.com.puertounion.services.ProductosResource.class);
        resources.add(py.com.puertounion.services.RelatorioResource.class);
        resources.add(py.com.puertounion.services.RelatoriosPendientes.class);
        resources.add(py.com.puertounion.services.SectoresResource.class);
        resources.add(py.com.puertounion.services.Test.class);
        resources.add(py.com.puertounion.services.TestServiceResource.class);
        resources.add(py.com.puertounion.services.TipoPrecintosResource.class);
    }
    
}
