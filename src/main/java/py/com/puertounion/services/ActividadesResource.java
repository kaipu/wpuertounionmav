/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.ActividadesBarcaza;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("actividades")
public class ActividadesResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ActividadesResource
     */
    public ActividadesResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.ActividadesResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "SELECT * FROM actividades_barcaza  ";
        List<String[]> lista = DBTrasactions.retList(sql);
        List<ActividadesBarcaza> listaActividadesBarcaza = new ArrayList<ActividadesBarcaza>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            ActividadesBarcaza p = new ActividadesBarcaza();
            p.setIdentificador(Integer.parseInt(get[0]));
            //p.setCapacidad((int)Double.parseDouble(get[1]));
            p.setDescripcion(get[1]);
            p.setComentarios(get[2]);
            listaActividadesBarcaza.add(p);
        }
        

        return  gen.serialize(listaActividadesBarcaza);
    }

    /**
     * PUT method for updating or creating an instance of ActividadesResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
