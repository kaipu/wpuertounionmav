/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.Precintado;
import py.com.puertounion.beans.WebBean;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("precintados")
public class PrecintadosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PrecintadosResource
     */
    public PrecintadosResource() {
    }

    /**
     * Retrieves representation of an instance of
     * py.com.puertounion.services.PrecintadosResource
     *
     * @return an instance of java.lang.String
     */

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String putJson(String content) {
        Genson gen = new Genson();
        
        try {
            System.out.println("content:"+content);
            
            if(content.length()<3){
                return gen.serialize("failed!");
            }
            content = java.net.URLDecoder.decode(content);
            Precintado[] precintados = gen.deserialize(content, Precintado[].class);
            String cabeceras = "-99999";
            String idPrecintos = "-99999";
            for (int i = 0; i < precintados.length; i++) {
                Precintado precintado = precintados[i];
                int error = WebBean.insertPrecintados(precintado);
                System.out.println("precintado " + i + ": " + precintado);
                if(!cabeceras.contains(","+precintado.getId_movimiento_cab()+","))
                    cabeceras+= ","+precintado.getId_movimiento_cab()+"";
                
            }
            String qry = "update movimientos_cab set estado = 'PR' where identificador in ("+cabeceras+")";
            DBTrasactions.modDb(qry);
            
            /*
            qry = " delete from  movimientos_det where id_movimiento_cab in ("+cabeceras+") and id_precinto not in ("+ idPrecintos+ " )  " ;
            DBTrasactions.modDb(qry);
            
            qry = " update precintos set estado = 'AC' where identificador not in ("+ idPrecintos+ " ) and estado = 'AS' " ;
            DBTrasactions.modDb(qry);    
            */
            return gen.serialize("ok");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
