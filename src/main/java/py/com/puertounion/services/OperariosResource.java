/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.Operarios;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author monica
 */

@Path("getoperarios")
public class OperariosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProductosResource
     */
    public OperariosResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.ProductosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@QueryParam("user") String user, 
            @QueryParam("pass") String pass) {
        
        String sql = " SELECT a.identificador, a.id_persona, a.comentarios, a.usuario, a.contrasenha, a.admin " +
                    " FROM operarios a, personas b " +
                    " where a.id_persona = b.identificador " +
                    " and a.usuario = '"+user+"' " +
                    " and a.contrasenha = '"+pass+"' ";
        System.out.println(sql);
        List<String[]> lista = DBTrasactions.retList(sql);
        List<Operarios> listaOperario = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            Operarios p = new Operarios();
            p.setIdentificador((int)Double.parseDouble(get[0]));
            p.setId_persona((int)Double.parseDouble(get[1]));
            p.setComentarios((get[2]));
            p.setUsuario(get[3]);
            p.setContrasenha("");
            
            sql = " SELECT  coalesce(a.admin,0 ) " +
                    "FROM cargadores a " +
                    " where a.usuario = '"+user+"' " ;
            boolean isadmin = DBTrasactions.retBoolean(sql);
            if(isadmin)
                p.setAdmin(1);
            else
                p.setAdmin(0);
            listaOperario.add(p);
        }
        if(listaOperario==null || listaOperario.size()<=0)
            return null;
        else{
             Genson genson = new Genson();

            return genson.serialize(listaOperario);
        }
    }

    /**
     * PUT method for updating or creating an instance of ProductosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
