/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.services;

import com.owlike.genson.Genson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import py.com.puertounion.pojos.Productos;
import py.com.puertounion.utiles.DBTrasactions;

/**
 * REST Web Service
 *
 * @author user
 */
@Path("getproductos")
public class ProductosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProductosResource
     */
    public ProductosResource() {
    }

    /**
     * Retrieves representation of an instance of py.com.puertounion.services.ProductosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        Genson gen = new Genson();
        String sql = "select codigo, descripcion, comentarios, coalesce(id_tipos_producto,0), identificador from productos ";
        List<String[]> lista = DBTrasactions.retList(sql);
        List<Productos> listaProductos = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            String[] get = lista.get(i);
            Productos p = new Productos();
            p.setCodigo(get[0]);
            p.setDescripcion(get[1]);
            p.setComentarios(get[2]);
            
            p.setId_tipos_producto((int)Double.parseDouble(get[3]));
            p.setIdentificador((int)Double.parseDouble(get[4]));
            listaProductos.add(p);
        }
        return gen.serialize(listaProductos);
    }

    /**
     * PUT method for updating or creating an instance of ProductosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
