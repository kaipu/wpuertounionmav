/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

/**
 *
 * @author monica
 */
public class Tipos_precinto{
	private String codigo;
	private String descripcion;
	private String comentarios;
 private int identificador;
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
	public String getCodigo(){
		return codigo;
	}

	public void setCodigo(String codigo){
		this.codigo=codigo;
	}

	public String getDescripcion(){
		return descripcion;
	}

	public void setDescripcion(String descripcion){
		this.descripcion=descripcion;
	}

	public String getComentarios(){
		return comentarios;
	}

	public void setComentarios(String comentarios){
		this.comentarios=comentarios;
	}
}