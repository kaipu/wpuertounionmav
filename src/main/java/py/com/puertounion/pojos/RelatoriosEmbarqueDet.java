/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

public class RelatoriosEmbarqueDet {
    private int identificador;
    private int id_relatorio_cab;
    private java.util.Date fecha;
    private String hora_inicio;
    private String hora_fin;
    private int id_actividad;

    public int getId_relatorio_cab(){
        return id_relatorio_cab;
    }

    public void setId_relatorio_cab(int id_relatorio_cab){
        this.id_relatorio_cab=id_relatorio_cab;
    }

    public java.util.Date getFecha(){
        return fecha;
    }

    public void setFecha(java.util.Date fecha){
        this.fecha=fecha;
    }

    public String getHora_inicio(){
        return hora_inicio;
    }

    public void setHora_inicio(String hora_inicio){
        this.hora_inicio=hora_inicio;
    }

    public String getHora_fin(){
        return hora_fin;
    }

    public void setHora_fin(String hora_fin){
        this.hora_fin=hora_fin;
    }

    public int getId_actividad(){
        return id_actividad;
    }

    public void setId_actividad(int id_actividad){
        this.id_actividad=id_actividad;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
}
