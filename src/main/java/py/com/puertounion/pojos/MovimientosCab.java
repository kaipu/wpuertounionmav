/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

/**
 *
 * @author user
 */
public class MovimientosCab{
	private String numero;
	private String fecha;
	private int id_cliente;
	private int id_almacen;
	private int id_barcaza;
	private String destino;
	private String comentarios;
	private int id_operario;
	private int id_tipo_movimiento;
	private int id_producto;
	private double peso;
        
        
        
    private int identificador;
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
	public String getNumero(){
		return numero;
	}

	public void setNumero(String numero){
		this.numero=numero;
	}

	public String getFecha(){
		return fecha;
	}

	public void setFecha(String fecha){
		this.fecha=fecha;
	}

	public int getId_cliente(){
		return id_cliente;
	}

	public void setId_cliente(int id_cliente){
		this.id_cliente=id_cliente;
	}

	public int getId_almacen(){
		return id_almacen;
	}

	public void setId_almacen(int id_almacen){
		this.id_almacen=id_almacen;
	}

	public int getId_barcaza(){
		return id_barcaza;
	}

	public void setId_barcaza(int id_barcaza){
		this.id_barcaza=id_barcaza;
	}

	public String getDestino(){
		return destino;
	}

	public void setDestino(String destino){
		this.destino=destino;
	}

	public String getComentarios(){
		return comentarios;
	}

	public void setComentarios(String comentarios){
		this.comentarios=comentarios;
	}

	public int getId_operario(){
		return id_operario;
	}

	public void setId_operario(int id_operario){
		this.id_operario=id_operario;
	}

	public int getId_tipo_movimiento(){
		return id_tipo_movimiento;
	}

	public void setId_tipo_movimiento(int id_tipo_movimiento){
		this.id_tipo_movimiento=id_tipo_movimiento;
	}

	public int getId_producto(){
		return id_producto;
	}

	public void setId_producto(int id_producto){
		this.id_producto=id_producto;
	}

	public double getPeso(){
		return peso;
	}

	public void setPeso(double peso){
		this.peso=peso;
	}

	private String remolcador;
    public String getRemolcador() {
        return remolcador;
    }

    public void setRemolcador(String remolcador) {
        this.remolcador = remolcador;
    }
}