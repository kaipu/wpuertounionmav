/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

/**
 *
 * @author user
 */
public class  Barcazas{
	private String nro_chapa;
	private int id_bandera;
	private String remolcador;

	public String getRemolcador() {
		return remolcador;
	}

	public void setRemolcador(String remolcador) {
		this.remolcador = remolcador;
	}

	public String getNro_chapa(){
		return nro_chapa;
	}

	public void setNro_chapa(String nro_chapa){
		this.nro_chapa=nro_chapa;
	}

	public int getId_bandera(){
		return id_bandera;
	}

	public void setId_bandera(int id_bandera){
		this.id_bandera=id_bandera;
	}
            private int identificador;
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    
    	private long peso;
	public long getPeso() {
		return peso;
	}

	public void setPeso(long peso) {
		this.peso = peso;
	}
        
	private int id_producto;

	public int getId_producto() {
		return id_producto;
	}

	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}


}