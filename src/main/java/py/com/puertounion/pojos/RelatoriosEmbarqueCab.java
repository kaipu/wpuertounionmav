/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

import java.util.Date;
import java.util.List;

public class RelatoriosEmbarqueCab {
   private int identificador;
    private int id_puerto;
    private String nombre_remolcador;
    private java.util.Date fecha_arribo;
    private java.util.Date fecha_inicio;
    private java.util.Date fecha_fin;
    private int id_producto;
    private int id_barcaza;
    private double cantidad;

        private int id_movimiento_cab;
    private long peso;

    public int getId_movimiento_cab() {
        return id_movimiento_cab;
    }

    public void setId_movimiento_cab(int id_movimiento_cab) {
        this.id_movimiento_cab = id_movimiento_cab;
    }

    public long getPeso() {
        return peso;
    }

    public void setPeso(long peso) {
        this.peso = peso;
    }
    
    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    private List<RelatoriosEmbarqueDet> relatoriosEmbarqueDet;

    public int getIdentificador(){
        return identificador;
    }

    public void setIdentificador(int identificador){
        this.identificador=identificador;
    }

    public int getId_puerto(){
        return id_puerto;
    }

    public void setId_puerto(int id_puerto){
        this.id_puerto=id_puerto;
    }

    public String getNombre_remolcador(){
        return nombre_remolcador;
    }

    public void setNombre_remolcador(String nombre_remolcador){
        this.nombre_remolcador=nombre_remolcador;
    }

    public java.util.Date getFecha_arribo(){
        return fecha_arribo;
    }

    public void setFecha_arribo(java.util.Date fecha_arribo){
        this.fecha_arribo=fecha_arribo;
    }

    public java.util.Date getFecha_inicio(){
        return fecha_inicio;
    }

    public void setFecha_inicio(java.util.Date fecha_inicio){
        this.fecha_inicio=fecha_inicio;
    }

    public int getId_producto(){
        return id_producto;
    }

    public void setId_producto(int id_producto){
        this.id_producto=id_producto;
    }

    public int getId_barcaza(){
        return id_barcaza;
    }

    public void setId_barcaza(int id_barcaza){
        this.id_barcaza=id_barcaza;
    }

    public double getCantidad(){
        return cantidad;
    }

    public void setCantidad(double cantidad){
        this.cantidad=cantidad;
    }


    public List<RelatoriosEmbarqueDet> getRelatoriosEmbarqueDet() {
        return relatoriosEmbarqueDet;
    }

    public void setRelatoriosEmbarqueDet(List<RelatoriosEmbarqueDet> relatoriosEmbarqueDet) {
        this.relatoriosEmbarqueDet = relatoriosEmbarqueDet;
    }
}
