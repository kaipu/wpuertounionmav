/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

/**
 *
 * @author user
 */
public class  Precintado{
  private int id_movimiento_cab;
    private int id_precinto;
    private int id_tipo_precinto;
    private int nro_tapa;
    private int nro_tapita;
    private String codigo_barras;
    private int id_sector_barcaza;
    private String usuario;
    private int identificador;


    public int getIdentificador() {
        return identificador;
    }
    public void setIdentificador(int identificador) {
        this.identificador= identificador;
    }
    public int getId_movimiento_cab(){
        return id_movimiento_cab;
    }

    public void setId_movimiento_cab(int id_movimiento_cab){
        this.id_movimiento_cab=id_movimiento_cab;
    }

    public int getId_precinto(){
        return id_precinto;
    }

    public void setId_precinto(int id_precinto){
        this.id_precinto=id_precinto;
    }

    public int getId_tipo_precinto(){
        return id_tipo_precinto;
    }

    public void setId_tipo_precinto(int id_tipo_precinto){
        this.id_tipo_precinto=id_tipo_precinto;
    }

    public String getCodigo_barras(){
        return codigo_barras;
    }

    public void setCodigo_barras(String codigo_barras){
        this.codigo_barras=codigo_barras;
    }

    public int getId_sector_barcaza(){
        return id_sector_barcaza;
    }

    public void setId_sector_barcaza(int id_sector_barcaza){
        this.id_sector_barcaza=id_sector_barcaza;
    }

    public String getUsuario(){
        return usuario;
    }

    public void setUsuario(String usuario){
        this.usuario=usuario;
    }

    @Override
    public String toString() {
        return "Precintado{" + "id_movimiento_cab=" + id_movimiento_cab + ", id_precinto=" + id_precinto + ", id_tipo_precinto=" + id_tipo_precinto + ", codigo_barras=" + codigo_barras + ", id_sector_barcaza=" + id_sector_barcaza + ", usuario=" + usuario + ", identificador=" + identificador + '}';
    }

    public int getNro_tapa() {
        return nro_tapa;
    }

    public void setNro_tapa(int nro_tapa) {
        this.nro_tapa = nro_tapa;
    }

    public int getNro_tapita() {
        return nro_tapita;
    }

    public void setNro_tapita(int nro_tapita) {
        this.nro_tapita = nro_tapita;
    }

}