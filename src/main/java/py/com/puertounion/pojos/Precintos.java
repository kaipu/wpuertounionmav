/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

/**
 *
 * @author user
 */
public class Precintos {
    
	private double numero;
	private String codigo_barras;
	private String observacion;
	private int id_tipo_precinto;
	private String estado;
    private int identificador;
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
	public double getNumero(){
		return numero;
	}

	public void setNumero(double numero){
		this.numero=numero;
	}

	public String getCodigo_barras(){
		return codigo_barras;
	}

	public void setCodigo_barras(String codigo_barras){
		this.codigo_barras=codigo_barras;
	}

	public String getObservacion(){
		return observacion;
	}

	public void setObservacion(String observacion){
		this.observacion=observacion;
	}

	public int getId_tipo_precinto(){
		return id_tipo_precinto;
	}

	public void setId_tipo_precinto(int id_tipo_precinto){
		this.id_tipo_precinto=id_tipo_precinto;
	}

	public String getEstado(){
		return estado;
	}

	public void setEstado(String estado){
		this.estado=estado;
	}

    
}
