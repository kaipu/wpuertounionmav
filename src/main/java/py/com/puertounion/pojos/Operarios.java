/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

/**
 *
 * @author user
 */
public class Operarios{
	private int id_persona;
	private int admin;
	private String comentarios;
	private String usuario;
	private String contrasenha;

	public int getId_persona(){
		return id_persona;
	}

	public void setId_persona(int id_persona){
		this.id_persona=id_persona;
	}

	public String getComentarios(){
		return comentarios;
	}

	public void setComentarios(String comentarios){
		this.comentarios=comentarios;
	}

	public String getUsuario(){
		return usuario;
	}

	public void setUsuario(String usuario){
		this.usuario=usuario;
	}

	public String getContrasenha(){
		return contrasenha;
	}

	public void setContrasenha(String contrasenha){
		this.contrasenha=contrasenha;
	}

    private int identificador;
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }
}