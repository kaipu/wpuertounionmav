/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.pojos;

/**
 *
 * @author user
 */
public class MovimientosDet {
    
	private int id_movimiento_cab;
	private int nro_linea;
	private double cantidad;
	private int id_precinto;
	private String comentarios;
	private int id_tipo_precinto;
    private int identificador;
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
	public int getId_movimiento_cab(){
		return id_movimiento_cab;
	}

	public void setId_movimiento_cab(int id_movimiento_cab){
		this.id_movimiento_cab=id_movimiento_cab;
	}

	public int getNro_linea(){
		return nro_linea;
	}

	public void setNro_linea(int nro_linea){
		this.nro_linea=nro_linea;
	}

	public double getCantidad(){
		return cantidad;
	}

	public void setCantidad(double cantidad){
		this.cantidad=cantidad;
	}

	public int getId_precinto(){
		return id_precinto;
	}

	public void setId_precinto(int id_precinto){
		this.id_precinto=id_precinto;
	}

	public String getComentarios(){
		return comentarios;
	}

	public void setComentarios(String comentarios){
		this.comentarios=comentarios;
	}

	public int getId_tipo_precinto(){
		return id_tipo_precinto;
	}

	public void setId_tipo_precinto(int id_tipo_precinto){
		this.id_tipo_precinto=id_tipo_precinto;
	}
    
}
