/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.beans;

import java.text.SimpleDateFormat;
import java.util.List;
import py.com.puertounion.pojos.ActividadesBarcaza;
import py.com.puertounion.pojos.Precintado;
import py.com.puertounion.pojos.Precintos;
import py.com.puertounion.pojos.RelatoriosEmbarqueCab;
import py.com.puertounion.pojos.RelatoriosEmbarqueDet;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author user
 */
public class WebBean {

    public static boolean validadrUser(String user, String pass) {
        return esUsuario(user, pass);
    }

    public static boolean insert(Object object) {
        return true;
    }

    public static int insertPrecintados(Precintado precintado) {
        String query = "delete from precintado where id_precinto = " + precintado.getId_precinto()
                + " and id_movimiento_cab = " + precintado.getId_movimiento_cab();

        int cantidad = DBTrasactions.modDb(query);

        query = String.format("insert into precintado ( "
                + " id_movimiento_cab, id_precinto,id_tipo_precinto, "
                + "codigo_barras, id_sector_barcaza,usuario, nro_tapa, nro_tapita ) "
                + "values ( "
                + precintado.getId_movimiento_cab() + ","
                + precintado.getId_precinto() + ","
                + precintado.getId_tipo_precinto() + ",'"
                + precintado.getCodigo_barras() + "',"
                + precintado.getId_sector_barcaza() + ",'"
                + precintado.getUsuario() + "',"
                + precintado.getNro_tapa() + ","
                + precintado.getNro_tapita()
                + ")");
        cantidad = DBTrasactions.modDb(query);

        return cantidad;
    }

    private static boolean esUsuario(String usuario, String password) {
        if (usuario == null || password == null || usuario.length() <= 0 || password.length() <= 0) {
            return false;
        }
        String sql = String.format("select * from cargadores "
                + "where UPPER(usuario) =UPPER('" + usuario.trim() + "') and"
                + " UPPER(contrasenha)=UPPER('" + password.trim() + "')",
                usuario, password);
        return DBTrasactions.retBoolean(sql);
    }

    public static int insertRelatorioCab(RelatoriosEmbarqueCab relat) {

        String fechaArribo = "null";

        if (relat.getFecha_arribo() != null) {
            fechaArribo = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(relat.getFecha_arribo()) + "' ";
        }

        String fechaInicio = "null";
        if (relat.getFecha_inicio() != null) {
            fechaInicio = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(relat.getFecha_inicio()) + "' ";
        }

        String fechaFin = "null";
        if (relat.getFecha_fin() != null) {
            fechaFin = " '" + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(relat.getFecha_fin()) + "' ";
        }

        String query = "delete from relatorios_embarque_cab where id_movimiento_cab = " + relat.getId_movimiento_cab() + " ";

        int cantidad = DBTrasactions.modDb(query);

        query = String.format("insert into relatorios_embarque_cab ( "
                + "  nombre_remolcador, fecha_arribo,  "
                + " fecha_inicio, id_barcaza, fecha_fin, peso,id_movimiento_cab, id_producto   ) "
                + "values ( '" + relat.getNombre_remolcador() + "', " + fechaArribo + ", "
                + fechaInicio + ", " + relat.getId_barcaza() + ", " + fechaFin + ", "
                + relat.getPeso() + ", " + relat.getId_movimiento_cab() + ", " + relat.getId_producto()
                + ")");
        cantidad = DBTrasactions.modDb(query);

        query = String.format("select max(identificador) from relatorios_embarque_cab ");
        int id = DBTrasactions.retInt(query);

        List<RelatoriosEmbarqueDet> det = relat.getRelatoriosEmbarqueDet();
        if (det != null && det.size() > 0) {
            for (int i = 0; i < det.size(); i++) {
                RelatoriosEmbarqueDet get = det.get(i);
                String fecha = new SimpleDateFormat("yyyy-MM-dd").format(get.getFecha());
                //String hora = new SimpleDateFormat("HH:mm").format(get.getHora_inicio());

                int idActividad = get.getId_actividad();
                if(get.getId_actividad()<=0){
                    String q = " select identificador from actividades_barcaza where comentarios like '"+get.getId_actividad()+ "'";
                    idActividad = DBTrasactions.retInt(q);
                }    
                query = String.format("insert into relatorios_embarque_det ( id_relatorio_cab, fecha, hora_inicio, hora_fin, id_actividad  ) "
                        + "values ( " + id + ", '" + fecha + "', '" + get.getHora_inicio() + "', '" + get.getHora_fin() + "', " + idActividad + ")");
                cantidad = DBTrasactions.modDb(query);
            }
        }
        
        query = String.format("update movimientos_cab set remolcador = '"+relat.getNombre_remolcador()+"' "
        + " where identificador = "+ relat.getId_movimiento_cab());
        cantidad = DBTrasactions.modDb(query);

        return cantidad;
    }
    
    public static int insertActividades(ActividadesBarcaza actividadd) {
        String query = String.format("INSERT INTO actividades_barcaza (descripcion, comentarios) " +
        " VALUES ('"+ actividadd.getDescripcion()+"', '"+actividadd.getComentarios()+"') ") ;
        int cantidad = DBTrasactions.modDb(query);

        return cantidad;
    }


}
