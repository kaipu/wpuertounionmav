/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;
import py.com.puertounion.utiles.Retorno;

/**
 *
 * @author user
 */
@WebServlet(name = "deleteData", urlPatterns = {"/deleteData"})
public class deleteData extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String id = request.getParameter("id");
            String clase = request.getParameter("class");
            String estado = request.getParameter("estado");
            
            try {
                if(clase.contains("precintos")){
                    String query = "insert into precintos_eliminados (identificador, numero,codigo_barras, observacion, id_tipo_precinto, estado,usuario, fecha) \n" + 
                                    "        select identificador, numero,codigo_barras, observacion, id_tipo_precinto, estado , '"+request.getSession().getAttribute("userlogon")+"' , CURRENT_TIMESTAMP   from precintos where identificador = "+id;
                    int cant = DBTrasactions.modDb(query);
                    
                }
                
                
                
                
                String query = "delete from "+clase+" where identificador = "+id;
                int cant = DBTrasactions.modDb(query);

                if(clase.contains("precintos") || clase.contains("movimientos_det") ){
                    if(estado!=null && estado.length()>0){
                        query = " update precintos set estado = '"+estado+"' where identificador = "+id;
                    }else{
                        query = " update "+clase+" set estado = 'AN' where identificador = "+id;
                    }
                }
                cant = DBTrasactions.modDb(query);
                
                 
            } catch (Exception ex) {
                Logger.getLogger(processServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
