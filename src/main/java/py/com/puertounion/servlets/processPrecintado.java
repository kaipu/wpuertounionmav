/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author monica
 */
@WebServlet(name = "processPrecintado", urlPatterns = {"/processPrecintado"})
public class processPrecintado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String values = request.getParameter("values");
            String clase = request.getParameter("class");
            String id_tipo_precinto = request.getParameter("id_tipo_precinto");
            String nroinicial = request.getParameter("nroinicial");
            String id = request.getParameter("id");
            String id_sector_barcaza = request.getParameter("id_sector_barcaza");
            String nrotapa = request.getParameter("nrotapa");
            String nrotapita = request.getParameter("nrotapita");
            
            
            try {
                String count = "select count(*) from precintado where codigo_barras= '"+nroinicial+"' and id_tipo_precinto = "+id_tipo_precinto+" ";                
                boolean existe = DBTrasactions.retBoolean(count);
                if(existe){
                    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
                    out.println("Error, precinto ya fue cargado");
                }
                
                count = " select identificador from precintos where numero= "+nroinicial+" and id_tipo_precinto = "+id_tipo_precinto+" ";
                existe = DBTrasactions.retBoolean(count);
                if(existe){
                    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
                    out.println("Error, precinto no existe en stock");
                }
                
                String sqlPrec = "(select identificador from precintos where numero= "+nroinicial+" and id_tipo_precinto = "+id_tipo_precinto+" )";
                
                String query = "INSERT INTO precintado (id_movimiento_cab, id_precinto, "+
                                        " id_tipo_precinto, codigo_barras, id_sector_barcaza, "+
                                        " usuario, nro_tapa, nro_tapita) " +
                    "	VALUES ( "+id+","+sqlPrec+" ,"+
                        " "+id_tipo_precinto+", '"+nroinicial+"', "+id_sector_barcaza+", '',"+
                        " "+nrotapa+", "+nrotapita+" ) " ;
                    int cant = DBTrasactions.modDb(query);
                
                
                
                if(cant>0){
                    response.setStatus(HttpServletResponse.SC_OK);
                    List<String[]> lista = DBTrasactions.retList("select max(identificador) from "+clase);
                    out.println(lista.get(0)[0]);
                }else{
                    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
                }
                

            } catch (Exception ex) {
                Logger.getLogger(processServlet.class.getName()).log(Level.SEVERE, null, ex);
                 response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
