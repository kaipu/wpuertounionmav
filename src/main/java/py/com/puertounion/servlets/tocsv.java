/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author user
 */
@WebServlet(name = "tocsv", urlPatterns = {"/tocsv"})
public class tocsv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            try {
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment; filename=\"asignacion.csv\"");
                String strIde = request.getParameter("identificador");
                List<String[]> lis = null;
                List<String[]> lis2 = null;
                String[] valores = null;
                String[] valores1 = null;
                String cliente = "";
                String asignador = "";
                int identificador = 0;
                try {
                    identificador = Integer.parseInt(strIde);
                    String sql = "select  date_format(a.fecha,'%d-%m-%Y'), "
                            + "d.denominacion, c.descripcion, a.numero, "
                            + "a.peso, e.nro_chapa, a.comentarios,  "
                            + "SUBSTR(a.comentarios, length(a.comentarios)-3, 4),  "
                            + "    coalesce(a.despacho_compl_1,''),  "
                            + "    coalesce(a.despacho_compl_2,''),   "
                            + "     coalesce(cast(a.peso_compl_1 AS char), ''), "
                            + "     coalesce(cast(a.peso_compl_2 as char) ,''), "
                            + "     coalesce(a.usuario,'') "
                            + "   from    movimientos_cab a, clientes b, productos c, "
                            + "personas d, barcazas e "
                            + "   where   a.id_cliente = b.identificador "
                            + "   and     b.id_persona = d.identificador "
                            + "   and     a.id_barcaza = e.identificador "
                            + "   and     a.id_producto = c.identificador "
                            + "   and     a.identificador =  " + identificador
                            + " ";
                    lis = DBTrasactions.retList(sql);
                    valores = lis.get(0);
                    
                    sql = "select date_format(max(fecha_precintado),'%d-%m-%Y') "
                            + "from precintado "
                            + "where id_movimiento_cab="+ identificador;
                    lis2 = DBTrasactions.retList(sql);
                    valores1 = lis2.get(0);
                    cliente =lis.get(0)[1].trim();
                    asignador = lis.get(0)[13].trim();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(valores1[0]);
                Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
                cal.setTime(date1);
                
                String str1 = lis.get(0)[7];
                // str1 = (str1!= null && str1.length()>5)?""+str1.substring(str1.length()-4,str1.length() ):"";
                
                String str2 = lis.get(0)[8];
                str2 = (str2!= null && str2.length()>5)?""+str2.substring(str2.length()-4,str2.length() ):"";
                
                String str3 = lis.get(0)[9];
                str3 = (str3!= null && str3.length()>5)?""+str3.substring(str3.length()-4,str3.length() ):"";
                
                
                try
                {
                    OutputStream outputStream = response.getOutputStream();
                    String outputResult = "EXP. No.; KILOS; BARCAZA\n";
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();
                    outputResult = str1+"; "+str2+ "; "+str3+ "\n";
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();
                    
                    String p0 = (lis.get(0)[4]!=null)?lis.get(0)[4]:"";
                    p0 = p0.replace(".000", "");
                    if(p0!=null && p0.length()>1){
                        String pattern = "###,###";
                        DecimalFormat decimalFormat = new DecimalFormat(pattern);
                        String format = decimalFormat.format(Long.parseLong(p0));
                        outputResult = format+ "\n";
                        outputStream.write(outputResult.getBytes());
                        outputStream.flush();
                    }
                    
                    String p1 = (lis.get(0)[10]!=null)?lis.get(0)[10]:"";
                    p1 = p1.replace(".000", "");
                    if(p1!=null && p1.length()>1){
                        String pattern = "###,###";
                        DecimalFormat decimalFormat = new DecimalFormat(pattern);

                        String format = decimalFormat.format(Long.parseLong(p1));
                        
                        outputResult = format+ "\n";
                        outputStream.write(outputResult.getBytes());
                        outputStream.flush();
                    }      
                    
                    String p2 = (lis.get(0)[11]!=null)?lis.get(0)[11]:"";
                    p2 = p2.replace(".000", "");

                    if(p2!=null && p2.length()>1){
                        String pattern = "###,###";
                        DecimalFormat decimalFormat = new DecimalFormat(pattern);

                        String format = decimalFormat.format(Long.parseLong(p2));
                        outputResult = format+ "\n";
                        outputStream.write(outputResult.getBytes());
                        outputStream.flush();

                    }                  
                    
                    String sql = " select  b.codigo_barras "
                    + "  from    precintado a, precintos b"
                    + "  where   a.id_precinto = b.identificador"
                    + "  and     b.id_tipo_precinto = 4 "
                    + "  and     a.id_movimiento_cab = " + identificador
                    + " order by b.numero asc " ;
                    List<String[]> lista = DBTrasactions.retList(sql);
                    if(lista==null)
                        lista = new ArrayList<String[]>();                                    
                    
                    if(lista.size()>0){
                        outputResult = "ADUANA PY ("+lista.size()+")"+"\n";
                        outputStream.write(outputResult.getBytes());
                        outputStream.flush();

                    }
                    String precintos = "";
                    for (int idx = 0; idx < lista.size(); idx++) {
                        String[] elem = lista.get(idx);
                        System.out.println(elem[0]);
                        if (idx % 10 == 0 && idx > 0) {
                            precintos+="\n";
                        }
                        precintos+=  elem[0] + ";";
                    }
                    outputResult = precintos;
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();

                    sql = " select  b.codigo_barras, a.usuario "
                           + "  from    precintado a, precintos b"
                           + "  where   a.id_precinto = b.identificador"
                           + "  and     b.id_tipo_precinto = 3 "
                           + "  and     a.id_movimiento_cab = " + identificador
                            + " order by b.numero asc " ;
                    List<String[]> lista2 = DBTrasactions.retList(sql);
                    if(lista==null)
                       lista = new ArrayList<String[]>();
                    String precintador = " ";
                    if(lista2.size()>0){
                        outputResult = "CLIENTE ("+lista2.size()+")";
                        outputStream.write(outputResult.getBytes());
                        outputStream.flush();
                    }                    
                    precintos = "";
                    for (int idx = 0; idx < lista.size(); idx++) {
                        String[] elem = lista.get(idx);
                        System.out.println(elem[0]);
                        if (idx % 10 == 0 && idx > 0) {
                            precintos+="\n";
                            precintador =elem[1] ;
                        }
                        precintos+=  elem[0] + ";";
                    }
                    
                    outputResult = precintos;
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();                    
                    
                    outputResult = "Precintador;"+precintador;
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();                 
                    
                    outputResult = "Usuario de Balanza;"+asignador;
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();                    
                    
                    
                    outputStream.close();
                }
                catch(Exception e)
                {
                    System.out.println(e.toString());
                }
            } catch (ParseException ex) {
                    Logger.getLogger(tocsv.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
