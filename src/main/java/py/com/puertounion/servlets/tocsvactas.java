/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author user
 */
@WebServlet(name = "tocsvactas", urlPatterns = {"/tocsvactas"})
public class tocsvactas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        try {
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment; filename=\"asignacion.csv\"");
                
                List<String[]> lis = null;
                List<String[]> lis2 = null;
                String[] valores = null;
                String[] valores1 = null;
                String cliente = "";
                String asignador = "";
                int identificador = 0;
                try {
                    
                    String sql = "select a.identificador, a.numero, DATE_FORMAT(a.fecha,'%d-%m-%Y'), cli.denominacion, "+ 
                    "          c.nombre,  d.nro_chapa, a.destino, a.comentarios, "
                    + " IF(tipo_asignacion='PR', 'Precintado', 'Desprecintado'), a.estado"+
                    "  from movimientos_cab a, clientes b, personas cli, "+ 
                    "       almacenes c, barcazas d, operarios e, personas ope, "+ 
                    "       productos f  "+ 
                    "  where a.id_cliente = b.identificador "+ 
                    "  and b.id_persona = cli.identificador "+ 
                    "  and c.identificador = a.id_almacen "+ 
                    "  and d.identificador = a.id_barcaza  "+ 
                    "  and a.id_operario = e.identificador "+ 
                    "  and e.id_persona = ope.identificador "+ 
                    "  and f.identificador = a.id_producto "+ 
                    "  order by a.fecha desc ";
                    
                    lis = DBTrasactions.retList(sql);
                    
                    OutputStream outputStream = response.getOutputStream();
                    String outputResult = "Id.; No.; Fecha; Cliente; Barcaza; Chapa; Destino; Comentario; Tipo; Estado; Precinto\n";
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();
                    for (int i = 0; i < lis.size(); i++) {
                        String[] get = lis.get(i);
                        for (int j = 0; j < get.length; j++) {
                            String string = get[j];
                            outputResult += string+"; ";
                        }
                        outputResult += "\n";
                        outputStream.write(outputResult.getBytes());
                        outputStream.flush();
                        
                         sql = " select  b.codigo_barras "
                                + "  from    precintado a, precintos b"
                                + "  where   a.id_precinto = b.identificador"
                                + "  and     b.id_tipo_precinto = 4 "
                                + "  and     a.id_movimiento_cab = " + get[0]
                                + " order by b.numero asc " ;
                        List<String[]> lista = DBTrasactions.retList(sql);
                        for (int j = 0; j < lista.size(); j++) {
                            String[] get1 = lista.get(j);
                            outputResult = "; ; ; ; ; ; ; ; ; ;  "+get1[0]+"\n";
                            outputStream.write(outputResult.getBytes());
                            outputStream.flush();
                        }
                        
                    }
                    
                    outputStream.flush();                    
                    outputStream.close();
                }
                catch(Exception e)
                {
                    System.out.println(e.toString());
                }
            } catch (Exception ex) {
                    Logger.getLogger(tocsvactas.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
