/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author user
 */
@WebServlet(name = "processOperarios", urlPatterns = {"/processOperarios"})
public class processOperarios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        
            /* TODO output your page here. You may use following sample code. */
            String values = request.getParameter("values");
            String clase = request.getParameter("class");
            
            String[] clases = clase.split(",");
            HashMap<String,String> mClases = new HashMap<>();
            
            for (int i = 0; i < clases.length; i++) {
                String query = "insert into "+clases[i]+" (%s) values (%s)";
                mClases.put(clases[i],query);
            }
            
            values = URLDecoder.decode(values);
            System.out.println(clase);
            System.out.println(URLDecoder.decode(values));
            try {
                
                String campos=" ";
                String valores = " ";
                String[] arr1 = values.split(";");
                System.out.println(arr1.length);
                String val2 = "";
                for (int i = 0; i < arr1.length; i++) {
                    System.out.println(arr1[i]);
                    String[] arr2 = arr1[i].split(":");
                    String val = mClases.get(arr2[0]);
                    
                    
                    if(arr2[3]==null || arr2[3].contains("null") || arr2[3].length()<=0 )
                        valores =  "null"+",";
                    else
                        if(arr2[2].toLowerCase().contains("string"))
                            valores = "'"+arr2[3].toUpperCase() + "',";
                        else
                            valores = ""+arr2[3] + ",";
                    
                    val2 = String.format(val, arr2[1]+",%s", valores + "%s");
                    mClases.put(arr2[0], val2);
                }
                 int c = 0;
                 String[] sqls = new String[clases.length];
                for (int i = 0; i < clases.length; i++) {
                    String query = mClases.get(clases[i]).replace(",%s)", ")");
                    //System.out.println("val2: "+query);
                    sqls[i]= query;
                    //c++;
                }
                 c = DBTrasactions.modDbList(sqls);
                    
                if(c>0){
                    response.setStatus(HttpServletResponse.SC_OK);
                    List<String[]> lista = DBTrasactions.retList("select max(identificador) from "+clases[0]);
                    out.println(lista.get(0)[0]);
                }else{
                    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
                }
                
                //int cant = DBTrasactions.modDb(val2);
                
            } catch (Exception ex) {
                Logger.getLogger(processServlet.class.getName()).log(Level.SEVERE, null, ex);
                response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
