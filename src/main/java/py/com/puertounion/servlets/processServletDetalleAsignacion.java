/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author user
 */
@WebServlet(name = "processServletDetalleAsignacion", urlPatterns = {"/processServletDetalleAsignacion"})
public class processServletDetalleAsignacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String values = request.getParameter("values");
            String clase = request.getParameter("class");
            String id = request.getParameter("id");
            String id_tipo_precinto = request.getParameter("id_tipo_precinto");
            
            values = URLDecoder.decode(values);
            System.out.println(clase);
            System.out.println(URLDecoder.decode(values));
            try {
                
                String[] arr1 = values.split(";");
                int desde = Integer.parseInt(arr1[0].split(":")[1]);
                int hasta = Integer.parseInt(arr1[1].split(":")[1]);
                int ide = Integer.parseInt(id);
                int c = 0;
                for (int i = desde; i <= hasta; i++) {
                    String query = "select identificador, estado from precintos where numero = "+i+" and id_tipo_precinto = "+id_tipo_precinto+"";
                    List<String[]> precinto = DBTrasactions.retList(query);
                    int idPrecinto = 0;
                    if(precinto ==null || precinto.size()<=0){
                        out.println("No exite precinto nro "+i);
                        break;
                    }else{
                        if(precinto.get(0)[1] == null || precinto.get(0)[1].contains("IN") ||  precinto.get(0)[1].contains("AS")){
                            out.println("Precinto se encuentra "+precinto.get(0)[1]);
                            break;
                        }
                        idPrecinto = Integer.parseInt(precinto.get(0)[0]);
                    }
                       
                    
                    query = "insert into movimientos_det (id_movimiento_cab, nro_linea, cantidad, id_precinto, comentarios, id_tipo_precinto) "+
                            "values ("+id+","+c+",1,"+idPrecinto+",'', "+id_tipo_precinto+" )";
                    int cantDet = DBTrasactions.modDb(query);
                    if(cantDet<=0)
                        break;
                    query = "update precintos set estado = 'AS' where identificador = "+idPrecinto;
                    cantDet = DBTrasactions.modDb(query);
                    if(cantDet<=0)
                        break;
                    
                    c++;
                }
                
                
                
                if(c>0){
                    response.setStatus(HttpServletResponse.SC_OK);
                    out.println(id);
                }else{
                    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
                }
                

            } catch (Exception ex) {
                Logger.getLogger(processServlet.class.getName()).log(Level.SEVERE, null, ex);
                 response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
