/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author user
 */
@WebServlet(name = "tocssprecintos", urlPatterns = {"/tocssprecintos"})
public class tocssprecintos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            try {
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment; filename=\"asignacion.csv\"");
                
                List<String[]> lis = null;
                List<String[]> lis2 = null;
                String[] valores = null;
                String[] valores1 = null;
                String cliente = "";
                String asignador = "";
                int identificador = 0;
                try {
                    
                    String  sql = " select numero, tp.descripcion" 
                            +" from precintos p, tipos_precinto tp"
                            +" where p.id_tipo_precinto = tp.identificador"
                            +" and p.estado = 'AC'"
                            +" order by numero";
                    
                    lis = DBTrasactions.retList(sql);
                    
                    OutputStream outputStream = response.getOutputStream();
                    String outputResult = "NUMERO. No.; TIPO PRECINTO\n";
                    outputStream.write(outputResult.getBytes());
                    outputStream.flush();
                    for (int i = 0; i < lis.size(); i++) {
                        String[] get = lis.get(i);
                        outputResult = get[0]+"; "+get[1]+ "\n";
                        outputStream.write(outputResult.getBytes());
                        outputStream.flush();
                    }
                    
                    outputStream.flush();                    
                    
                    
                    outputStream.close();
                }
                catch(Exception e)
                {
                    System.out.println(e.toString());
                }
            } catch (Exception ex) {
                    Logger.getLogger(tocssprecintos.class.getName()).log(Level.SEVERE, null, ex);
            }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
