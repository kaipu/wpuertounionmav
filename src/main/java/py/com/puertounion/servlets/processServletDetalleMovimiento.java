/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;

/**
 *
 * @author user
 */
@WebServlet(name = "processServletDetalleMovimiento", urlPatterns = {"/processServletDetalleMovimiento"})
public class processServletDetalleMovimiento extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String values = request.getParameter("values");
            String clase = request.getParameter("class");
            String id = request.getParameter("id");
            
            
            values = URLDecoder.decode(values);
            System.out.println(clase);
            System.out.println(URLDecoder.decode(values));
            try {
                
                String[] arr1 = values.split(";");
                int c = 0;
                String[] mov = arr1[0].split("-");
                String query = "";
                if(mov[1].contains("SU"))
                        query = "insert into precintos (numero,codigo_barras,observacion,id_tipo_precinto) "+
                            "values ("+arr1[2]+",'"+arr1[2]+"','insertado por movimientos',"+arr1[1]+" )";
                    else
                        query = " update precintos set estado = 'IN' where identificador = (select identificador from precintos where numero ="+arr1[2]+" and id_tipo_precinto = "+arr1[1]+") ";
                    int cantDet = DBTrasactions.modDb(query);                    
                   
                if(cantDet>0){
                    response.setStatus(HttpServletResponse.SC_OK);
                    
                    query = "insert into operaciones_det (id_operacion, id_tipo_movimiento, operacion, id_precinto ) "+
                            "values ("+id+","+mov[0]+",'"+mov[1]+"', (select identificador from precintos where numero ="+arr1[2]+" and id_tipo_precinto = "+arr1[1]+") )";
                    int cant = DBTrasactions.modDb(query);
                     if(cant>0){
                        response.setStatus(HttpServletResponse.SC_OK);
                    }else
                        response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
                }
                else 
                    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);

            } catch (Exception ex) {
                Logger.getLogger(processServlet.class.getName()).log(Level.SEVERE, null, ex);
                 response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
