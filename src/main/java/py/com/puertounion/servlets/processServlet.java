/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import py.com.puertounion.utiles.DBTrasactions;
import py.com.puertounion.utiles.Retorno;

/**
 *
 * @author monica
 */
@WebServlet(name = "processServlet", urlPatterns = {"/processServlet"})
public class processServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String values = request.getParameter("values");
            String clase = request.getParameter("class");
            String consulta = request.getParameter("consulta");
            values = URLDecoder.decode(values);
            System.out.println(clase);
            System.out.println(URLDecoder.decode(values));
            try {
                
                String campos=" ";
                String valores = " ";
                String[] arr1 = values.split(";");
                System.out.println(arr1.length);
                
                for (int i = 0; i < arr1.length; i++) {
                    System.out.println(arr1[i]);
                    String[] arr2 = arr1[i].split(":");
                    
                    campos= campos + arr2[0]+",";
                    System.out.println(campos);
                    if(arr2[2]==null || arr2[2].contains("null") || arr2[2].length()<=0 )
                        valores = valores + "null,";
                    else
                        if(arr2[1].toLowerCase().contains("string"))
                            valores = valores + "'"+arr2[2].toUpperCase() + "',";
                        else
                            valores = valores + ""+arr2[2] + ",";
                    
                }
                
                String query = "insert into "+clase+"("+campos.substring(0, campos.length()-1)+") values ("+
                        valores.substring(0, valores.length()-1)+")";
                int cant = DBTrasactions.modDb(query);
                List<String[]> lista = DBTrasactions.retList("select max(identificador) from  "+clase);
                String id =lista.get(0)[0];
                System.out.println("Ultimo id: "+id);
                if(cant>0){
                    String user = request.getSession().getAttribute("userlogon").toString();
                    if(clase.contains("movimientos_cab")){
                        query = "update movimientos_cab set usuario = '"+user+"' where identificador ="+id;
                        
                        cant = DBTrasactions.modDb(query);
                    }    
                    response.setStatus(HttpServletResponse.SC_OK);
                    out.println(id);
                }else{
                    response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
                }
                

            } catch (Exception ex) {
                Logger.getLogger(processServlet.class.getName()).log(Level.SEVERE, null, ex);
                 response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
