/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.puertounion.utiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bytebuddy.matcher.FilterableList;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.jdbc.ReturningWork;

/**
 *
 * @author ojo
 */
public class DBTrasactions {
    static String CLASE = "org.mariadb.jdbc.Driver";
    static String URL = "jdbc:mysql://192.168.1.100:3306/kaipuco_puerto";
    //static String URL = "jdbc:mysql://192.168.0.8:3306/kaipuco_puerto?zeroDateTimeBehavior=convertToNull";  
    //static String URL = "jdbc:mysql://181.120.120.76:3306/kaipuco_puerto?zeroDateTimeBehavior=convertToNull&autoReconnect=true";  
    
    static String USER = "root";
    static String PASS = "puertounion123";
    //static String PASS = "kaipu123*";

    /*
    public static int insertFile(String sql, final File file){
        Session session = null;
        final String query  = sql.replace("'null'", "null");//.toUpperCase();
        int dataFound = -1;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            
            dataFound = session.doReturningWork(new ReturningWork<Integer>() {
                @Override
                public Integer execute(Connection conn) throws SQLException {
                    FileInputStream input = null;
                    try {
                        PreparedStatement stmtConsulta = null;
                        int b;
                        System.out.println(query);
                        stmtConsulta = conn.prepareStatement(query);
                        input = new FileInputStream(file);
                        stmtConsulta.setBinaryStream(1, input);
                        b = stmtConsulta.executeUpdate();
                        conn.commit();

                        if(stmtConsulta!=null)
                            stmtConsulta.close();
                        return b;
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
                            input.close();
                        } catch (IOException ex) {
                            Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    return null;
                }
            });

            tx.commit();
            session.close();
            return dataFound;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if(session.isOpen())
                session.close();
        }

    }
     */
    public static int modDb(String sql) {
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);

            final String query = sql.replace("'null'", "null");//.toUpperCase();
            int dataFound = -1;

            PreparedStatement stmtConsulta = null;
            int b;

            System.out.println(query);

            stmtConsulta = con.prepareStatement(sql);
            b = stmtConsulta.executeUpdate();

            if (stmtConsulta != null) {
                stmtConsulta.close();
            }

            return (b);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
    
    
    public static int modDbList(String[] sqls) {
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);

            PreparedStatement stmtConsulta = null;
            
                int b=0;
            for (int i = 0; i < sqls.length; i++) {
                String sql = sqls[i];
                 final String query = sql.replace("'null'", "null");//.toUpperCase();
                int dataFound = -1;


                System.out.println(query);

                stmtConsulta = con.prepareStatement(sql);
                b += stmtConsulta.executeUpdate();
                
            }
           

            if (stmtConsulta != null) {
                stmtConsulta.close();
            }

            return (b);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public static int modDb(String sql, boolean sess) {
        Session session = null;
        final String query = sql.replace("'null'", "null");//.toUpperCase();
        int dataFound = -1;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            dataFound = session.doReturningWork(new ReturningWork<Integer>() {
                @Override
                public Integer execute(Connection conn) throws SQLException {
                    PreparedStatement stmtConsulta = null;
                    int b;

                    System.out.println(query);

                    stmtConsulta = conn.prepareStatement(query);
                    b = stmtConsulta.executeUpdate();
                    conn.commit();
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;
                }
            });

            tx.commit();
            session.close();
            return dataFound;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }

    }

    public static boolean retBoolean(final String sql, boolean sess) {
        //       System.out.println("antes openSession");
        Session session = null;

        boolean dataFound = false;
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            //     System.out.println("despues openSession");
            //   System.out.println("antes doReturningWork");
            dataFound = session.doReturningWork(new ReturningWork<Boolean>() {
                @Override
                public Boolean execute(Connection conn) throws SQLException {
                    PreparedStatement stmtConsulta = null;
                    boolean b = false;

                    ResultSet rsConsulta = null;

                    System.out.println(sql/*.toUpperCase()*/);
                    stmtConsulta = conn.prepareStatement(sql/*.toUpperCase()*/);
                    rsConsulta = stmtConsulta.executeQuery();

                    if (rsConsulta.next()) {
                        b = true;
                    } else {
                        b = false;
                    }
                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;
                }
            });

            return dataFound;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }
    }

    public static boolean retBoolean(final String sql) {
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);

            final String query = sql.replace("'null'", "null");//.toUpperCase();
            int dataFound = -1;
            ResultSet rsConsulta = null;

            PreparedStatement stmtConsulta = null;
            boolean b;

            System.out.println(query);

            stmtConsulta = con.prepareStatement(sql);
            rsConsulta = stmtConsulta.executeQuery();

                    if (rsConsulta.next()) {
                        b = true;
                    } else {
                        b = false;
                    }
                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public static int retInt(final String sql) {
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);

            final String query = sql.replace("'null'", "null");//.toUpperCase();
            int dataFound = -1;
            ResultSet rsConsulta = null;

            PreparedStatement stmtConsulta = null;
            int b = 0;

            System.out.println(query);

            stmtConsulta = con.prepareStatement(sql);
            rsConsulta = stmtConsulta.executeQuery();

                    if (rsConsulta.next()) {
                        b = rsConsulta.getInt(1);
                    } else {
                        b = 0;
                    }
                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }



    public static long retLong(final String sql) {
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);

            final String query = sql.replace("'null'", "null");//.toUpperCase();
            int dataFound = -1;
            ResultSet rsConsulta = null;

            PreparedStatement stmtConsulta = null;
            long b = 0;

            System.out.println(query);

            stmtConsulta = con.prepareStatement(sql);
            rsConsulta = stmtConsulta.executeQuery();

                    if (rsConsulta.next()) {
                        b = rsConsulta.getLong(1);
                    } else {
                        b = 0;
                    }
                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public static List retList(final String sql, boolean sess) {
        //       System.out.println("antes openSession");
        Session session = null;

        List dataFound = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            //     System.out.println("despues openSession");
            //System.out.println("antes doReturningWork");
            //System.out.println(sql);
            dataFound = session.doReturningWork(new ReturningWork<List>() {
                @Override
                public List execute(Connection conn) throws SQLException {
                    PreparedStatement stmtConsulta = null;
                    List b = new ArrayList();
                    ResultSet rsConsulta = null;

                    System.out.println(sql/*.toUpperCase()*/);
                    stmtConsulta = conn.prepareStatement(sql/*.toUpperCase()*/);
                    rsConsulta = stmtConsulta.executeQuery();

                    //System.out.println("después del rs");
                    ResultSetMetaData rsmd = rsConsulta.getMetaData();
                    //System.out.println("1");
                    while (rsConsulta.next()) {
                        //System.out.println("2");
                        String[] datos = new String[rsmd.getColumnCount()];
                        for (int i = 0; i < rsmd.getColumnCount(); i++) {

                            datos[i] = rsConsulta.getString(i + 1);
                            //System.out.println(i);
                        }
                        //System.out.println(datos);
                        b.add(datos);
                    }
                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;
                }
            });

            return dataFound;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static HashMap<String,List<String>> retHashMap(final String sql) {
        //       System.out.println("antes openSession");
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);
       
                    PreparedStatement stmtConsulta = null;
                    HashMap<String,List<String>> b = new HashMap<String,List<String>>();
                    ResultSet rsConsulta = null;

                    System.out.println(sql/*.toUpperCase()*/);
                    stmtConsulta = con.prepareStatement(sql/*.toUpperCase()*/);
                    rsConsulta = stmtConsulta.executeQuery();

                    //System.out.println("después del rs");
                    ResultSetMetaData rsmd = rsConsulta.getMetaData();
                    //System.out.println("1");
                    List<String> t = null;
                    while (rsConsulta.next()) {
                        if(b.get(rsConsulta.getString(1))==null){
                            t = new ArrayList<>();
                            t.add(rsConsulta.getString(2)+" ("+rsConsulta.getString(3)+")");
                            b.put(rsConsulta.getString(1),t);
                        }else{
                            b.get(rsConsulta.getString(1)).add(rsConsulta.getString(2)+" ("+rsConsulta.getString(3)+")");
                        }
                        
                    }
                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;
               
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
          if (con != null) {
              try {
                  con.close();
              } catch (SQLException ex) {
                  Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
              }
          }
                
        }
    }

    public static List retList(final String sql) {
        //       System.out.println("antes openSession");
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);
       
                    PreparedStatement stmtConsulta = null;
                    List b = new ArrayList();
                    ResultSet rsConsulta = null;

                    System.out.println(sql/*.toUpperCase()*/);
                    stmtConsulta = con.prepareStatement(sql/*.toUpperCase()*/);
                    rsConsulta = stmtConsulta.executeQuery();

                    //System.out.println("después del rs");
                    ResultSetMetaData rsmd = rsConsulta.getMetaData();
                    //System.out.println("1");
                    while (rsConsulta.next()) {
                        //System.out.println("2");
                        String[] datos = new String[rsmd.getColumnCount()];
                        for (int i = 0; i < rsmd.getColumnCount(); i++) {

                            datos[i] = rsConsulta.getString(i + 1);
                            //System.out.println(i);
                        }
                        //System.out.println(datos);
                        b.add(datos);
                    }
                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return b;
               
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
          if (con != null) {
              try {
                  con.close();
              } catch (SQLException ex) {
                  Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
              }
          }
                
        }
    }

    public static Retorno retListDesc(final String sql) {
        //       System.out.println("antes openSession");
        Connection con = null;
        try {
            Class.forName(CLASE);
            con = DriverManager.getConnection(URL, USER, PASS);

                    PreparedStatement stmtConsulta = null;
                    Retorno ret = new Retorno();
                    List b = new ArrayList();
                    ResultSet rsConsulta = null;

                    System.out.println(sql/*.toUpperCase()*/);
                    stmtConsulta = con.prepareStatement(sql/*.toUpperCase()*/);
                    rsConsulta = stmtConsulta.executeQuery();

                    //System.out.println("después del rs");
                    ResultSetMetaData rsmd = rsConsulta.getMetaData();
                    //System.out.println("1");
                    while (rsConsulta.next()) {
                        //System.out.println("2");
                        String[] datos = new String[rsmd.getColumnCount()];
                        for (int i = 0; i < rsmd.getColumnCount(); i++) {

                            datos[i] = rsConsulta.getString(i + 1);
                            //System.out.println(i);
                        }
                        //System.out.println(datos);
                        b.add(datos);
                    }
                    String strRet = "";
                    for (int i = 0; i < rsmd.getColumnCount(); i++) {
                        strRet = strRet + ";" + rsmd.getColumnName(i + 1);
                    }
                    ;
                    ret.setLista(b);
                    ret.setDescripcion(strRet);

                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return ret;
          
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
          if (con != null) {
              try {
                  con.close();
              } catch (SQLException ex) {
                  Logger.getLogger(DBTrasactions.class.getName()).log(Level.SEVERE, null, ex);
              }
            }
                
        }
    }

    public static Retorno retListDesc(final String sql, boolean sess) {
        //       System.out.println("antes openSession");
        Session session = null;

        Retorno dataFound = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            //     System.out.println("despues openSession");
            //System.out.println("antes doReturningWork");
            //System.out.println(sql);
            dataFound = session.doReturningWork(new ReturningWork<Retorno>() {
                @Override
                public Retorno execute(Connection conn) throws SQLException {
                    PreparedStatement stmtConsulta = null;
                    Retorno ret = new Retorno();
                    List b = new ArrayList();
                    ResultSet rsConsulta = null;

                    System.out.println(sql/*.toUpperCase()*/);
                    stmtConsulta = conn.prepareStatement(sql/*.toUpperCase()*/);
                    rsConsulta = stmtConsulta.executeQuery();

                    //System.out.println("después del rs");
                    ResultSetMetaData rsmd = rsConsulta.getMetaData();
                    //System.out.println("1");
                    while (rsConsulta.next()) {
                        //System.out.println("2");
                        String[] datos = new String[rsmd.getColumnCount()];
                        for (int i = 0; i < rsmd.getColumnCount(); i++) {

                            datos[i] = rsConsulta.getString(i + 1);
                            //System.out.println(i);
                        }
                        //System.out.println(datos);
                        b.add(datos);
                    }
                    String strRet = "";
                    for (int i = 0; i < rsmd.getColumnCount(); i++) {
                        strRet = strRet + ";" + rsmd.getColumnName(i + 1);
                    }
                    ;
                    ret.setLista(b);
                    ret.setDescripcion(strRet);

                    if (rsConsulta != null) {
                        rsConsulta.close();
                    }
                    if (stmtConsulta != null) {
                        stmtConsulta.close();
                    }

                    return ret;
                }
            });

            return dataFound;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

}
