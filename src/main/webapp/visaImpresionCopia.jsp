<%-- 
    Document   : visaImpresion
    Created on : 08/07/2017, 01:24:47 PM
    Author     : ojo
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.owlike.genson.Genson"%>
<%@page import="py.com.fleming.visas.pojos.Visa"%>
<%@page import="py.com.fleming.visas.beans.VisaBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AUTORIZACION DE ESTUDIOS Y/O ANALISIS</title>
    </head>
  <style type="text/css">
    .bb td, .bb th {
     border-bottom: 1px solid black !important;
    }
    .bt td, .bb th {
     border-top: 1px solid black !important;
    }
  </style>
  <body bgcolor="#FFFFFF">
      <%
          String numvisa = request.getParameter("numvisa");
          
          long numvis = Long.parseLong(numvisa);
          System.out.println("número de visa final:"+numvis);
          Visa v = VisaBean.getVisa(numvis);
          Genson genson = new Genson();
          SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
          

        Calendar cal = Calendar.getInstance(); 
        cal.setTime(v.getCabecera().getFecha());
        cal.add(Calendar.MONTH, 1);
        Date venc = new Date(cal.getTimeInMillis());

      %>
      <div style="background-color: white;" id="impresion">
	<left>
		<div style="height: 95px; width:  1100px;vertical-align: middle;">
		    
			<table>
				<tr>
					<td align="left"><img src="./images/logo.png" width="190px" height="80px"/></td>
					<td width="900px" align="center">AUTORIZACION DE ESTUDIOS Y/O ANALISIS</td>
				</tr>
			</table>

		</div>
		<div style="border: 1px solid #000000; width:  1100px; text-align: left;height: 40px; vertical-align: middle;">
		    
			<table>
				<tr align="left">
					<td width="160px" align="left">Nro. de Autorización:</td><td width="200px" align="left"><b><%= v.getCabecera().getNrovis() %></b></td>
					<td width="60px" align="left">Fecha:</td><td width="200px" align="left"><b><%= dt1.format(v.getCabecera().getFecha()) %></b></td>
					<td width="100px" align="left">Vencimiento:</td><td width="200px" align="left"><b><%= dt1.format(venc) %></b></td>
				</tr>
			</table>
		</div>
		<div style="border: 1px solid #000000; width:  1100px; text-align: left;height: 100px; vertical-align: middle;">
		    
			<table>
				<tr>
					<td width="60px" align="left">Titular:</td><td width="560px" align="left"><b><%= v.getGa().getTitular().getNombreCompleto() %></b></td>
					<td width="60px" align="left">Estado:</td><td width="200px" align="left"><b><%= v.getGa().getEstado() %></b></td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="110px" align="left">Contrato Nro.:</td><td width="250px" align="left"><b><%= v.getCabecera().getNro_cont() %></b></td>
					<td width="50px" align="left">Plan:</td><td width="200px" align="left"><b><%= v.getCabecera().getNrorem() %></b></td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="80px" align="left">Paciente:</td><td width="540px" align="left"><b><%= v.getCabecera().getAdh().getNombreCompleto() %></b></td>
					<td width="60px" align="left">Parent:</td><td width="170px" align="left"><b><%= v.getCabecera().getAdh().getDesParentesco() %></b></td>
					<td width="60px" align="left">Edad:</td><td width="80px" align="left"><b><%= v.getCabecera().getAdh().getEdad() %></b></td>
				</tr>
			</table>
		</div>
		<div style="border: 1px solid #000000; width:  1100px; text-align: left; vertical-align: middle;">
		    
			<table>
				<tr>
                                    <td width="80px" align="left">Prestador:</td><td width="700px" align="left"><b><%= v.getCabecera().getProveedor() %> <%= v.getCabecera().getPresta().getPrtDenominacion() %> - <%= v.getCabecera().getPresta().getPrtNombre() %></b></td>
				</tr>
			</table>
			<table>
				<tr>
					<td width="150px" align="left">Medico Solicitante:</td><td width="500px" align="left">
                                            <b><%= (v.getCabecera().getMedico()!=null)?v.getCabecera().getMedico().getCodigo()+" "+v.getCabecera().getMedico().getNombre():"" %> 
                                                </b></td>
				</tr>
			</table>
		</div>
		<div style="border: 0px solid #000000; width:  1100px; text-align: left; vertical-align: middle;">
		    
			<table>
				<tr class="bb">
					<td width="700px" align="left">Servicio</td>
					<td width="100px" align="left">Cantidad</td>
					<td width="100px" align="left">Cobertura</td>
					
				</tr>
                                <% 
                                    Double total = 0.0;
                                    Double cob = 0.0;
                                    for(int i=0; i< v.getDetalle().size();i++)
                                    {
                                %>
                                    
				<tr>
					<td width="700px" align="left"><b><%= v.getDetalle().get(i).getDes_nome() %></b></td>
					<td width="100px" align="left"><b><%= v.getDetalle().get(i).getCantidad()%></b></td>
					<td width="100px" align="left"><b><%= v.getDetalle().get(i).getCobertura()%>%</b></td>
					
				</tr>
                                <%
                                    total = total + v.getDetalle().get(i).getAdherente();
                                    cob = cob + v.getDetalle().get(i).getCobertura();
                                }
                                %>
			</table>
			<table>
                            <tr class="bt">
					<td width="700px" align="left"><%=v.getCabecera().getObs()%></td>

				</tr>
			</table>
		</div>
		<div  style="border: 0px solid #000000; width:  1100px; text-align: left;height: 90px; vertical-align: middle;">
		</div>
		<div style="border: 0px solid #000000; width:  1100px; text-align: left; vertical-align: middle;">
		    
			<table style="float: left;" >
				<tr class="bb" >
					<td width="300px" align="left"></td>
				</tr>
				<tr>
					<td width="300px" align="left">Firma del Paciente</td>
				</tr>
				<tr>
					<td width="300px" align="left">Aclaración:</td>
				</tr>
			</table>
			<table style="display: inline-block;">
				<tr>
					<td width="500px" align="left">Procesado por</td>
				</tr>
				<tr>
                                        <%
                                            String user = session.getAttribute("userlogon").toString();
                                            
                                            String[] d1 = VisaBean.getDateFormateado().split(" ");
                                            String[] d2 = d1[0].split("-");
                                            
                                            String fechaImpresion = d2[2]+"-"+d2[1]+"-"+d2[0]+" "+d1[1].substring(0, 5);
                                        %>
					<td width="500px" align="left"><%= user %></td>
				</tr>
				<tr>
					<td width="500px" align="left"><%= fechaImpresion%></td>
				</tr>
			</table>
		</div>
		<div style="border: 0px solid #000000; width:  1100px; text-align: left; vertical-align: middle;">
		    
			<table style="" >
				<tr>
					<td width="1000px" align="left">LAS AUTORIZACIONES SIN LA FIRMA DEL PACIENTE NO SERAN RECONOCIDAS PARA EL PAGO DE LOS SERVICIOS</td>
				</tr>
				<tr>
					<td width="1000px" align="left">Para proceder al pago de los servicios se deberá adjuntar la orden médica Firmada por el médico solicitante</td>
				</tr>
			</table>
		</div>
		<div  style="border: 0px solid #000000; width:  1100px; text-align: left;height: 20px; vertical-align: middle;">
		</div>

		<div  style="border: 1px solid #000000; width:  1100px; text-align: left;height: 30px; vertical-align: middle;">
			&nbsp;Apreciado Asegurado, ante cualquier duda comuniquese con nosotros al nro. telefónico <b>021 327 4000(RA)</b> su consulta es siempre grata.
		</div>

	</left>
               
      </div>                 
        <div style="height: 140px;">
            <button id="imprimirPdf" type="button" class="btn btn-danger glyphicon glyphicon-print btn-lg"></button>
        </div>
    </body>
     <script>
         
        $("#imprimirPdf").click(
            function (){ 
                var divToPrint=document.getElementById('impresion');
                var newWin=window.open('','Print-Window');
                newWin.document.open();
                newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
            newWin.document.close();
            }
        );

     </script>
</html>
