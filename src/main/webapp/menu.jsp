<%@page import="py.com.puertounion.beans.WebBean"%>
<!DOCTYPE html>
<html lang="es">
<%
    HttpSession ss  = request.getSession();
    
    if(ss !=null && ss.getAttribute("passlogon")!=null && ss.getAttribute("passlogon").toString().length()<=0){
        //out.print("asdasdf sda sdfafdsaf dsdaf");
        boolean x =WebBean.validadrUser(ss.getAttribute("userlogon").toString(), ss.getAttribute("passlogon").toString());
        if(!x)
            response.sendRedirect("index.jsp?error=No se encuentra logueado...");
    }else{
        String user = request.getParameter("userlogon");
        String pass= request.getParameter("passlogon");
        System.out.println("user:"+user+" pass:"+pass);
        boolean x =WebBean.validadrUser(user,pass);
        if(!x){
            System.out.println("index.jsp?error=No se encuentra logueado...");
            response.sendRedirect("index.jsp?error=No se encuentra logueado...");
            return;
            
        }
        else{
            ss.setAttribute("userlogon", request.getParameter("userlogon").toString());
        }
    }
%>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Puerto Uni�n - L�deres en el servicio portuario nacional.</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="nscript/css2?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        <!-- Include Bootstrap Datepicker -->
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_bootstrap-datepicker_1.8.0_css_bootstrap-datepicker.min.css" />
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_bootstrap-datepicker_1.8.0_js_bootstrap-datepicker.min.js"></script>

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-success sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center bg-white" href="menu.jsp">
        <div class="sidebar-brand-icon rotate-n-15">
        </div>
        <div class="sidebar-brand-text mx-3" style="background-color:white;">
            <img src="images/logo2.png" width="100%" height="100%" />

        <sup>2</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="menu2.html">
          <i class="fas fa-fw fa-folder-alt"></i>
          <span>Inicio</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Generales
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Parametros</span>
        </a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Parametros</h6>
            <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoTiposProducto.jsp');">Tipos de Producto</a>
            <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoBanderas.jsp');">Banderas</a>
            <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoSectoresBarcaza.jsp');">Sectores</a>
            <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoTiposPrecinto.jsp');">Tipos de Precinto</a>
            <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoTiposMovimiento.jsp');">Tipos de Movimiento</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Mantenimiento</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Mantenimiento</h6>
                <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoAlmacenes.jsp');">Depositos</a>
                <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoBarcazas.jsp');">Barcazas</a>
                <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoProductos.jsp');">Productos</a>
                <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoOperadores.jsp');">Precintadores</a>
                <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoCargadores.jsp');">Operadores Adm.</a>
                <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoClientes.jsp');">Clientes</a>
                <a class="collapse-item" href="javascript:loadPag('mantenimiento/mantenimientoActividadesBarcaza.jsp');">Actividades BArcaza</a>
          </div>
        </div>
      </li>
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Precintos</span>
        </a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Precintos</h6>
                <a class="collapse-item" href="javascript:loadPag('precintos/listaRecepciones.jsp');">Recepci�n de Precintos</a>                            
                <a class="collapse-item" href="javascript:loadPag('precintos/listaAsignaciones.jsp');">Asignaci�n de Precintos</a>
                <a class="collapse-item" href="javascript:loadPag('precintos/listaMovimientos.jsp');">Ent./Sal. de Precintos</a>
                <a class="collapse-item" href="javascript:loadPag('precintos/listaRelatorio.jsp');">Relatorios de Embarque</a>
             
          </div>
        </div>
      </li>
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFourth" aria-expanded="true" aria-controls="collapseFourth">
          <i class="fas fa-fw fa-cog"></i>
          <span>Reportes</span>
        </a>
        <div id="collapseFourth" class="collapse" aria-labelledby="headingFourth" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Reportes</h6>
                <a class="collapse-item" href="javascript:loadPag('precintos/precintosDisponibles.jsp');">Precintos Disponibles</a>                            
                <a class="collapse-item" href="javascript:loadPag('precintos/precintosAsignados.jsp');">Precintos Asignados</a>
                <a class="collapse-item" href="javascript:loadPag('precintos/precintosInactivos.jsp');">Precintos Inactivos</a>
          </div>
        </div>
      </li>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        
        <!-- Begin Page Content -->
        <div id="div1" class="container-fluid" style="overflow-x: scroll;">


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">�</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>
    <script>
        function loadPag(param) {

            if(!confirm("Confirma realmente que quiere ir a la opci�n?")){
               return;
            }
            $.ajax({url: param,
                beforeSend: function () {
                    
                    $("#div1").show();
                    $('#div1').html("<img src='./images/loading.gif' />");
                },
                success: function (result) {
                    $("#div1").show();
                    $("#div1").html(result);
                }});
        }
    </script>
</body>

</html>
