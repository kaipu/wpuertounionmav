<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        HttpSession ss  = request.getSession();
        String user = ss.getAttribute("userlogon").toString();
        
        boolean esAdmin = DBTrasactions.retBoolean("SELECT * FROM cargadores where usuario ='"+user+"' and  admin = 1");
        
        
    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonprint {
            background-image: url(images/print-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonuser {
            background-image: url(images/user-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>    
    <body>
        <div class="container">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend style="color:rgb(83,156,239);font-size:20pt;font-family:Trebuchet MS">Relatorios de Embarque</legend>
                    <button id="nuevo" type="button" name="nuevo" class="btn btn-success">Nuevo Relatorio</button>

                </fieldset>
            </form>
            <div class="row col-md-10 col-md-offset-2 custyle">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Remolcador.</th>
                            <th>Fecha Arribo</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Barcaza</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <% 
                        String query = "select a.identificador, a.nombre_remolcador, "
                                + " DATE_FORMAT(a.fecha_arribo,'%d-%m-%Y'), " 
                                + " DATE_FORMAT(a.fecha_inicio,'%d-%m-%Y'), "
                                + " DATE_FORMAT(a.fecha_fin,'%d-%m-%Y'),"
                                + " d.nro_chapa"+
                                      "  from relatorios_embarque_cab a, barcazas d"+ 
                                      "  where d.identificador = a.id_barcaza  "+ 
                                      "  order by a.fecha_arribo desc ";
                        
                        List<String[]> lista = DBTrasactions.retList(query);
                        for (int idx = 0; idx < lista.size(); idx++) {
                            String elem[] = lista.get(idx);
                            out.print(" <tr>");
                            for (int id = 0; id < elem.length; id++) {
                                Object e = elem[id];
                                out.print("<td>" + e + "</td>");
                            }
                            String data = String.join("#",Arrays.asList(elem));
                                String botonBorrar = "<td class=\"text-center\">";
                                
                                botonBorrar += "<a href=\"#\" placeholder='Imprimir Relatorio' \" onclick=\"javascript:impRelatorio("+elem[0]+",'"+data+"')\" class=\"btn btn-success btn-xs buttonprint\">";
                                botonBorrar+="<a href=\"#\" placeholder='Actualizar Relatorio' \" onclick=\"javascript:editar("+elem[0]+",'"+data+"')\" class=\"btn btn-success btn-xs buttonedit\">";
                                
                                botonBorrar += "</td>";
                                out.print(botonBorrar);
                                out.print("</tr>");

                        }
                         
                    %>               
                </table>
            </div>

        </div>
    </body>
    
    <script>
        function InitOverviewDataTable()
        {

            var table = $('#example').dataTable(
                    {
                        "language": {
                        "url": "inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 5
                    });
        }
        $(document).ready(function () {
            InitOverviewDataTable();

        });
        $(function () {
            $("#datepicker").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
        
        function editar(id){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({url: 'precintos/relatorioEmbarque.jsp?identificador='+id,
                beforeSend: function () {
                    $("#div1").show();
                    $('#div1').html("<img src='./images/loading.gif' />");
                },
                success: function (result) {
                    $("#div1").show();
                    $("#div1").html(result);
                }});

        }
        
        function impRelatorio(id){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({url: 'precintos/impRelatorioEmbarque.jsp?identificador='+id,
                beforeSend: function () {
                    $("#div1").show();
                    $('#div1').html("<img src='./images/loading.gif' />");
                },
                success: function (result) {
                    $("#div1").show();
                    $("#div1").html(result);
                }});

        }
        function excel(id){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            var win = window.open('./tocsv?identificador='+id, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            }
        }
        function actaDesprecintados(id){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({url: 'precintos/actaDesprecintado.jsp?identificador='+id,
                beforeSend: function () {
                    $("#div1").show();
                    $('#div1').html("<img src='./images/loading.gif' />");
                },
                success: function (result) {
                    $("#div1").show();
                    $("#div1").html(result);
                }});

        }
        $('#nuevo').click(function(){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({url: 'precintos/relatorioEmbarque.jsp',
                beforeSend: function () {
                    $("#div1").show();
                    $('#div1').html("<img src='./images/loading.gif' />");
                },
                success: function (result) {
                    $("#div1").show();
                    $("#div1").html(result);
                }});

        });

    </script>
    
</html>
