<%-- 
    Document   : Caratula Acta
    Created on : 16/03/2019, 06:31:28 AM
    Author     : user
--%>

<%@page import="java.text.NumberFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="py.com.puertounion.utiles.ConvierteNumero"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>
        
    </head>
    <style type="text/css">
        th{
          background-color: gray;
          text-align: center;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <%
            
            HttpSession ss  = request.getSession();
            String strIde = request.getParameter("identificador");
            List<String[]> lis = null;
            List<String[]> lis2 = null;
            String[] valores = null;
            String[] valores1 = null;
            String cliente = "";
            int identificador = 0;
            try {
                identificador = Integer.parseInt(strIde);
                String sql = "select  date_format(a.fecha,'%d-%m-%Y'), "
                        + "d.denominacion, c.descripcion, a.numero, "
                        + "a.peso, e.nro_chapa, a.comentarios,  "
                        + "SUBSTR(a.comentarios, length(a.comentarios)-3, 4),  "
                        + "    coalesce(a.despacho_compl_1,''),  "
                        + "    coalesce(a.despacho_compl_2,''),   "
                        + "     coalesce(cast(a.peso_compl_1 AS char), ''), "
                        + "     coalesce(cast(a.peso_compl_2 as char) ,''),"
                        + "     a.destino,"
                        + " round(coalesce(a.peso,0)+coalesce(a.peso_compl_1,0)+coalesce(a.peso_compl_1,0),0)"
                        + "   from    movimientos_cab a, clientes b, productos c, "
                        + "personas d, barcazas e "
                        + "   where   a.id_cliente = b.identificador "
                        + "   and     b.id_persona = d.identificador "
                        + "   and     a.id_barcaza = e.identificador "
                        + "   and     a.id_producto = c.identificador "
                        + "   and     a.identificador =  " + identificador
                        + " ";
                lis = DBTrasactions.retList(sql);
                valores = lis.get(0);
                
                sql = "select date_format(max(fecha_precintado),'%d-%m-%Y') "
                        + "from precintado "
                        + "where id_movimiento_cab="+ identificador;
                lis2 = DBTrasactions.retList(sql);
                valores1 = lis2.get(0);
                cliente =lis.get(0)[1].trim();

            } catch (Exception ex) {
                    ex.printStackTrace();
            }
            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(valores1[0]);
            Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
            cal.setTime(date1);

            String nroExp = lis.get(0)[7];
            nroExp = (nroExp!= null && nroExp.length()>5)?""+nroExp.substring(nroExp.length()-4,nroExp.length() ):"";
            
            String despComp1 = lis.get(0)[8];
            despComp1 = (despComp1!= null && despComp1.length()>5)?""+despComp1.substring(despComp1.length()-4,despComp1.length() ):"";
            
            String despComp2 = lis.get(0)[9];
            despComp2 = (despComp2!= null && despComp2.length()>5)?""+despComp2.substring(despComp2.length()-4,despComp2.length() ):"";

        %>        
            <div id="impresion" name="impresion">
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b><%=lis.get(0)[1]%></b>
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b>PUERTO DE:    PUERTO UNION S.A.     FECHA: <%=lis.get(0)[0]%></b>
            </div>
            <div id="texto" style="text-align: left; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b>A BORDO DE LAS BARCAZAS: <%=lis.get(0)[5]%></b>
            </div>
            <div id="texto" style="text-align: left; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b>DESTINO:                 <%=lis.get(0)[12]%></b>
            </div>
                        
            <br>
            <table style="margin: 0px auto;" border="1" cellpadding="0" cellspacing="0">
                <tr>

                	<td valign="top">
                        <table>
                        		<tr>
					<th width="390px">CANTIDAD</th>
					</tr>
								
                                        <tr>
                    				<td>
                                                    &nbsp;
                    				</td>
                    			</tr> 		
                                        <tr>
                    				<td>
                                                    &nbsp;
                    				</td>
                    			</tr> 
                                        <tr>
                    				 <td align="right">
                    					<%
                                                            Double dd = 0.0;
                                                            try{
                                                                dd = Double.parseDouble(lis.get(0)[4]);

                                                                 NumberFormat nf = NumberFormat.getInstance(); 

                                                                 out.println(nf.format(dd.longValue()).replace(",", "."));

                                                            }catch(Exception ex){
                                                            }

                                                        %>
                    				</td>
                    			</tr>
                    			 <tr>
				                    <td align="right">
                                                            <%
                                                                Double dd2 = 0.0;
                                                                try{
                                                                    dd2 = Double.parseDouble(lis.get(0)[10]);
                                                                    NumberFormat nf = NumberFormat.getInstance(); 

                                                                     out.println(nf.format(dd2.longValue()).replace(",", "."));
                                                                }catch(Exception ex){
                                                                }

                                                            %>
				                    </td>
				                </tr>
                    			 <tr>
				                    <td align="right">
                                                        <%
                                                            Double dd3 = 0.0;
                                                             try{
                                                                dd3 = Double.parseDouble(lis.get(0)[11]);
                                                                NumberFormat nf = NumberFormat.getInstance(); 

                                                                 out.println(nf.format(dd3.longValue()).replace(",", "."));
                                                            }catch(Exception ex){
                                                            }

                                                            long ll = ((Double)(dd3+dd2+dd)).longValue();

                                                        %>
				                    </td>
				                </tr>
                        </table>
                    </td>
                    <td  valign="top">
                        
                        <table>
                        		<tr>
					<th width="390px">DESCRIPCION DE LA MERCADERIA</th>
								</tr>
                                        <tr>
                    				<td>
                                                    <%=lis.get(0)[2]%>
                    				</td>
                    			</tr>
                                        <tr>
                    				<td>
                                                    <%=lis.get(0)[6]%>
                    				</td>
                    			</tr>
                                        <tr>
                    				<td>
                                                    <%=lis.get(0)[8]%>
                    				</td>
                    			</tr>
                                        <tr>
                    				<td>
                                                    <%=lis.get(0)[9]%>
                    				</td>
                    			</tr>
                                        
                                        
                        </table>
                    </td>
                    <td  valign="top">
                        
                        <table>
                        		<tr>
									<th width="390px">TOTAL EMBARCADO EN LETRAS</th>
								</tr>
								<tr>
                    				<td>
                                                    <% 
                                                        ConvierteNumero c = new ConvierteNumero();
                                                        String sll = "" + ll;
                                                        out.print(c.Convertir(sll, true));

                                                    %>
                    				</td>
                    			</tr>
                        </table>
                    </td>


                </tr>
              
                <tr  valign="top">
                    <td align="right">
                        Total Kg: <% 
                            
                            NumberFormat nf = NumberFormat.getInstance(); 
                                 
                                 out.println(nf.format(ll).replace(",", "."));
                            //out.println(ll);
                            
                        
                        %>
                          
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                
            </table>

            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <table style="margin: 0px auto; width:90%; font-size:80%;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    

                    <td align="center"><img src="../images/<%=ss.getAttribute("userlogon").toString().toUpperCase() %>.png"  width="120" height="60" /></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                </tr>                 
                <tr>
                    

                    <td align="center">....................................</td>
                    <td align="center">..........................</td>
                    <td align="center">..........................</td>
                </tr>
                <tr>
                    

                    <td align="center"><% out.print(cliente.trim()); %></td>
                    <td align="center">Aduanas</td>
                    <td align="center" >Capitán 1er. Oficial</td>
                </tr>
            </table>            
            
            
                                                <br><br><br><br><br><br>

             <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                 
                 <img src="../images/<%=ss.getAttribute("userlogon").toString().toUpperCase() %>.png"  width="120" height="60" />
                 <br>
                 Elaborado por Puerto Unión S.A.
             </div>
                    
            <br>
            <br>
            <br>
            <br>
            
        </div>

        <center>
            <button onclick="javascript:printDiv()" id="print" type="button" name="print" class="btn btn-success">Imprimir</button>
        </center>

         
    </body>
    <script>
        function printDiv()
        {
            
            var divToPrint = document.getElementById('impresion');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
        }

    </script>
</html>