<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>

    </head>
    <style type="text/css">
        th{
          background-color: gray;
          text-align: center;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <%
            
            List<String[]> lis = null;
            String[] valores = null;
            String sql = "";
            try {
                sql = " select codigo_barras, tp.descripcion" 
                            +" from precintos p, tipos_precinto tp"
                            +" where p.id_tipo_precinto = tp.identificador"
                            +" and p.estado = 'AC'"
                            +" order by numero";
                lis = DBTrasactions.retList(sql);
                valores = lis.get(0);

            } catch (Exception ex) {

            }
            

        %>        
        <div id="impresion" name="impresion">
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b>PUERTO UNION S.A.</b>
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b>Precintos Disponibles</b>
            </div>
            <a href="#" placeholder='Ver excel' onclick="javascript:toexcel();" class="btn btn-alert btn-xs buttonprint">Bajar a excel</a>
                
            
            <table id="example" class="display" style="width:100%">
                 <thead>
                        <tr>
                    <th width="180px">NUMERO. No.</th>
                    <th width="180px">TIPO PRECINTO</th>
                </tr>
                 </thead>
                
                    <%
                     for (int idx = 0; idx < lis.size(); idx++) {
                    %>
                    <tr>
                        <td><%=lis.get(idx)[0]%></td>
                        <td><%=lis.get(idx)[1]%></td>
                        
                    </tr>
                    <%
                      }
                    %>
                   
            </table>
        </div>
        <br>
        <center>
            <button onclick="javascript:printDiv()" id="print" type="button" name="print" class="btn btn-success">Imprimir</button>
        </center>
    
        <br><br><br><br><br><br>
        <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
        </div>
    </body>
    <script>
        function printDiv()
        {
            
            var divToPrint = document.getElementById('impresion');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
        }
        $(document).ready(function () {
            InitOverviewDataTable();

        });
        function InitOverviewDataTable()
        {

            var table = $('#example').dataTable(
                    {
                        "language": {
                        "url": "inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 30
                    });
        }
        function alerta(){
            alert(1);
        }
        function toexcel(){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            var win = window.open('./tocssprecintos ','_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            }
        }

    </script>
</html>