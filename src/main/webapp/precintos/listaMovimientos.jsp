<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>    
    <body>
        <div class="container">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend style="color:rgb(83,156,239);font-size:20pt;font-family:Trebuchet MS">Entrada/Salida de Precintos</legend>
                    <hr>                  
                    <div id="detalle"  style="display:block" >
                          <div class="input-group">
                              <label class="col-md-2 control-label" for="id_tipo_precinto">Tipo Precinto: </label>  
                                <select class="col-md-2 form-control" id="id_tipo_precinto">
                                <% 
                                    List<String[]> tlista2 = DBTrasactions.retList("select identificador, descripcion from tipos_precinto");
                                    for (int idx = 0; idx < tlista2.size(); idx++) {
                                        String elem[] = tlista2.get(idx);
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }

                                %>
                              </select>  
                                <label class="col-md-2 control-label" for="nro">Nro. Precinto: </label>  
                                <input id="nro" name="nro" type="text" placeholder="" class="col-md-2 form-control input-sm">
                              
                                <button id="agregardetalle" type="button" name="agregardetalle" class="btn btn-success">Agregar</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <hr>                  
            <div class="row col-md-10 col-md-offset-2 custyle">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID.</th>
                            <th>Código precinto</th>
                            <th>Tipo Precinto</th>
                            <th>Fecha Recepción</th>
                            <th>Nro Remisión</th>
                            <th>Observación</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <% 
                        String query = "select  a.identificador, a.codigo_barras , b.descripcion, c.fecha, c.nro_remision,a.observacion "+
                                       " from    precintos a, tipos_precinto b, recepciones_cab c, recepciones_det d"+
                                       " where   a.id_tipo_precinto = b.identificador"+
                                       " and     a.estado = 'AC'"+
                                       " and     a.identificador = d.identificador"+
                                       " and     a.id_tipo_precinto  = d.id_tipo_precinto"+
                                       " and     d.id_recepcion_cab = c.identificador "+
                                       " order by c.fecha desc ";
                        
                        List<String[]> lista = DBTrasactions.retList(query);
                        for (int idx = 0; idx < lista.size(); idx++) {
                            String elem[] = lista.get(idx);
                            out.print(" <tr>");
                            for (int id = 0; id < elem.length; id++) {
                                Object e = elem[id];
                                out.print("<td>" + e + "</td>");

                            }
                            String data = String.join("#",Arrays.asList(elem));
                                
                                String botonBorrar = "<td class=\"text-center\"><button href=\"#\" onclick=\"javascript:borrarFila("+elem[0]+")\" class=\"btn btn-danger btn-xs buttoncancel\"></button></td>";
                                botonBorrar += "</td>";
                                out.print(botonBorrar);
                                out.print("</tr>");

                        }
                         
                    %>               
                </table>
            </div>

        </div>
    </body>
    
    <script>
        function InitOverviewDataTable()
        {

            var table = $('#example').dataTable(
                    { 
                        "language": {
                        "url": "inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 5
                    });
        }


        $(document).ready(function () {
            InitOverviewDataTable();

        });
        $(function () {
            $("#datepicker").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
        
        function editar(id){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({url: 'precintos/movimientoPrecintos.jsp?identificador='+id,
                beforeSend: function () {
                    $("#div1").show();
                    $('#div1').html("<img src='./images/loading.gif' />");
                },
                success: function (result) {
                    $("#div1").show();
                    $("#div1").html(result);
                }});

        }
        
        function borrarFila(id){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "deleteData",
                data: {
                    id: id,
                    class: "precintos"
                },
                success: function (result) {
                    alert("Registro eliminado exitosamente.");

                    $.ajax({url: 'precintos/listaMovimientos.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }

        $('#nueva').click(function(){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({url: 'precintos/listaMovimientos.jsp',
                beforeSend: function () {
                    $("#div1").show();
                    $('#div1').html("<img src='./images/loading.gif' />");
                },
                success: function (result) {
                    $("#div1").show();
                    $("#div1").html(result);
                }});

        });
        $('#agregardetalle').click(function(){
            var text =  'id_tipo_precinto:string:'+$('#id_tipo_precinto').val()+";"+
                        'codigo_barras:string:'+$('#nro').val()+";"+
                        'estado:string:AC;'+
                        'observacion:string:Alta en movimiento;'+
                        'numero:int:'+$('#nro').val()+";";
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processServlet",
                data: {
                    values: encoded,
                    class: "precintos",
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/listaMovimientos.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        });

    </script>
    
</html>
