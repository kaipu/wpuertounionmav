<%-- 
    Document   : acta_precintado
    Created on : 16/03/2019, 06:31:28 AM
    Author     : user
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>
        
    </head>
    <style type="text/css">
        th{
          background-color: gray;
          text-align: center;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <%
            
            HttpSession ss  = request.getSession();
            List<String[]> lis=null;
            String strIde = request.getParameter("identificador");
            String cliente = "";
            int identificador = 0;
            long x  = 0;
            try {
                identificador = Integer.parseInt(strIde);
                String sql = " select  date_format(c.fecha_arribo,'%d-%m-%Y'),"
                            +" date_format(c.fecha_arribo,'%H:%i:%S'),"
                            +" date_format(c.fecha_inicio,'%d-%m-%Y'),"
                            +" date_format(c.fecha_inicio,'%H:%i:%S'),"
                            +" date_format(c.fecha_fin,'%d-%m-%Y'),"
                            +" date_format(c.fecha_fin,'%H:%i:%S'),"
                            +" c.nombre_remolcador,"
                            +" p.descripcion,"
                            +" r.nro_chapa,"
                            +" c.peso, c.id_movimiento_cab"
                            +" from relatorios_embarque_cab c left outer join productos p on (p.identificador = c.id_producto), "
                            +" relatorios_embarque_det b, "
                            +" "
                            +" barcazas r"
                            +" where c.identificador = b.id_relatorio_cab"
                            +" "
                            +" and c.id_barcaza = r.identificador"
                            +" and c.identificador= " + identificador
                            + " ";
                lis = DBTrasactions.retList(sql);
                
                
                sql = " select coalesce(peso,0)+ coalesce(peso_compl_1,0)+ coalesce(peso_compl_2,0)  "
                            +" from movimientos_cab where identificador= " + lis.get(0)[10]
                            + " ";
                x = DBTrasactions.retInt(sql);
                
                
                
            } catch (Exception ex) {
                    ex.printStackTrace();
            }
            
        %>        
        <div id="impresion" name="impresion">
            <div id="texto" style="font-size: 10px; text-align: center; text-justify: inter-word;width:80%; height: 38px; margin: auto;">
                    
            </div>
            <div id="texto" style="font-size: 10px; text-align: center; text-justify: inter-word;width:80%; height: 38px; margin: auto;">
                    <b>Statement of Facts</b>
            </div>
            
            <div id="texto" style="font-size: 10px; text-align: left; text-justify: inter-word;width:80%; height: 150px; margin: auto;">
                    <b>Relatorio de Embarque</b><br>
                    <b>Puerto Embarque: Puerto Union S.A. - Zeballos Cué. Asunción</b><br>
                    <b>Nombre del Remolcador: <%=lis.get(0)[6]%> </b><br>
                    <b>Arribo de la barcaza: <%=lis.get(0)[0]%> <%=lis.get(0)[1]%></b><br>
                    <b>Inicio del embarque: <%=lis.get(0)[2]%> <%=lis.get(0)[3]%></b><br>
                    <b>Final del embarque: <%=lis.get(0)[4]%> <%=lis.get(0)[5]%></b><br>
                    <b>Producto: <%=lis.get(0)[7]%> </b><br>
                    <b>Barcaza Nominada: <%=lis.get(0)[8]%> </b><br>
                    <b>Cantidad embarcada: <%=x%> Kls.</b><br>
            </div>
            <br>
            <table style="margin: 0px auto;" border="0" cellpadding="0" cellspacing="0">
                <!--<tr>
                    <th width="90px">EXP. No.</th>
                    <th width="90px">KILOS</th>
                    <th width="90px">BARCAZA</th>
                    <th width="500px">PRECINTOS</th>
                </tr>-->
                <%
                    String sql = " select date_format(d.fecha, '%d-%m-%Y'),"+
                    " d.hora_inicio, d.hora_fin,"+
                    " a.descripcion"+
                    " from relatorios_embarque_det d,"+
                    " actividades_barcaza a"+
                    " where a.identificador = d.id_actividad"+
                    " and d.id_relatorio_cab = " + identificador+" order by d.fecha, d.hora_inicio asc";
                 
                    List<String[]> lista = DBTrasactions.retList(sql);
                    if(lista==null)
                    lista = new ArrayList<String[]>();                                    
                
                %>
                                   
                  
                <%
                    String gfecha = "", fecha = ""  ;
                    for (int i=0; i < lista.size(); i++ ){
                            
                            if(!gfecha.equals(lista.get(i)[0])){
                                fecha = lista.get(i)[0];
                                gfecha = fecha;
                            }
                            
                            out.print("<tr width=\"700px\" > "
                                            +"<td  width=\"120px\" style=\"font-size: 10px; text-align: center; text-justify: inter-word; margin: auto;\"> "
                                                +fecha
                                            +"</td>"
                                            +"<td  width=\"120px\"  style=\"font-size: 10px; text-align: center; text-justify: inter-word; margin: auto;\"> "
                                                +lista.get(i)[1]
                                            +"</td>"
                                            +"<td  width=\"120px\"  style=\"font-size: 10px; text-align: center; text-justify: inter-word; margin: auto;\"> "
                                                +lista.get(i)[2]
                                            +"</td>"
                                            +"<td width=\"300px\"  style=\"font-size: 10px; text-align: left; text-justify: inter-word; margin: auto;\"> "
                                                +lista.get(i)[3]
                                            +"</td>"
                                        + "</tr>"
                        );
                 }

                %>
                
            </table>

            <br>
            <br>
            <br>
            <br>
            <table style="font-size: 10px; margin: 0px auto; width:90%;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    

                    <td align="center"><img src="images/<%=ss.getAttribute("userlogon").toString().toUpperCase() %>.png"  width="120" height="60" /></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                </tr>                  
                <tr>
                    

                    <td align="center">...............................</td>
                    <td align="center">...............................</td>
                    <td align="center">...............................</td>
                </tr>
                <tr>
                    

                    <td align="center">Cargill Agropecuaria S.A.C.A.</td>
                    <td align="center">Aduanas</td>
                    <td align="center" >Capitán 1er. Oficial</td>
                </tr>
            </table>
                                                <br><br><br><br><br><br>


             <div id="texto" style="font-size: 10px; text-align: center; text-justify: inter-word;width:80%; height: 38px; margin: auto;">
                 
                 <img src="images/<%=ss.getAttribute("userlogon").toString().toUpperCase() %>.png"  width="120" height="60" />
                 <br>
                 Elaborado por Puerto Unión S.A.
             </div>
                    
            <br>
            <br>
            <br>
            <br>
            
        </div>

        <center>
            <button onclick="javascript:printDiv()" id="print" type="button" name="print" class="btn btn-success">Imprimir</button>
        </center>

         
    </body>
    <script>
        function printDiv()
        {
            
            var divToPrint = document.getElementById('impresion');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
        }

    </script>
</html>