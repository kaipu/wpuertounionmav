<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>
        
    </head>
    <style type="text/css">
        th{
          background-color: gray;
          text-align: center;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <%
            String strIde = request.getParameter("identificador");
            List<String[]> lis = null;
            String[] valores = null;
            String cliente = "";
            int identificador = 0;
            try {
                identificador = Integer.parseInt(strIde);
                String sql = "select  date_format(a.fecha,'%d-%m-%Y'), d.denominacion, "
                        + "c.descripcion, a.numero, a.peso, e.nro_chapa  a.comentarios,  "
                        + "SUBSTR(a.comentarios, length(a.comentarios)-2, 3),  "
                        + "    coalesce(a.despacho_compl_1,''),  "
                        + "    coalesce(a.despacho_compl_2,''),   "
                        + "     coalesce(cast(a.peso_compl_1 AS char), ''), "
                        + "     coalesce(cast(a.peso_compl_2 as char) ,''), "
                        + " case c.imp_leyenda_acta when 'S' then 'ASEGURADO CONFORME A GMP+FSA' ELSE '' END"
                        + "   from    movimientos_cab a, clientes b, productos c, personas d, barcazas e "
                        + "   where   a.id_cliente = b.identificador "
                        + "   and     b.id_persona = d.identificador "
                        + "   and     a.id_barcaza = e.identificador "
                        + "   and     a.id_producto = c.identificador "
                        + "   and     a.identificador =  " + identificador;
                lis = DBTrasactions.retList(sql);
                valores = lis.get(0);
                cliente =lis.get(0)[1].trim();

            } catch (Exception ex) {

            }
            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(lis.get(0)[0]);
            Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
            cal.setTime(date1);


        %>        
        <div id="impresion" name="impresion">
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b><%=lis.get(0)[1]%></b>
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    <b>ACTA DE DESPRECINTADO</b>
            </div>
            <div id="texto" style="text-align: justify; text-justify: inter-word;width:80%; margin: auto;">
                En Puerto Unión S.A., Zeballos Cué, Asunción, Republica del Paraguay, a los <%= cal.get(Calendar.DAY_OF_MONTH)%> 
                días del mes de <%= cal.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("es", "ES"))%> del año 
                <%= cal.get(Calendar.YEAR)%>, en presencia del funcionario de la DIRECCIÓN DE ADUANAS, 
                y el personal de <%=lis.get(0)[1]%> se procede al desprecintado de la barcaza que figura más abajo, procedente del puerto <%=""%>, transportando <%=lis.get(0)[2]%>
                con la misma nominación para la recarga en Puerto Unión, Amparados por el Despacho con 
                reserva de retorno: <%= lis.get(0)[3]%>
                <br/>
                <br/>
                <%=lis.get(0)[12]%>
            </div>
            <br>
            <table style="margin: 0px auto;" border="1" cellpadding="0" cellspacing="0">
                <tr>
                    <th width="90px">EXP. No.</th>
                    <th width="90px">KILOS</th>
                    <th width="90px">BARCAZA</th>
                    <th width="500px">PRECINTOS</th>
                </tr>
                <tr>
                    <td>
                        <table border=0 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><%=lis.get(0)[7]%></td>
                            </tr>
                            <tr>
                                <td><%=lis.get(0)[8]%></td>
                            </tr>
                            <tr>
                                <td><%=lis.get(0)[9]%></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border=0 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><%=lis.get(0)[4]%></td>
                            </tr>
                            <tr>
                                <td><%=lis.get(0)[10]%></td>
                            </tr>
                            <tr>
                                <td><%=lis.get(0)[11]%></td>
                            </tr>
                        </table>
                        
                    </td>
                    <td><%=lis.get(0)[5]%></td>
                    <td>
                        <table width="500px" border=1 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr width="500px" >
                                <%
                                    String sql = " select  b.codigo_barras "
                                            + "  from    precintado a, precintos b"
                                            + "  where   a.id_precinto = b.identificador"
                                            + "  and     b.id_tipo_precinto = 4 "
                                            + "  and     a.id_movimiento_cab = " + identificador
                                            + " order by b.numero asc " ;
                                    List<String[]> lista = DBTrasactions.retList(sql);
                                    if(lista==null)
                                        lista = new ArrayList<String[]>();
                                %>                                
                                <th align="center">
                                    
                                <%
                                    if(lista.size()>0){
                                        out.print("ADUANA PY ("+lista.size()+")");
                                
                                 }
                                
                                %>
                                    
                                </th>
                            </tr>
                            <tr>
                                <td>
                               <table style="margin: 0px auto;" border="1" >
                                    <tr>
                                        <%
                                            for (int idx = 0; idx < lista.size(); idx++) {
                                                String[] elem = lista.get(idx);
                                                System.out.println(elem[0]);
                                                if (idx % 10 == 0 && idx > 0) {
                                                    out.println("</tr><tr>");
                                                }
                                                out.println("<td  width=\"50px\">" + elem[0] + "</td>");
                                            }
                                        %>
                                    </tr>
                               </table>
                                    </td>
                            </tr>
                        </table>
                        <table width="500px" border=1 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr width="500px" >
                                <%
                                     sql = " select  b.codigo_barras "
                                            + "  from    precintado a, precintos b"
                                            + "  where   a.id_precinto = b.identificador"
                                            + "  and     b.id_tipo_precinto = 3 "
                                            + "  and     a.id_movimiento_cab = " + identificador
                                             + " order by b.numero asc " ;
                                      lista = DBTrasactions.retList(sql);
                                      if(lista==null)
                                        lista = new ArrayList<String[]>();
                                %>                                
                                <th align="center">
                                <%
                                    if(lista.size()>0){
                                        out.print("CLIENTE ("+lista.size()+")");
                                
                                 }
                                
                                %>
                                </th>
                            </tr>
                            <tr>
                                <td>
                               <table style="margin: 0px auto;" border="1" >
                                    <tr>
                                        <%
                                            for (int idx = 0; idx < lista.size(); idx++) {
                                                String[] elem = lista.get(idx);
                                                System.out.println(elem[0]);
                                                if (idx % 10 == 0 && idx > 0) {
                                                    out.println("</tr><tr>");
                                                }
                                                out.println("<td  width=\"50px\">" + elem[0] + "</td>");
                                            }
                                        %>
                                    </tr>
                               </table>
                                    </td>
                            </tr>
                        </table>
                    </td>

                </tr>
            </table>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <table style="margin: 0px auto; width:90%;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    
                    <td align="center">...............................</td>
                    <td align="center">...............................</td>
                    <td align="center">...............................</td>

                </tr>
                <tr>
                    
                    <td><% out.print(cliente.trim()); %></td>
                    <td>Aduanas</td>
                    <td>Capitán 1er. Oficial</td>
                </tr>
            </table>
                                                <br><br><br><br><br><br>

        <br>
            <br>
            <br>
            <br>
             <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    Elaborado por Puerto Unión S.A.
             </div>
                    
            <br>
            <br>
            <br>
            <br>
            <div id="texto" style="text-align: left; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    Nro. <%=lis.get(0)[3]%>
            </div>
        </div>

        <center>
            <button onclick="javascript:printDiv()" id="print" type="button" name="print" class="btn btn-success">Imprimir</button>
        </center>
    
        <br><br><br><br><br><br>
        <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                    NRO. <%=lis.get(0)[3]%>
        </div>
    </body>
    <script>
        function printDiv()
        {
            
            var divToPrint = document.getElementById('impresion');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
        }

    </script>
</html>