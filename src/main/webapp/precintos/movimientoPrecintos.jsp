<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        String strIde = request.getParameter("identificador");
        List<String[]> lis = null;
        String[] valores = null;
        int identificador = 0;
        try{
            identificador = Integer.parseInt(strIde );
             lis= DBTrasactions.retList("select * from operaciones_cab where identificador = "+identificador);
             valores = lis.get(0);
            
        }catch(Exception ex){

        }
    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <div class="container">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                     <legend style="color:rgb(83,156,239);font-size:20pt;font-family:Trebuchet MS">Entrada/Salida de Precintos</legend>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="fecha">Fecha</label>  
                        
                        <div id="datepicker" class="col-md-4 input-group date" data-date-format="yyyy-mm-dd">
                            <input id="fecha" class="form-control" type="text" readonly 
                                   value="<%=(identificador>0)?valores[1]:""%>" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </div>               
                        <label class="col-md-2 control-label" for="id_almacen">Almacen</label>  
                        <div class="col-md-4">
                        <select class="form-control" id="id_almacen" <%=(identificador>0)?"disabled":""%>>
                            <% 
                                List<String[]> slista = DBTrasactions.retList("select identificador, nombre from almacenes");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[4])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="comentarios">Comentarios</label>
                        <div class="col-md-4">                     
                            <textarea class="form-control" id="comentarios" name="comentarios"
                                   <%=(identificador>0)?"disabled":""%>><%=(identificador>0)?valores[3]:""%></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>
                        <div class="col-md-8">
                            <%= (identificador<=0)?"<button id=\"button1id\" type=\"button\" name=\"button1id\" class=\"btn btn-success\">Agregar Detalle</button>":"" %>
                            <!--<button id="button2id" name="button2id" class="btn btn-danger">Cancelar</button>-->
                        </div>
                    </div>
                    <div id="detalle"  style="display: <%= (identificador<=0)?"none":"block" %>;" >
                          <div class="input-group">
                              <label class="control-label" for="id_tipo_movimiento">Tipo Movimiento: </label>  
                                <select class="form-control" id="id_tipo_movimiento">
                                <% 
                                    List<String[]> tlista = DBTrasactions.retList("select identificador, descripcion, operacion from tipos_movimiento");
                                    for (int idx = 0; idx < tlista.size(); idx++) {
                                        String elem[] = tlista.get(idx);
                                        out.print("<option value='"+elem[0]+"-"+elem[2]+"'>" + elem[1] + "</option>");
                                    }

                                %>
                              </select>  
                              <label class="control-label" for="id_tipo_precinto">Tipo Precinto: </label>  
                                <select class="form-control" id="id_tipo_precinto">
                                <% 
                                    List<String[]> tlista2 = DBTrasactions.retList("select identificador, descripcion from tipos_precinto");
                                    for (int idx = 0; idx < tlista2.size(); idx++) {
                                        String elem[] = tlista2.get(idx);
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }

                                %>
                              </select>  
                                <label class="control-label" for="nro">Nro. Precinto: </label>  
                                <input id="nro" name="nro" type="text" placeholder="" class="col-md-2 form-control input-sm">
                              
                                <button id="agregardetalle" type="button" name="agregardetalle" class="btn btn-success">Agregar</button>
                        </div>
                    </div>

                </fieldset>
            </form>
            <div class="row col-md-11 col-md-offset-1 custyle">
               <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tipo de Precintos</th>
                            <th>Nro. de precinto</th>
                            <th>Movimiento</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <% 
                        String query = "select b.identificador, c.descripcion, b.codigo_barras, d.descripcion "+
                                       "from operaciones_det a, precintos b, tipos_precinto c, tipos_movimiento d "+
                                       "where a.id_precinto =  b.identificador "+
                                       "and c.identificador = b.id_tipo_precinto "+
                                       "and a.id_tipo_movimiento = d.identificador "+
                                       "and a.id_operacion = "+identificador;
                        
                        List<String[]> lista = DBTrasactions.retList(query);
                        for (int idx = 0; idx < lista.size(); idx++) {
                            Object elem[] = lista.get(idx);
                            out.print(" <tr>");
                            for (int id = 0; id < elem.length; id++) {
                                Object e = elem[id];
                                out.print("<td>" + e + "</td>");

                            }
                            String botonBorrar = "<td class=\"text-center\"><button href=\"#\" onclick=\"javascript:borrarFila("+elem[0]+")\" class=\"btn btn-danger btn-xs buttoncancel\"></button></td>";
                            out.print(botonBorrar);
                            out.print("</tr>");

                        }
                         
                    %>  
                </table>
            </div>

        </div>

    </body>
    <script>
   
        function InitOverviewDataTable()
               {

            var table = $('#example').dataTable(
                    {
                        "language": {
                        "url": "inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 5
                    });
        }


        $(document).ready(function () {
            InitOverviewDataTable();

        });
        $(function () {
            $("#datepicker").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
        $('#button1id').click(function(){
           
           var comentario = ($('#comentarios').val())?$('#comentarios').val():null;
            var text = 'fecha:string:'+$('#fecha').val()+";"+
                    'id_almacen:int:'+$('#id_almacen').val()+";"+
                    'comentarios:string:'+comentario+";";
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processServlet",
                data: {
                    values: encoded,
                    class: "operaciones_cab",
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/movimientoPrecintos.jsp?identificador='+ result,
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        });
        
              
        $('#agregardetalle').click(function(){
            var text =  ''+$('#id_tipo_movimiento').val()+";"+
                        ''+$('#id_tipo_precinto').val()+";"+
                        ''+$('#nro').val()+";";
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processServletDetalleMovimiento",
                data: {
                    values: encoded,
                    class: "precintos",
                    id:<%=identificador%>,
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/movimientoPrecintos.jsp?identificador=<%=identificador%>',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        });
        


    </script>

</html>

