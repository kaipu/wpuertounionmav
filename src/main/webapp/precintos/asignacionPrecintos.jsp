<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        String strIde = request.getParameter("identificador");
        List<String[]> lis = null;
        String[] valores = null;
        int identificador = 0;
        String estado="";
        HttpSession ss  = request.getSession();        
        String user = ss.getAttribute("userlogon").toString();
        boolean esAdmin = DBTrasactions.retBoolean("SELECT * FROM cargadores where usuario ='"+user+"' and  admin = 1");
                
        try{
            identificador = Integer.parseInt(strIde );
             lis= DBTrasactions.retList("select * from movimientos_cab where identificador = "+identificador);
             valores = lis.get(0);
             estado = valores[13];
        }catch(Exception ex){

        }
    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>    
    <body>
        <div class="container">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend style="color:rgb(83,156,239);font-size:20pt;font-family:Trebuchet MS">Asignacion de Precintos</legend>
                 
                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="numero">Nro.</label>  
                        <div class="col-md-3">
                            <input id="numero" name="numero" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[1]:""%>" 
                                   <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>

                        </div>
                    
                        <label class="col-md-2 control-label" for="fecha">Fecha</label>  
                        
                        <div id="datepicker" class="col-md-3 input-group date" data-date-format="yyyy-mm-dd">
                            <input id="fecha" class="form-control" type="text" readonly 
                                   value="<%=(identificador>0 && estado.equals("PR"))?valores[2]:""%>" />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </div>  
                    </div>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="id_cliente">Cliente</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="id_cliente" <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                            <% 
                                List<String[]> slista = DBTrasactions.retList("select c.identificador, p.denominacion from clientes c, personas p where p.identificador=c.id_persona");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[3])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>

                        <label class="col-md-2 control-label" for="id_operario">Precintador</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="id_operario" <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                            <% 
                                slista = DBTrasactions.retList("select c.identificador, p.denominacion from operarios c, personas p where p.identificador=c.id_persona");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[8])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="id_barcaza">Barcaza</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="id_barcaza" <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                            <% 
                                slista = DBTrasactions.retList("select identificador, nro_chapa from barcazas");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[5])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>
                        <label class="col-md-2 control-label" for="destino">Destino</label>  
                        <div class="col-md-3">
                            <input id="destino" name="destino" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[6]:""%>" 
                                   <%=(identificador>0 && !esAdmin)?"disabled":""%>>
                        </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="select">Producto</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="id_producto" <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                            <% 
                                slista = DBTrasactions.retList("select identificador, descripcion from productos");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[10])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>
                        <label class="col-md-2 control-label" for="peso">Peso</label>  
                        <div class="col-md-3">
                            <input id="peso" name="peso" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[11]:""%>" 
                                   <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                        </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="id_almacen">Almacen</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="id_almacen" <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                            <% 
                                slista = DBTrasactions.retList("select identificador, nombre from almacenes");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[4])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>
                        <label class="col-md-2 control-label" for="comentarios">Despacho</label>  
                        <div class="col-md-3">
                            <input id="comentarios" name="comentarios" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[7]:""%>" 
                                   <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                        </div>
                    </div>
                    <div class="input-group">    
                    <label class="col-md-2 control-label" for="tipo_asignacion">Tipo Asignacion</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="tipo_asignacion" <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                            <% 
                                if(identificador>0 && valores[12].equals("DE") ){
                                    out.print("<option value='DE' selected> Desasignar Precintos </option>");
                                }else{
                                    if(identificador>0 && valores[12].equals("AS")){
                                        out.print("<option value='AS' selected> Asignar Precintos </option>");
                                    }
                                    else
                                    {
                                        out.print("<option value='DE' selected> Desprecintado </option>");
                                        out.print("<option value='PR' selected> Precintado </option>");
                                        
                                    }
                                }

                            %>
                          </select>    
                        </div>
                        <label class="col-md-2 control-label" for="despacho_compl_1">Despacho Compl 1</label>  
                        <div class="col-md-3">
                            <input id="despacho_compl_1" name="despacho_compl_1" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[14]:""%>" 
                                   <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                        </div>  
                    </div> 
                    <div class="input-group">    
                        <label class="col-md-2 control-label" for="peso_compl_1">Peso Compl.1</label>                      
                        <div class="col-md-3">
                            <input id="peso_compl_1" name="peso_compl_1" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[15]:""%>" 
                                   <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                        </div>
                        <label class="col-md-2 control-label" for="despacho_compl_2">Despacho Compl.2</label>                      
                        
                        <div class="col-md-3">
                            <input id="despacho_compl_2" name="despacho_compl_2" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[16]:""%>" 
                                   <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="peso_compl_2">Peso Compl.2</label>                      
                        <div class="col-md-3">
                            <input id="peso_compl_2" name="peso_compl_2" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[17]:""%>" 
                                   <%=(identificador>0 && estado.equals("PR") && !esAdmin)?"disabled":""%>>
                        </div>
                        
                    </div>  
                    <!-- Text input-->
                    <div class="form-group">
                        <div class="col-md-4">
                        <%= (identificador<=0 )?"<button id=\"button1id\" type=\"button\" "
                                + "name=\"button1id\" class=\"btn btn-success\">Agregar Detalle</button>":
                                    "<button id=\"buttonUpdate\" type=\"button\" name=\"buttonUpdate\" "
                                + "class=\"btn btn-success\">Actualizar Asignacion</button>" %>
                            <!--<button id="button2id" name="button2id" class="btn btn-danger">Cancelar</button>-->
                        </div>
                    
                        
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        <%= (!esAdmin )?"":
                                
                                    "<button id=\"buttonUpdate\" type=\"button\" name=\"buttonUpdate\" "
                                + "onclick=\"javascript:borrarAsignacion("+identificador+")\"class=\"btn btn-success\">Eliminar Asignacion</button>" 
                        
                        %>
                                
                            <!--<button id="button2id" name="button2id" class="btn btn-danger">Cancelar</button>-->
                        </div>
                    </div>
                            
                     <div id="detalle"  style="display: <%= (identificador<=0)?"none":"block" %>;" >
                          <div class="input-group">
                              <label class="col-md-2 control-label" for="id_tipo_precinto">Tipo Precinto</label>  
                                <select class="col-md-2 control-label" id="id_tipo_precinto">
                                <% 
                                    List<String[]> tlista = DBTrasactions.retList("select identificador, descripcion from tipos_precinto");
                                    for (int idx = 0; idx < tlista.size(); idx++) {
                                        String elem[] = tlista.get(idx);
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }

                                %>
                              </select>  
                                <label class="col-md-2 control-label" for="nroinicial">Nro. Precinto Inicial</label>  
                                <input id="nroinicial" name="nroinicial" type="text" placeholder="" class="col-md-2 form-control input-sm">
                              
                                <label class="col-md-2 control-label" for="nrofinal">Nro. Precinto Final</label>  
                                <input id="nrofinal" name="nrofinal" type="text" placeholder="" class="col-md-2 form-control input-sm">
                 
                            <button id="agregardetalle" type="button" name="agregardetalle" class="btn btn-success">Agregar</button>
                        </div>
                    </div>
                    
                </fieldset>
            </form>
            <div class="row col-md-8 col-md-offset-2 custyle">
               <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tipo de Precintos</th>
                            <th>Nro. de precinto</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <% 
                        String query = "select a.identificador, c.descripcion, b.codigo_barras, b.identificador "+
                                       "from  movimientos_det a, precintos b, tipos_precinto c "+
                                       "where a.id_precinto =  b.identificador "+
                                       "and c.identificador = a.id_tipo_precinto "+
                                       "and a.id_movimiento_cab = "+identificador;
                        
                        List<String[]> lista = DBTrasactions.retList(query);
                        for (int idx = 0; idx < lista.size(); idx++) {
                            Object elem[] = lista.get(idx);
                            out.print(" <tr>");
                            for (int id = 0; id < elem.length-1; id++) {
                                Object e = elem[id];
                                out.print("<td>" + e + "</td>");

                            }
                            String botonBorrar = "<td class=\"text-center\"><button href=\"#\" onclick=\"javascript:borrarFila("+elem[0]+","+elem[3]+")\" class=\"btn btn-danger btn-xs buttoncancel\"></button></td>";
                            out.print(botonBorrar);
                            out.print("</tr>");

                        }
                         
                    %>               
                </table>
            </div>

        </div>
    </body>
    
    <script>
        function InitOverviewDataTable()
                {

            var table = $('#example').dataTable(
                    {
                        "language": {
                        "url": "inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 5
                    });
        }



        $(document).ready(function () {
            InitOverviewDataTable();

        });
        $(function () {
            $("#datepicker").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
        
        $('#buttonUpdate').click(function(){
            var comentario = ($('#comentarios').val())?$('#comentarios').val():null;
            var despacho_compl_1 = ($('#despacho_compl_1').val())?$('#despacho_compl_1').val():null;
            var despacho_compl_2 = ($('#despacho_compl_2').val())?$('#despacho_compl_2').val():null;
            var peso_compl_1 = ($('#peso_compl_1').val())?$('#peso_compl_1').val():null;
            var peso_compl_2 = ($('#peso_compl_2').val())?$('#peso_compl_2').val():null;
            //var remolcador = ($('#remolcador').val())?$('#remolcador').val():null;
           
            var text = 'numero:string:'+$('#numero').val()+";"+
                    'fecha:string:'+$('#fecha').val()+";"+
                    'id_cliente:int:'+$('#id_cliente').val()+";"+
                    'id_operario:int:'+$('#id_operario').val()+";"+
                    'id_barcaza:int:'+$('#id_barcaza').val()+";"+
                    'destino:string:'+$('#destino').val()+";"+
                    'id_producto:int:'+$('#id_producto').val()+";"+
                    'id_almacen:int:'+$('#id_almacen').val()+";"+
                    'tipo_asignacion:string:'+$('#tipo_asignacion').val()+";"+
                    'peso:int:'+$('#peso').val()+";"+
                    'comentarios:string:'+comentario+";"+
                    'despacho_compl_1:string:'+despacho_compl_1+";"+
                    'despacho_compl_2:string:'+despacho_compl_2+";"+
                    'peso_compl_1:int:'+peso_compl_1+";"+
                    'peso_compl_2:int:'+peso_compl_2+";"+
                    
                    'estado:string:AS;'+
                    "id_tipo_movimiento:int:(select identificador from tipos_movimiento where codigo = 'AS');";
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processUpdate",
                data: {
                    values: encoded,
                    class: "movimientos_cab",
                    id:<%=identificador%>,
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/asignacionPrecintos.jsp?identificador=<%=identificador%>',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        });  
        $('#button1id').click(function(){
            var comentario = ($('#comentarios').val())?$('#comentarios').val():null;
            var despacho_compl_1 = ($('#despacho_compl_1').val())?$('#despacho_compl_1').val():null;
            var despacho_compl_2 = ($('#despacho_compl_2').val())?$('#despacho_compl_2').val():null;
            var peso_compl_1 = ($('#peso_compl_1').val())?$('#peso_compl_1').val():null;
            var peso_compl_2 = ($('#peso_compl_2').val())?$('#peso_compl_2').val():null;
            var remolcador = ($('#remolcador').val())?$('#remolcador').val():null;
           
            var text = 'numero:string:'+$('#numero').val()+";"+
                    'fecha:string:'+$('#fecha').val()+";"+
                    'id_cliente:int:'+$('#id_cliente').val()+";"+
                    'id_operario:int:'+$('#id_operario').val()+";"+
                    'id_barcaza:int:'+$('#id_barcaza').val()+";"+
                    'destino:string:'+$('#destino').val()+";"+
                    'id_producto:int:'+$('#id_producto').val()+";"+
                    'id_almacen:int:'+$('#id_almacen').val()+";"+
                    'tipo_asignacion:string:'+$('#tipo_asignacion').val()+";"+
                    'peso:int:'+$('#peso').val()+";"+
                    'comentarios:string:'+comentario+";"+
                    'despacho_compl_1:string:'+despacho_compl_1+";"+
                    'despacho_compl_2:string:'+despacho_compl_2+";"+
                    'peso_compl_1:int:'+peso_compl_1+";"+
                    'peso_compl_2:int:'+peso_compl_2+";"+
                    'remolcador:string:'+remolcador+";"+
                    'estado:string:AS;'+
                    "id_tipo_movimiento:int:(select identificador from tipos_movimiento where codigo = 'AS');";
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processServlet",
                data: {
                    values: encoded,
                    class: "movimientos_cab",
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/asignacionPrecintos.jsp?identificador='+ result,
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        });
        function borrarFila(id,codigo) {
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "deletePrecinto",
                data: {
                    id: id,
                    class: "movimientos_det",
                    estado: "AC",
                    codigo: codigo
                },
                success: function (result) {
                    alert("Registro eliminado exitosamente.");

                    $.ajax({url: 'precintos/asignacionPrecintos.jsp?identificador=<%=identificador%>',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }
        
        function borrarAsignacion(id) {
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "deleteDataDetalle",
                data: {
                    id: id,
                    class: "movimientos_cab"
                },
                success: function (result) {
                    alert("Registro eliminado exitosamente.");

                    $.ajax({url: 'precintos/listaAsignaciones.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }
        
              
        $('#agregardetalle').click(function(){
           
            var text = 'nroinicial:'+$('#nroinicial').val()+";"+
                    'nrofinal:'+$('#nrofinal').val()+";";
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processServletDetalleAsignacion",
                data: {
                    values: encoded,
                    class: "movimientos_det",
                    id:<%=identificador%>,
                    id_tipo_precinto:$('#id_tipo_precinto').val(),
                    consulta: "movimientos_det"
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/asignacionPrecintos.jsp?identificador=<%=identificador%>',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('Error:'+result);
                }
            });
        });
        


    </script>
    
</html>
