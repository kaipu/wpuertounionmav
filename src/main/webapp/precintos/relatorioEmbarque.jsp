<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        String strIde = request.getParameter("identificador");
        List<String[]> lis = null;
        String[] valores = null;
        int identificador = 0;
        String estado="";
        HttpSession ss  = request.getSession();        
        String user = ss.getAttribute("userlogon").toString();
        boolean esAdmin = DBTrasactions.retBoolean("SELECT * FROM cargadores "
                + "where usuario ='"+user+"' and  admin = 1");
                
        try{
            identificador = Integer.parseInt(strIde );
             lis= DBTrasactions.retList("select identificador, nombre_remolcador, "
                     + "date_format(fecha_arribo,'%Y-%m-%d %H:%i:%S'), date_format(fecha_inicio,'%Y-%m-%d %H:%i:%S'),  date_format(fecha_fin,'%Y-%m-%d %H:%i:%S'), id_producto,"
                     + "id_barcaza, peso from relatorios_embarque_cab "
                     + "where identificador = "+identificador);
             valores = lis.get(0);
        }catch(Exception ex){

        }
    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>    
    <body>
        <div class="container">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend style="color:rgb(83,156,239);font-size:20pt;font-family:Trebuchet MS">Relatorio de Embarque</legend>
                 
                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="nombre_remolcador">Remolcador</label>  
                        <div class="col-md-3">
                            <input id="nombre_remolcador" name="nombre_remolcador" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[1]:""%>" 
                                   <%=(identificador>0 && !esAdmin)?"disabled":""%>/>
                        </div>
                        <label class="col-md-2 control-label" for="fecha_arribo">Fecha arribo</label>  
                        
                        <div id="datepicker" class="col-md-3 input-group">
                            <input id="fecha_arribo" class="form-control" type="text" 
                                   value="<%=(identificador>0)?valores[2]:""%>"
                                   <%=(identificador>0 && !esAdmin)?"disabled":""%>/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </div> 
                    </div>
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="fecha_inicio">Fecha Inicio</label>  
                        
                        <div id="datepicker1" class="col-md-3 input-group">
                            <input id="fecha_inicio" class="form-control" type="text" 
                                   value="<%=(identificador>0)?valores[3]:""%>"
                                    <%=(identificador>0 && !esAdmin)?"disabled":""%>/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </div>
                        <label class="col-md-2 control-label" for="fecha_fin">Fecha fin</label>  
                        
                        <div id="datepicker2" class="col-md-3 input-group">
                            <input id="fecha_fin" class="form-control" type="text" 
                                   value="<%=(identificador>0)?valores[4]:""%>"
                                    <%=(identificador>0 && !esAdmin)?"disabled":""%>/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                            </span>
                        </div>   
                    </div>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="id_producto">Producto</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="id_producto" <%=(identificador>0 && !esAdmin)?"disabled":""%>>
                            <% 
                                List<String[]> slista = DBTrasactions.retList("select identificador, descripcion from productos");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[5])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>

                        <label class="col-md-2 control-label" for="id_barcaza">Barcaza</label>  
                        <div class="col-md-3">
                        <select class="form-control" id="id_barcaza" <%=(identificador>0 && !esAdmin)?"disabled":""%>>
                            <% 
                                slista = DBTrasactions.retList("select identificador, nro_chapa from barcazas");
                                for (int idx = 0; idx < slista.size(); idx++) {
                                    Object elem[] = slista.get(idx);
                                    if(identificador>0 && elem[0].equals(valores[6])){
                                        out.print("<option value='"+elem[0]+"' selected>" + elem[1] + "</option>");
                                    }else{
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }
                                }

                            %>
                          </select>    
                        </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="input-group">
                          
                        <label class="col-md-2 control-label" for="peso">Cantidad</label>  
                        <div class="col-md-3">
                            <input id="peso" name="peso" type="text" placeholder="" class="form-control input-sm" required
                                   value="<%=(identificador>0)?valores[7]:""%>" 
                                   <%=(identificador>0 && !esAdmin)?"disabled":""%>>
                        </div>
                    </div>
                    
                      
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>
                        <div class="col-md-8">
                        <%= (identificador<=0 )?"<button id=\"button1id\" type=\"button\" "
                                + "name=\"button1id\" class=\"btn btn-success\">Agregar Detalle</button>":
                                    "<button id=\"buttonUpdate\" type=\"button\" name=\"buttonUpdate\" "
                                + "class=\"btn btn-success\">Actualizar</button>" %>
                            <!--<button id="button2id" name="button2id" class="btn btn-danger">Cancelar</button>-->
                        </div>
                    
                        <label class="col-md-4 control-label" for="button1id"></label>
                        <div class="col-md-8">
                        <%= (identificador<=0 )?"<button id=\"button1id\" type=\"button\" "
                                + "name=\"button3id\" class=\"btn btn-success\">Eliminar </button>":
                                    "<button id=\"buttonUpdate\" type=\"button\" name=\"buttonUpdate\" "
                                + "onclick=\"javascript:borrarRelatorio("+identificador+")\"class=\"btn btn-success\">Eliminar Relatorio</button>" %>
                                
                            <!--<button id="button2id" name="button2id" class="btn btn-danger">Cancelar</button>-->
                        </div>
                    </div>
                            
                     <div id="detalle"  style="display: <%= (identificador<=0)?"none":"block" %>;" >
                          <div class="input-group">
                              <label class="col-md-1 control-label" for="id_actividad">Actividad</label>  
                                <select class="col-md-2 control-label" id="id_actividad">
                                <% 
                                    List<String[]> tlista = DBTrasactions.retList("select identificador, "
                                            + "descripcion from actividades_barcaza order by descripcion");
                                    for (int idx = 0; idx < tlista.size(); idx++) {
                                        String elem[] = tlista.get(idx);
                                        out.print("<option value='"+elem[0]+"'>" + elem[1] + "</option>");
                                    }

                                %>
                              </select>  
                              <label class="col-md-2 control-label" for="fecha">Fecha</label>  
                        
                              <div id="datepicker4" class="col-md-2 input-group date" data-date-format="yyyy-mm-dd">
                                <input id="fecha" class="form-control" type="text" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i>
                                </span>
                              </div> <label class="col-md-1 control-label" for="hora_inicio">Hora Inicio</label>  
                        
                              <input id="hora_inicio" name="hora_inicio" type="text" placeholder="" class="form-control input-md-1" required
                                   value=""/>
                              
                              <label class="col-md-1 control-label" for="hora_fin">Hora Fin</label>  
                        
                              <input id="hora_fin" name="hora_fin" type="text" placeholder="" class="form-control input-md-1" required
                                   value=""/>
  
                            <button id="agregardetalle" type="button" name="agregardetalle" class="btn btn-success">Agregar Detalle</button>
                        </div>
                    </div>
                    
                </fieldset>
            </form>
            <div class="row col-md-8 col-md-offset-2 custyle">
               <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Actividad</th>
                            <th>Fecha</th>
                            <th>Hora Inicio</th>
                            <th>Hora Fin</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <% 
                        String query = "select d.identificador, a.descripcion, d.fecha, "
                                + "d.hora_inicio, d.hora_fin "
                                + "from actividades_barcaza a, relatorios_embarque_det d "
                                + "where a.identificador = d.id_actividad "
                                + "and d.id_relatorio_cab = "+identificador+" order by d.fecha, d.hora_inicio asc ";
                        
                        List<String[]> lista = DBTrasactions.retList(query);
                        for (int idx = 0; idx < lista.size(); idx++) {
                            Object elem[] = lista.get(idx);
                            out.print(" <tr>");
                            for (int id = 0; id < elem.length; id++) {
                                Object e = elem[id];
                                out.print("<td>" + e + "</td>");

                            }
                            String botonBorrar = "<td class=\"text-center\"><button href=\"#\" onclick=\"javascript:borrarFila("+elem[0]+")\" class=\"btn btn-danger btn-xs buttoncancel\"></button></td>";
                            out.print(botonBorrar);
                            out.print("</tr>");

                        }
                         
                    %>               
                </table>
            </div>

        </div>
    </body>
    
    <script>
        function InitOverviewDataTable()
                {

            var table = $('#example').dataTable(
                    {
                        "language": {
                        "url": "inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 5
                    });
        }



        $(document).ready(function () {
            InitOverviewDataTable();

        });
/*
        $(function () {
            $("#datepicker").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
          $(function () {
            $("#datepicker1").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
        $(function () {
            $("#datepicker2").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
        $(function () {
            $("#datepicker3").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        
    */
        $(function () {
            $("#datepicker4").datepicker({ 
                  autoclose: true, 
                  todayHighlight: true
            }).datepicker('update', new Date());
          });        

        $('#buttonUpdate').click(function(){
            /*var comentario = ($('#comentarios').val())?$('#comentarios').val():null;
            var despacho_compl_1 = ($('#despacho_compl_1').val())?$('#despacho_compl_1').val():null;
            var despacho_compl_2 = ($('#despacho_compl_2').val())?$('#despacho_compl_2').val():null;
            var peso_compl_1 = ($('#peso_compl_1').val())?$('#peso_compl_1').val():null;
            var peso_compl_2 = ($('#peso_compl_2').val())?$('#peso_compl_2').val():null;
           */
            var text = 'nombre_remolcador:string:'+$('#nombre_remolcador').val()+";"+
                    'fecha_arribo:string:'+$('#fecha_arribo').val().replace(":","=")+";"+
                    'fecha_inicio:string:'+$('#fecha_inicio').val().replace(":","=")+";"+
                    'fecha_fin:string:'+$('#fecha_fin').val().replace(":","=")+";"+
                    'id_producto:int:'+$('#id_producto').val().replace(":","=")+";"+
                    'id_barcaza:int:'+$('#id_barcaza').val().replace(":","=")+";"+
                    'peso:int:'+$('#peso').val();
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processUpdate",
                data: {
                    values: encoded,
                    class: "relatorios_embarque_cab",
                    id:<%=identificador%>,
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/relatorioEmbarque.jsp?identificador=<%=identificador%>',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        });  
        $('#button1id').click(function(){
            /*var comentario = ($('#comentarios').val())?$('#comentarios').val():null;
            var despacho_compl_1 = ($('#despacho_compl_1').val())?$('#despacho_compl_1').val():null;
            var despacho_compl_2 = ($('#despacho_compl_2').val())?$('#despacho_compl_2').val():null;
            var peso_compl_1 = ($('#peso_compl_1').val())?$('#peso_compl_1').val():null;
            var peso_compl_2 = ($('#peso_compl_2').val())?$('#peso_compl_2').val():null;
           */
            var text = 'nombre_remolcador:string:'+$('#nombre_remolcador').val()+";"+
                    'fecha_arribo:string:'+$('#fecha_arribo').val()+";"+
                    'fecha_inicio:string:'+$('#fecha_inicio').val()+";"+
                    'fecha_fin:string:'+$('#fecha_fin').val()+";"+
                    'id_producto:int:'+$('#id_producto').val()+";"+
                    'id_barcaza:int:'+$('#id_barcaza').val()+";"+
                    'peso:int:'+$('#peso').val();
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processServlet",
                data: {
                    values: encoded,
                    class: "relatorios_embarque_cab",
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/relatorioEmbarque.jsp?identificador='+ result,
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        });
        function borrarFila(id) {
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "deleteData",
                data: {
                    id: id,
                    class: "relatorios_embarque_det"
                },
                success: function (result) {
                    alert("Registro eliminado exitosamente.");

                    $.ajax({url: 'precintos/relatorioEmbarque.jsp?identificador=<%=identificador%>',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }
        
        function borrarRelatorio(id) {
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "deleteRelatorio",
                data: {
                    id: id,
                    class: "relatorios_embarque_cab"
                },
                success: function (result) {
                    alert("Registro eliminado exitosamente.");

                    $.ajax({url: 'precintos/relatorioEmbarque.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }
        
              
        $('#agregardetalle').click(function(){
           
            var text =  "";
            //alert(text);
            var encoded = encodeURIComponent(text);
            $.ajax({
                type: "POST",
                url: "processServletDetallaRelatorio",
                data: {
                    values: encoded,
                    class: "relatorios_embarque_det",
                    id:<%=identificador%>,
                    id_actividad:$('#id_actividad').val(),
                    fecha:$('#fecha').val(),
                    hora_inicio:$('#hora_inicio').val(),
                    hora_fin:$('#hora_fin').val(),
                    consulta: "relatorios_embarque_det"
                },
                success: function (result) {
                    alert("Registro agregado exitosamente. "+result);

                    $.ajax({url: 'precintos/relatorioEmbarque.jsp?identificador=<%=identificador%>',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('Error:'+result);
                }
            });
        });
        


    </script>
    
</html>
