<%-- 
    Document   : acta_precintado
    Created on : 16/03/2019, 06:31:28 AM
    Author     : user
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
        

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
        
        <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
        <script src="inet/datepicker.js"></script>
        
    </head>
    <style type="text/css">
        th{
          background-color: gray;
          text-align: center;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px; 
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <%
            HttpSession ss  = request.getSession();
            
            String strIde = request.getParameter("identificador");
            List<String[]> lis = null;
            List<String[]> lis2 = null;
            String[] valores = null;
            String[] valores1 = null;
            String cliente = "";
            int identificador = 0;
            try {
                identificador = Integer.parseInt(strIde);
                String sql = "select  date_format(a.fecha,'%d-%m-%Y'), "
                        + "d.denominacion, c.descripcion, a.numero, "
                        + "a.peso, e.nro_chapa, a.comentarios,  "
                        + "SUBSTR(a.comentarios, length(a.comentarios)-3, 4),  "
                        + "    coalesce(a.despacho_compl_1,''),  "
                        + "    coalesce(a.despacho_compl_2,''),   "
                        + "     coalesce(cast(a.peso_compl_1 AS char), ''), "
                        + "     coalesce(cast(a.peso_compl_2 as char) ,''), "
                        + " case c.imp_leyenda_acta when 'S' then 'ASEGURADO CONFORME A GMP+FSA' ELSE '' END"
                        + "   from    movimientos_cab a, clientes b, productos c, "
                        + "personas d, barcazas e "
                        + "   where   a.id_cliente = b.identificador "
                        + "   and     b.id_persona = d.identificador "
                        + "   and     a.id_barcaza = e.identificador "
                        + "   and     a.id_producto = c.identificador "
                        + "   and     a.identificador =  " + identificador
                        + " ";
                lis = DBTrasactions.retList(sql);
                valores = lis.get(0);
                
                sql = "select date_format(max(fecha),'%d-%m-%Y') "
                        + "from movimientos_cab  "
                        + "where identificador="+ identificador;
                lis2 = DBTrasactions.retList(sql);
                valores1 = lis2.get(0);
                cliente =lis.get(0)[1].trim();

            } catch (Exception ex) {
                    ex.printStackTrace();
            }
            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(valores1[0]);
            Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
            cal.setTime(date1);

            String str1 = lis.get(0)[7];
           // str1 = (str1!= null && str1.length()>5)?""+str1.substring(str1.length()-4,str1.length() ):"";
            
            String str2 = lis.get(0)[8];
            str2 = (str2!= null && str2.length()>5)?""+str2.substring(str2.length()-4,str2.length() ):"";
            
            String str3 = lis.get(0)[9];
            str3 = (str3!= null && str3.length()>5)?""+str3.substring(str3.length()-4,str3.length() ):"";

        %>        
        <div id="impresion" name="impresion">
            <div id="texto" style="text-align: center; text-justify: inter-word;width:100%; height: 50px; margin: auto;">
                    
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:100%; height: 50px; margin: auto;">
                    <b><%=lis.get(0)[1]%></b>
            </div>
            <div id="texto" style="text-align: center; text-justify: inter-word;width:100%; height: 50px; margin: auto;">
                    <b>ACTA DE PRECINTADO</b>
            </div>
            <div id="texto" style="font-size: 18px;text-align: justify; text-justify: inter-word;width:100%; margin: auto;">
                En Puerto Unión S.A., Zeballos Cué, Asunción, Republica del Paraguay, a los <%= cal.get(Calendar.DAY_OF_MONTH)%> 
                dias del mes de <%= cal.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("es", "ES"))%> del año 
                <%= cal.get(Calendar.YEAR)%>, en presencia del funcionario de <%=lis.get(0)[1]%>, 
                se procede al precintado de la barcaza que figura más abajo, transportando <%=lis.get(0)[2]%>.
                Amparados por el DESPACHO DE EXPORTACION PROVISORIO  <%=lis.get(0)[6]%> <%=lis.get(0)[8]%> <%=lis.get(0)[9]%> 
                <br/>
                <br/>
                <%=lis.get(0)[12]%> 
            </div>
            <br>
            <table style="margin: 0px auto;" border="1" cellpadding="0" cellspacing="0">
                <tr>
                    <th width="90px">EXP. No.</th>
                    <th width="90px">KILOS</th>
                    <th width="90px">BARCAZA</th>
                    <th width="500px">PRECINTOS</th>
                </tr>
                <tr>
                    <td>
                        <table border=0 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><%=str1%></td>
                            </tr>
                            <tr>
                                <td><%
                                    
                                    out.print(str2);
                                
                                %></td>
                            </tr>
                            <tr>
                                <td><%
                                    out.print(str3);    
                                    
                                
                                %></td>
                            </tr>
                        </table>
                        
                    
                    </td>
                    <td>
                        <table border=0 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><%
                                    String p0 = (lis.get(0)[4]!=null)?lis.get(0)[4]:"";
                                     p0 = p0.replace(".000", "");
                                     if(p0!=null && p0.length()>1){
                                        String pattern = "###,###";
                                        DecimalFormat decimalFormat = new DecimalFormat(pattern);

                                        String format = decimalFormat.format(Long.parseLong(p0));
                                        
                                        out.print(format);
                                     }    
                                    
                                
                                %></td>
                            </tr>
                            <tr>
                                <td><%
                                     String p1 = (lis.get(0)[10]!=null)?lis.get(0)[10]:"";
                                     p1 = p1.replace(".000", "");
                                     if(p1!=null && p1.length()>1){
                                        String pattern = "###,###";
                                        DecimalFormat decimalFormat = new DecimalFormat(pattern);

                                        String format = decimalFormat.format(Long.parseLong(p1));
                                        
                                        out.print(format);
                                     }
                                    %></td>
                            </tr>
                            <tr>
                                <td><%
                                    String p2 = (lis.get(0)[11]!=null)?lis.get(0)[11]:"";
                                     p2 = p2.replace(".000", "");
                                     
                                     if(p2!=null && p2.length()>1){
                                        String pattern = "###,###";
                                        DecimalFormat decimalFormat = new DecimalFormat(pattern);

                                        String format = decimalFormat.format(Long.parseLong(p2));
                                        
                                        out.print(format);
                                     }
                                         %></td>
                            </tr>
                        </table>
                        
                    </td>
                    <td><%=lis.get(0)[5]%></td>
                    <td>
                        <table width="800px" border=1 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr width="800px" >
                                <%
                                    String sql = " select  b.codigo_barras "
                                            + "  from    precintado a, precintos b"
                                            + "  where   a.id_precinto = b.identificador"
                                            + "  and     b.id_tipo_precinto = 4 "
                                            + "  and     a.id_movimiento_cab = " + identificador
                                            + " order by b.numero asc " ;
                                    List<String[]> lista = DBTrasactions.retList(sql);
                                    if(lista==null)
                                        lista = new ArrayList<String[]>();            

                                    sql = "select ((coalesce(a.nro_tapa,0)*10)+coalesce(a.nro_tapita,0)) as llave, c.codigo_barras, b.codigo "+
                                            " from precintado a, sectores_barcaza b, precintos c "+
                                            " where a.id_sector_barcaza = b.identificador"+
                                            " and c.identificador = a.id_precinto"+
                                            " and c.id_tipo_precinto = 4 "+
                                            " and     a.id_movimiento_cab = "+identificador+
                                            " order by c.numero asc " ;
                                    
                                    HashMap<String,List<String>> map = DBTrasactions.retHashMap(sql);
                                    
                                    sql = " select max(coalesce(a.nro_tapa,0)) "
                                            + "  from    precintado a"
                                            + "  where   a.id_movimiento_cab = " + identificador
                                            + "  " ;
                                    
                                    int max = DBTrasactions.retInt(sql);
                                %>                                
                                <th align="center">    
                                <%
                                    if(lista.size()>0){
                                        out.print("ADUANA PY ("+lista.size()+")");
                                
                                 }
                                
                                %>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <table style="margin: 0px auto;font-size: 18px;" border="1" >
                                         <tr>
                                             <%
                                                 for (int idx = 0; idx < lista.size(); idx++) {
                                                     String[] elem = lista.get(idx);
                                                     System.out.println(elem[0]);
                                                     if (idx % 10 == 0 && idx > 0) {
                                                         out.println("</tr><tr>");
                                                     }
                                                     out.println("<td  width=\"50px\">" + elem[0] + "</td>");
                                                 }
                                             %>
                                         </tr>
                                    </table>
                               </td>
                            </tr>
                        </table>
                        <table width="500px" border=1 style="margin: 0px auto;" cellpadding="0" cellspacing="0">
                            <tr width="500px" >
                                <%
                                     sql = " select  b.codigo_barras "
                                            + "  from    precintado a, precintos b"
                                            + "  where   a.id_precinto = b.identificador"
                                            + "  and     b.id_tipo_precinto = 3 "
                                            + "  and     a.id_movimiento_cab = " + identificador
                                             + " order by b.numero asc " ;
                                    List<String[]> lista2 = DBTrasactions.retList(sql);
                                    if(lista==null)
                                        lista = new ArrayList<String[]>();
                                %>                                
                                <th align="center">
                                <%
                                    if(lista2.size()>0){
                                        out.print("CLIENTE ("+lista2.size()+")");
                                
                                 }
                                
                                %>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <table style="margin: 0px auto;font-size: 18px;" border="1" >
                                         <tr>
                                             <%
                                                 for (int idx = 0; idx < lista2.size(); idx++) {
                                                     String[] elem = lista2.get(idx);
                                                     System.out.println(elem[0]);
                                                     if (idx % 10 == 0 && idx > 0) {
                                                         out.println("</tr><tr>");
                                                     }
                                                     out.println("<td  width=\"50px\">" + elem[0] + "</td>");
                                                 }
                                             %>
                                         </tr>
                                    </table>
                               </td>
                            </tr>
                        </table>

                    </td>

                </tr>

            </table>
            <br>

<center>
    <table style="border:1px solid black;padding: 10px;">

            <tr>
                <td>
                    <div class='rotate' style="font-size: 10px;">P</div>
                    <div class='rotate' style="font-size: 10px;">R</div>
                    <div class='rotate' style="font-size: 10px;">O</div>
                    <div class='rotate' style="font-size: 10px;">A</div>
                    
                </td>
                
                <%
                for (int idx = 1; idx <= max; idx++) {
                    if(map.get(idx+"0") == null && map.get(idx+"1") == null && map.get(idx+"2") == null){
                        continue;
                    }
                %>            
                <td width="55px" align="center">
                    <%=idx%>
                    
                    <table style="font-size: 10px;border:0px;">        
                    <%  
                        List<String> t = map.get(idx+"0");
                        
                            
                            if(t!=null && t.size()>0){
                                for (int idy = 0; idy < t.size(); idy++) {
                                        String a = (t.get(idy)==null)?"":t.get(idy) ;
                                        if(a.contains("ES"))
                                            out.println("<tr><td>"+a+"</td></tr>");

                                    }
                            }
                        

                    %>                        
                    </table>
                    <table style="border:1px dotted black;padding: 14px;" height="200px">
                        <tr>
                            <td height="50px" width="30px" style="font-size: 12px;">
                                <%=idx%>.1
                            </td>
                            <td style="border:1px solid black;font-size: 10px;" height="50px" width="30px">
                                <% 
                                    t = map.get(idx+"1");
                                    if(t!=null && t.size()>0){
                                        for (int idz = 0; idz < t.size(); idz++) {
                                                String a = (t.get(idz)==null)?"":t.get(idz) ;
                                                out.println(a);
                                                
                                            }
                                    }
                                    
                                
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td height="50px" width="30px"  style="font-size: 12px;">
                                <%=idx%>.2
                            </td>
                            <td style="border:1px solid black;font-size: 10px;" height="50px" width="30px">
                                <% 
                                    t = map.get(idx+"2");
                                    if(t!=null && t.size()>0){
                                        for (int idq = 0; idq < t.size(); idq++) {
                                                String a = (t.get(idq)==null)?"":t.get(idq) ;
                                                out.println(a);
                                                
                                            }
                                    }
                                    
                                
                                %>
                            </td>
                        </tr>
                    </table>
                    <table style="font-size: 10px;border:0px;">        
                    <% 
                        t = map.get(idx+"0");
                        if(t!=null && t.size()>0){
                            for (int idv = 0; idv < t.size(); idv++) {
                                    String a = (t.get(idv)==null)?"":t.get(idv) ;
                                    if(a.contains("BA"))
                                        out.println("<tr><td>"+a+"</td></tr>");

                                }
                        }


                    %>                        
                    </table>        
                </td>                        
                <%            
                }
                %>
                <td>
                    <div class='rotate' style="font-size: 10px;">P</div>
                    <div class='rotate' style="font-size: 10px;">O</div>
                    <div class='rotate' style="font-size: 10px;">P</div>
                    <div class='rotate' style="font-size: 10px;">A</div>
                </td>
            </tr>
    </table>
</center>


            <br>
            <br>
            <br>
            <table style="margin: 0px auto; width:90%;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    

                    <td align="center"><img src="images/<%=ss.getAttribute("userlogon").toString().toUpperCase() %>.png"  width="200" height="100" /></td>
                    <td align="center">&nbsp;</td>
                    <td align="center">&nbsp;</td>
                </tr>                
                <tr>
                    

                    <td align="center">...............................</td>
                    <td align="center">...............................</td>
                    <td align="center">...............................</td>
                </tr>
                <tr>
                    

                    <td align="center"><% out.print(cliente.trim()); %></td>
                    <td align="center">Aduanas</td>
                    <td align="center" >Capitán 1er. Oficial</td>
                </tr>
            </table>
                                                <br><br><br><br><br><br>

        <br>
            <br>
            <br>
            <br>
             <div id="texto" style="text-align: center; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                
                 <img src="images/<%=ss.getAttribute("userlogon").toString().toUpperCase() %>.png"  width="200" height="100" />
                 <br>
                 Elaborado por Puerto Unión S.A.
             </div>
                    
            <br>
            <br>
            <br>
            <br>
            <div id="texto" style="text-align: left; text-justify: inter-word;width:80%; height: 50px; margin: auto;">
                
                    Nro. <%=lis.get(0)[3]%>
            </div>
        </div>

        <center>
            <button onclick="javascript:printDiv()" id="print" type="button" name="print" class="btn btn-success">Imprimir</button>
        </center>

         
    </body>
    <script>
        function printDiv()
        {
            
            var divToPrint = document.getElementById('impresion');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
        }

    </script>
</html>