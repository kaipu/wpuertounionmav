
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");

    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <div class="container">
            <form class="form-horizontal panel-success">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Carga de Precintos</legend>

                    <!-- Select Basic -->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="selectbasic">Tipo de precinto</label>
                        <div class="col-md-4">
                            <select id="selectbasic" name="selectbasic" class="form-control">
                                <option value="AC">Aduana</option>
                                <option value="">Cliente</option>
                            </select>
                        </div>
                        <label class="col-md-2 control-label" for="cargadesde">Desde Precinto</label>  
                        <div class="col-md-4">
                            <input id="cargadesde" name="cargadesde" type="text" placeholder="" class="form-control input-sm" required>
                            <span class="help-block">Nro. de precinto inicial</span>  
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="cargahasta">Hasta Precinto</label>  
                        <div class="col-md-4">
                            <input id="cargahasta" name="cargahasta" type="text" placeholder="" class="form-control input-sm" required>
                            <span class="help-block">Nro. precinto final</span>  
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cargaAceptar"></label>
                        <div class="col-md-8">
                            <button id="cargaAceptar"  onclick="javascript:cargarPrecintos()" name="cargaAceptar" class="btn btn-success">Aceptar</button>
                            <button id="cargaCancelar" name="cargaCancelar" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>

                </fieldset>
            </form>
            <div class="row col-md-8 col-md-offset-2 custyle">
                <table class="table table-striped custab">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tipo de Precintos</th>
                        <th>Nro. de precinto</th>
                        <th class="text-center"></th>
                    </tr>
                    </thead>

                </table>
            </div>
        </div>

    </body>
    <script>

    function cargarPrecintos(){
        
    
    }
    </script>

</html>

