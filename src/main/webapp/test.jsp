<%@page import="py.com.fleming.visas.pojos.Zona"%>
<%@page import="py.com.fleming.visas.pojos.Departamento"%>
<%@page import="java.util.List"%>
<%@page import="py.com.fleming.visas.pojos.Especialidad"%>
<%@page import="py.com.fleming.visas.beans.VisaBean"%>

                
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="ncodmed">C�digo</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ncodmed" value="<%= VisaBean.getUltimoMedico() %>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="nnommed">Nombre</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="nnommed">
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="nespecmed">Especialidad</label>
                                <div class="col-sm-6">
                                    <select id="nespecmed" data-live-search="true">
                                        <%
                                            List<Especialidad> l =  VisaBean.getListaExpecialidades("null","null");
                                            for(int i=0;i<l.size();i++){
                                        %>
                                        <option value="<%= l.get(i).getNombre() %>"><%= l.get(i).getNombre() %></option>
                                        <%
                                            }
                                        %>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="ndep">Departamento</label>
                                <div class="col-sm-6">
                                    <select id="ndep" data-live-search="true">
                                        <%
                                            List<Departamento> ld =  VisaBean.getListaDepartamentos("null","null");
                                            for(int i=0;i<ld.size();i++){
                                        %>
                                        <option value="<%= ld.get(i).getCodigo()%>"><%= ld.get(i).getNombre_dpto()%></option>
                                        <%
                                            }
                                        %>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>       
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="nzon">Zona</label>
                                <div class="col-sm-6">
                                    <select id="nzon" data-live-search="true">
                                        <%
                                            List<Zona> lz =  VisaBean.getListaZonas("null","null");
                                            for(int i=0;i<lz.size();i++){
                                        %>
                                        <option value="<%= lz.get(i).getCodigo()%>"><%= lz.get(i).getNombre() %></option>
                                        <%
                                            }
                                        %>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>                                    
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                 <label class="col-sm-4 control-label" for="ndirmed">Direcci�n</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ndirmed">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                 <label class="col-sm-4 control-label" for="ntelmed">Tel�fono</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ntelmed">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                 <label class="col-sm-4 control-label">Prestador</label>
                                <div class="col-sm-2">
                                    <%
                                        String codpresta = (request.getParameter("codpresta")==null)?"":request.getParameter("codpresta").toString();
                                        String despresta = (request.getParameter("despresta")==null)?"":request.getParameter("despresta").toString();
                                    %>
                                    <input class="form-control" type="text" id="ncodpresta" value="<%=codpresta%>" readonly>
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ndespresta" value="<%=despresta%>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-lg-offset-1">
                            <div class="form-group form-group-sm">
                                <button type="button" class="btn btn-danger" id="cancelarmed" name="cancelarmed">
                                    Cancelar
                                </button>
                                <button type="button" class="btn btn-success" id="nuevomed" name="nuevomed">
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                                 
            <script>
                    
                $("#nuevomed").click(
                    function (){ 
                    var ncodmed = $("#ncodmed").val();
                    var nnommed = $("#nnommed").val();
                    var nespecmed = $("#nespecmed").val();
                    var ntelmed = $("#ntelmed").val();
                    var ncodpresta = $("#ncodpresta").val();
                    var ndirmed = $("#ndirmed").val();
                    var ndep = $("#ndep").val();
                    var nzon = $("#nzon").val();

                    if(ncodmed == "" || nnommed == ""){
                        alert("Debe llenar todos los campos");
                        return;
                    }
                    var data = "operacion=guardarmedic&ncodmed="+ncodmed+"&nnommed="+nnommed+"&nespecmed="+nespecmed+
                               "&ntelmed="+ntelmed+"&ncodpresta="+ncodpresta+"&ndirmed="+ndirmed+"&ndep="+ndep+"&nzon="+nzon;
                    $.ajax({
                        type: "POST",
                        url: "visa",
                        data: data,
                        success: function (result) {
                            if (typeof (result) !== 'undefined') {
                                $('#codmed').val(ncodmed);
                                $('#desmed').val(nnommed);
                                $( '#nuevomedicodiv').html("");                            
                                //$( '#cabecera').html(result);
                            }
                        },
                        error: function (error) {
                            console.log("error" + error);
                        }
                    });
                    }
                );  
                $("#cancelarmed").click(
                    function (){ 
                        $( '#nuevomedicodiv').html("");                            
                    }
                );
            </script>

        

