
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        String numvis = request.getParameter("numvisa");
        System.out.println(numvis);
        try{
            
        Visa v = null;
        if(numvis != null && numvis.length()>1){   
            v = VisaBean.getVisa(Long.parseLong(numvis));
        }
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");    

    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
         }
    </style>
    <body>
        <div class="container">
            <form class="form-horizontal">
                <div  id="cabecera">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="utlvisa">Último número de visa</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="utlvisa" value="<%= VisaBean.getUltimaVisa() %>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="numvisa">Nro. Visa</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="numvisa" value="<%= (v==null)?"":v.getCabecera().getNrovis() %>">
                                </div>
                                <button class="btn col-sm-2 glyphicon glyphicon-search" type="button" id="buscarvisa"></button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="fecliq">Fec. Liquidación</label>
                                <div class="col-sm-6  ">
                                    <input class="form-control" type="text" id="fecliq" readonly>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="fecvisa">Fecha</label>
                                <div class="col-sm-6">
                                    <input size="16" type="text" value="<%= (v==null)?"": dt1.format(v.getCabecera().getFecha()) %>" readonly class="form_datetime">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="numorden">Nro. Orden</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="numorden" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label" for="">Nueva visa</label>
                            <button type="button" class="btn glyphicon btn-success glyphicon-erase btn-lg" id="limpiarvisa"></button>
                        </div>
                    </div>
                </div>
                <%
                        System.out.println(numvis);
                %>
                <div class="row" style="background-color: blanchedalmond; border: 1px solid #eee;">
                        <div class="form-group form-group-sm">
                            <div class="col-sm-1">
                                <label class="control-label" for="adh">Adherente</label>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="adh">Adherente</label>
                                <input class="form-control" type="text" id="adh" value="<%= (v==null)?"":v.getCabecera().getAdh().getNroAdh() %>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <div class="col-sm-2">
                                <label class="control-label" for="ci">Cédula</label>
                                <input class="form-control" type="text" id="ci"  value="<%= (v==null)?"":v.getCabecera().getAdh().getNroCedula() %>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label" for="nom">Nombre</label>
                                <input class="form-control" type="text" id="nom" value="<%= (v==null)?"":v.getCabecera().getAdh().getNombres() %>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label" for="ape">Apellido</label>
                                <input class="form-control" type="text" id="ape" value="<%= (v==null)?"":v.getCabecera().getAdh().getApellidos() %>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Buscar</label>
                                <button id="modalbuscaradh" type="button" class="btn glyphicon btn-info glyphicon-search" 
                                data-toggle="modal" data-target=".modal" data-tipo="11" <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Cancelar</label>
                                <button type="button" class="btn glyphicon btn-danger glyphicon-erase" id="limpiaradh"
                                         <%= (v==null)?"":"disabled" %>>
                                </button>
                            </div>
                        </div>
                </div>
                <div class="row" style="background-color: blanchedalmond; border: 1px solid #eee;" id="mensajereco">
                    
                </div>
                <div class="row" style="background-color: blanchedalmond; border: 1px solid #eee;" id="resultadoadh">
                    
                </div>
                <div class="row" style="background-color: blanchedalmond; border: 1px solid #eee;" id="divobservacion">
                     <div class="form-group form-group-sm">
                            <div class="col-sm-1">
                                <label class="control-label" for="adh">Observación</label>
                            </div>
                           <div class="col-sm-6">
                               <textarea class="form-control" rows="3" id="comentario" <%= (v==null)?"":"readonly" %>><%= (v==null)?"":v.getCabecera().getObs() %></textarea>
                           </div>
                     </div>
                </div>
                <div class="row" style="background-color: blanchedalmond; border: 1px solid #eee;">
                        <div class="form-group form-group-sm">
                            
                            <div class="col-sm-1">
                                <label class="control-label">Prestador</label>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="presta">Código</label>
                                <input class="form-control" type="text" id="presta" 
                                       value="<%= (v==null)?"":v.getCabecera().getPresta().getIdPrt() %>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label" for="presta">Nombre</label>
                                <input class="form-control" type="text" id="prestadesc"
                                       value="<%= (v==null)?"":v.getCabecera().getPresta().getPrtDenominacion()%>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <!--
                            <div class="col-sm-2">
                                <label class="control-label" for="tipovisa">Tipo de Visa</label>
                                <select class="selectpicker" id="tipovisa">
                                    <option value="a">Ambulatorio</option>
                                    <option value="i">Internación</option>
                                  </select>
                            </div>
                            -->
                            <div class="col-sm-1">
                                <label class="control-label" for="">Buscar</label>
                                <button id="buscarprestador" type="button" class="btn glyphicon btn-info glyphicon-search" 
                                    data-toggle="modal" data-target=".modal" data-tipo="10"
                                    <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Cancelar</label>
                                <button type="button" class="btn glyphicon btn-danger glyphicon-erase" id="limpiarpresta"
                                        <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            
                        </div>
                    
                </div>
                <div class="row" style="background-color: blanchedalmond; border: 1px solid #eee;">
                        
                        <div class="form-group form-group-sm">
                            <div class="col-sm-1">
                                <label class="control-label">Médico</label>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="codmed">Código</label>
                                <input class="form-control" type="text" id="codmed"
                                       value="<%= ( v == null || v.getCabecera().getMedico()==null)?"":v.getCabecera().getMedico().getCodigo() %>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label" for="desmed">Nombre</label>
                                <input class="form-control" type="text" id="desmed"
                                       value="<%= ( v == null || v.getCabecera().getMedico()==null)?"":v.getCabecera().getMedico().getNombre() %>" <%= (v==null)?"":"readonly" %>>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Buscar</label>
                                <button id="buscarmedico" type="button" class="btn glyphicon btn-info glyphicon-search" 
                                    data-toggle="modal" data-target=".modal" data-tipo="14"
                                    <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Nuevo Med</label>
                                <button type="button" class="btn glyphicon btn-info glyphicon-plus" 
                                        id="nuevomedico"
                                    <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Cancelar</label>
                                <button type="button" class="btn glyphicon btn-danger glyphicon-erase" id="limpiarmedico"
                                        <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            
                            <div class="col-sm-3">
                                <label class="control-label" for="">Agregar Prestaciones</label>
                                <button type="button" class="btn glyphicon btn-success glyphicon-log-in" id="agregarvisa" name="agregarvisa"
                                        <%= (v==null)?"":"disabled" %>></button>
                            </div>
                        </div>
                    
                </div>
                <div id="nuevomedicodiv" class="row" style="background-color: blanchedalmond; border: 1px solid #eee;">                            
                    
                </div>
                <div class="row" id="detallevisa" 
                     style="background-color: blanchedalmond; border: 1px solid #eee;display: <%= (v==null)?"none":"block" %>;">

                        <div class="form-group form-group-sm">
                            <div class="col-sm-1">
                                <label class="control-label">Prestaciones</label>
                            </div>
                            <div class="col-sm-1 col-sm-offset-1">
                                <label class="control-label" for="nome">Nro.Nome</label>
                                <input class="form-control" type="text" id="nome">
                            </div>
                            <div class="col-sm-3">
                                <label class="control-label" for="nomedesc">Nomeclador</label>
                                <input class="form-control" type="text" id="nomedesc">
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Buscar</label>
                                <button id="modalbuscarnome" type="button" class="btn btn-info glyphicon glyphicon-search" 
                                    data-toggle="modal" data-target=".modal" data-tipo="12"
                                    <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="cant">Cantidad</label>
                                <input class="form-control" type="text" id="cant" value="1">
                            </div>
                            <div class="col-sm-3">
                                <!--
                                <label class="control-label" for="obs">Observación</label>
                                <input class="form-control" type="text" id="obs">
                                -->
                            </div>
                            <div class="col-sm-1">
                                <label class="control-label" for="">Agregar</label>
                                <button type="button" class="btn glyphicon btn-success glyphicon-log-in" 
                                        id="agregardetalle" name="agregardetalle"
                                        <%= (v==null)?"":"disabled" %>></button>
                            </div>
                            
                        </div>
                    
                </div>
                <div class="row" id="detallevisatabla" 
                     style="background-color: blanchedalmond; border: 1px solid #eee;">
                    <table>
                        <%
                        if(v!=null)
                        {
                            
                            out.print(VisaBean.printDetalleVisaFinal(String.valueOf(v.getCabecera().getNrovis())));
                        }
                        %>
                    </table>
                </div>      
                <div class="row" id="imprimirvisa" style="background-color: blanchedalmond; border: 1px solid #eee;height: 140px;">
                            <div class="col-sm-1 col-sm-offset-4">
                                <label class="control-label" for="">Cancelar</label>
                                <button id="cancelarvisa" type="button" class="btn btn-danger glyphicon glyphicon-remove btn-lg">
                                </button>
                            </div>
                            <div class="col-sm-1 col-sm-offset-2">
                                <label class="control-label" for=""><%= (v!=null)?"Imprimir":"Guardar" %></label>
                                <button id="imprimirvisabt" type="button" class="btn btn-info glyphicon <%= (v!=null)?"glyphicon-print":"glyphicon-floppy-disk" %> btn-lg"></button>
                            </div>
                </div>                
            </form>
        </div>

        <!-- Taken from Bootstrap's documentation -->
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="pull-right btn btn-default" data-dismiss="modal"
                                id="aceptarmodal1" style="display: block;">Aceptar</button>
                        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        -->
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body" id="modalbody">
                        <p>&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="pull-right btn btn-default" data-dismiss="modal"
                                id="aceptarmodal2" style="display: block;">Aceptar</button>
                        <button type="button" class="pull-right btn btn-info" data-dismiss="modal" id="imprimirPdf" name="imprimirPdf"
                                style="display: none;">
                            Imprimir
                        </button>
                       
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- 
        <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>

        -->

    </body>
    <script>

        var $modal = $('.modal');
        // Show loader & then get content when modal is shown
        $modal.on('show.bs.modal', function (e) {
            var tipo = $(e.relatedTarget).data('tipo');
            var codpresta = $("#presta").val();
            var despresta = $("#prestadesc").val();
            var adh = $("#adh").val();
            var ci = $("#ci").val();
            var nom = document.getElementById("nom").value;//$("#nom").val();
            var ape = document.getElementById("ape").value;//$("#ape").val();
            var nome = $("#nome").val();
            var nomedesc = $("#nomedesc").val();
            var cant = $("#cant").val();
            var obs = $("#obs").val();
            var codmed = $("#codmed").val();
            var desmed = $("#desmed").val();
            var numvisa = $("#numvisa").val();
            
            
            var url = "";
            if(tipo==10) 
                url = encodeURI('./visa?operacion=buscarpresta&codpresta=' + codpresta+'&despresta='+despresta);
            else if(tipo==11)
                url = encodeURI('./visa?operacion=buscaradh&adh='+adh+'&ci='+ci+'&nom='+nom+'&ape='+ape);
            else if(tipo==12)
                url = encodeURI('./visa?operacion=buscarnome&nome='+nome+'&nomedesc='+nomedesc);
            else if(tipo==13)
                url = encodeURI('./visa?operacion=agregardet&nome='+nome+'&cant='+cant+'&obs='+obs);
            else if(tipo==14) 
                url = encodeURI('./visa?operacion=buscarmed&codmed=' + codmed+"&desmed="+desmed);
            else if(tipo==16) {
                
            }
         
            $(this)
            .addClass('modal-scrollfix')
            .find('.modal-body')
            .html('loading...')
            .load(url, function () {
                $modal.removeClass('modal-scrollfix').modal('handleUpdate');
            });
        });
        
        $("#ci").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#modalbuscaradh').trigger('click');
            }
        });
        $("#adh").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#modalbuscaradh').trigger('click');
            }
        });
        $("#nome").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#modalbuscaradh').trigger('click');
            }
        });
        $("#ape").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#modalbuscaradh').trigger('click');
            }
        });
        
        $("#presta").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#buscarprestador').trigger('click');
            }
        });
        $("#prestadesc").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#buscarprestador').trigger('click');
            }
        });
        $("#codmed").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#buscarmedico').trigger('click');
            }
        });
        $("#desmed").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#buscarmedico').trigger('click');
            }
        });
        $("#nome").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#modalbuscarnome').trigger('click');
            }
        });
        $("#nomedesc").keypress(function( event ) {
            if ( event.which == 13 ) {
               $('#modalbuscarnome').trigger('click');
            }
        });
        
        $("#nuevomedico").click(
           function (){

                var codpresta = $("#presta").val();
                var despresta = $("#prestadesc").val();
                 
                var data = 'codpresta='+codpresta+'&despresta='+despresta;
                if(codpresta == "" || despresta == ""){
                    alert("debe elegir un prestador antes");
                    $("#prestadesc").focus();
                    return;
                }
            
                $.ajax({
                    type: "POST",
                    url: "test.jsp",
                    data: data,
                    beforeSend: function() {
                        $('#nuevomedicodiv').html("<img src='./images/loading.gif' />");
                    },
                    success: function (result) {
                        if (typeof (result) !== 'undefined') {
                            $( '#nuevomedicodiv').html("");
                            $( '#nuevomedicodiv').html(result);

                        }
                    },
                    error: function (error) {
                        console.log("error" + error);
                    }
                });
            }
        );
    
                
        $("#imprimirvisabt").click(
            function (){
                var cantidad_filas = $('#cantidad_filas').val() ;
                var comentario = $('#comentario').val() ;
                
                if(cantidad_filas >0){
                    if(comentario.length>3){
                        var numvisa = $('#numvisa').val() ;
                        var url = encodeURI('./visaImpresion<%= (v==null)?"":"Copia" %>.jsp?numvisa='+numvisa+"&comentario="+comentario);
                        $.ajax({
                                type: "POST",
                                url: url,
                                beforeSend: function() {
                                    $('#principal').html("<img src='./images/loading.gif' />");
                                },
                                success: function (result) {
                                    if (typeof (result) !== 'undefined') {
                                        $( '#principal').html("");
                                        $( '#principal').html(result);
                                    }
                                },
                                error: function (error) {
                                    console.log("error" + error);
                                }
                            });
                        }else{
                            alert("Debe cargar un comentario");
                        }
                }else{
                    alert("No puede guardar visa sin detalle");
                }
            }
        );
                
                
        $("#agregardetalle").click(
            function (){
                
                var numvisa = $('#numvisa').val() ;
                var presta = $('#presta').val();
                var cant = $('#cant').val();
                var obs = $('#obs').val();
                var nome = $('#nome').val();
                var plan = $('#g9').val();
                var contrato = $('#g2').val();
                var adh = $('#adh').val();
                var incioContrato = $('#g3').val();
                var cantdiascontrato = $('#g10').val();
               
                var data = "operacion=guardardetalle&numvisa="+numvisa+"&presta="+presta+"&cant="+cant+
                           "&obs="+obs+"&nome="+nome+"&plan="+plan+"&contrato="+contrato+"&adh="+adh+
                           "&incioContrato="+incioContrato+"&cantdiascontrato="+cantdiascontrato;
                $.ajax({
                    type: "POST",
                    url: "visa",
                    data: data,
                    //beforeSend: function() {
                    //    $('#detallevisatabla').html("<img src='./images/loading.gif' />");
                    //},
                    success: function (result) {
                        if (typeof (result) !== 'undefined') {
                            $( '#detallevisatabla').html("");
                            $( '#detallevisatabla').html(result);
                            $('#numvisa').val();
                            
                            $('#nome').val("");
                            $('#nomedesc').val("");
                            $('#cant').val("1");
                            $('#obs').val("");
                        }
                    },
                    error: function (error) {
                        console.log("error" + error);
                    }
                });
            }
        );

        $("#imprimirPdf").click(
            function (){ 
                $("#aceptarmodal1").hide();
                $("#aceptarmodal2").hide();
                printDiv() ;
            }
        );
        function printDiv() 
        {
            $("#aceptarmodal1").show();
            $("#aceptarmodal2").show();
            $("#imprimirPdf").hide();
            $('#principal').load('./visa.jsp');
            
            var divToPrint=document.getElementById('modalbody');
            var newWin=window.open('','Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
            newWin.document.close();
        }
        
        $("#buscarvisa").click(
            function (){
                
                var numvisa = $('#numvisa').val() ;
                $('#principal').load('./visa.jsp?'+"numvisa="+numvisa);
            }
        );

        $("#agregarvisa").click(
            function (){
                
                var ci = $('#ci').val() ;
                var idadh = $('#adh').val();
                var contplan = $('#g8').val();
                var contrato = $('#g2').val();
                var presta = $('#presta').val();
                var codplan = $('#g9').val();
                var codmed = $('#codmed').val();
                var intenacion = $('#tipovisa').val();
               
                if(presta==""){
                    alert("Debe elegir un prestador");
                    $('#despresta').focus();
                    return;
                    
                }
                    
                var data = "operacion=guardarvisa&ci="+ci+"&idadh="+idadh+"&contplan="+contplan+
                           "&contrato="+contrato+"&presta="+presta+"&codplan="+codplan+"&intenacion="+intenacion+"&codmed="+codmed;
                $.ajax({
                    type: "POST",
                    url: "visa",
                    data: data,
                    //beforeSend: function() {
                    //    $('#cabecera').html("<img src='./images/loading.gif' />");
                    //},
                    success: function (result) {
                        if (typeof (result) !== 'undefined') {
                            $( '#cabecera').html("");
                            $( '#cabecera').html(result);
                            $( "#detallevisa" ).show();
                            $( "#detallevisatabla" ).show();
                            $("#presta").prop("readonly", true);
                            $("#prestadesc").prop("readonly", true);
                            $("#codmed").prop("readonly", true);
                            $("#desmed").prop("readonly", true);
                            $("#buscarprestador").prop("disabled",true);
                            //$("#limpiarpresta").prop("disabled",true);
                            $("#buscarmedico").prop("disabled",true);
                            $("#agregarvisa").prop("disabled",true);
                            //$("#limpiarmedico").prop("disabled",true);
                            $("#tipovisa").prop("disabled",true);
                            
                            
                        }
                    },
                    error: function (error) {
                        console.log("error" + error);
                    }
                });
            }
        );

        function elegirPrestador(presta,prestadesc){
            //alert(presta);
            document.getElementById("presta").value = presta;
            document.getElementById("prestadesc").value = prestadesc;
        }
        function elegirMedico(cod,nom){
            //alert(presta);
            document.getElementById("codmed").value = cod;
            document.getElementById("desmed").value = nom;
        }
        
        function elegirAdh(contrato,cedula){
             $.ajax({url: './visa?operacion=buscaradhcontrato&contrato='+contrato+'&cedula='+cedula, 
                beforeSend: function() {
                    $('#resultadoadh').html("<img src='./images/loading.gif' />");
                },
                 success: function(result){
                    $("#resultadoadh").html(result);
                }});
                //alert(contrato+" "+cedula);             
        }
        function buscarCuotas(){
                var contrato = $('#g2').val() ;
                //alert(contrato );
                $.ajax({url: './visa?operacion=buscarcuotas&contrato='+contrato, 
                beforeSend: function() {
                    $("#detallecuotas").show();
                    $('#detallecuotas').html("<img src='./images/loading.gif' />");
                },
                 success: function(result){
                    $("#detallecuotas").show();
                    $("#detallecuotas").html(result);
                }});
        }
        
        function ocultarCuotas(){
            $("#detallecuotas").hide();
            
        }
        
        /*reconocimiento*/
        function elegirAdhGrupo(adh, ape, nom, nroci){
            $("#adh").val(adh);
            $("#adh").prop("readonly", true);
            $("#nom").val(nom);
            $("#nom").prop("readonly", true);
            $("#ci").val(nroci);
            $("#ci").prop("readonly", true);
            $("#ape").val(ape);
            $("#ape").prop("readonly", true);

            var data = "operacion=buscarreco&adh="+adh;
            $.ajax({
                type: "POST",
                url: "visa",
                data: data,
                //beforeSend: function() {
                //    $('#detallevisatabla').html("<img src='./images/loading.gif' />");
                //},
                success: function (result) {
                    if (typeof (result) !== 'undefined') {
                        $( '#mensajereco').html("");
                        $( '#mensajereco').html(result);
                    }
                },
                error: function (error) {
                    console.log("error" + error);
                }
            });
            
                
        }
        function elegirNome(nome, nomedesc){
            document.getElementById("nome").value = nome;
            document.getElementById("nomedesc").value = nomedesc;
        }
        function eliminardetalle(numvisa, nome){
            var data = "operacion=elimiardetalle&numvisa="+numvisa+"&nome="+nome;
            $.ajax({
                type: "POST",
                url: "visa",
                data: data,
                //beforeSend: function() {
                //    $('#detallevisatabla').html("<img src='./images/loading.gif' />");
                //},
                success: function (result) {
                    if (typeof (result) !== 'undefined') {
                        $( '#detallevisatabla').html("");
                        $( '#detallevisatabla').html(result);

                        $('#nome').val("");
                        $('#nomedesc').val("");
                        $('#cant').val("1");
                        $('#obs').val("");
                    }
                },
                error: function (error) {
                    console.log("error" + error);
                }
            });
        }
        
        $("#limpiaradh").click(
            function (){
                var cantidad_filas = $('#cantidad_filas').val() ;
                if(cantidad_filas >0){
                    alert("Existen datos en el detalle, no puede cambiar adherente.");
                    return;
                }
                else{

                }
                $('#adh').val("");
                $('#adh').removeAttr('readonly');
                $('#ci').val("");
                $('#ci').removeAttr('readonly');
                $('#nom').val("");
                $('#nom').removeAttr('readonly');
                $('#ape').val("");
                $('#ape').removeAttr('readonly');
                $('#resultadoadh').html("");
        });
        $("#limpiarpresta").click(function(){
            var cantidad_filas = $('#cantidad_filas').val() ;
            if(cantidad_filas >0){
                alert("Existen datos en el detalle, no puede cambiar prestador.");
                return;
            }
            
            $('#presta').val("");
            $('#presta').removeAttr('readonly');
            $('#prestadesc').val("");
            $('#prestadesc').removeAttr('readonly');
            $("#agregarvisa").prop("disabled",false);
            $("#buscarprestador").prop("disabled",false);
            $("#buscarmedico").prop("disabled",false);
            $( "#detallevisa" ).hide();
            $( "#detallevisatabla" ).hide();
            
            
        });
        $("#limpiarmedico").click(function(){
            var cantidad_filas = $('#cantidad_filas').val() ;
            if(cantidad_filas >0){
                alert("Existen datos en el detalle, no puede cambiar prestador.");
                return;
            }            
            $('#codmed').val("");
            $('#codmed').removeAttr('readonly');
            $('#desmed').val("");
            $('#desmed').removeAttr('readonly');
            $("#buscarprestador").prop("disabled",false);
            $("#buscarmedico").prop("disabled",false);
            $( "#detallevisa" ).hide();
            $( "#detallevisatabla" ).hide();            
        });
        $("#limpiarvisa").click(function(){
            $("#principal").load("./visa.jsp");
        });
        $("#cancelarvisa").click(function(){
            $("#principal").load("./visa.jsp");
        });
        <%
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        %>
    </script>

</html>

