<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");

    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
        <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
        <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
        <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
        <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
        <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
        <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
            <div class="container">
                <form id="formulario" class="form-horizontal">
                    <fieldset>

                        <!-- Form Name -->
                        <legend style="color:rgb(83,156,239);font-size:20pt;font-family:Trebuchet MS">Actividades Barcaza</legend>


                        <!-- Text input-->
                        <!--<div class="input-group">-->
                            <label class="col-md-2 control-label" for="descripcion:string">Descripcion</label>  
                            <div class="col-md-4">
                                <input id="descripcion:string" name="descripcion:string" type="text" placeholder="" class="form-control input-sm" required>

                            </div>
                            
                            <!--<label class="col-md-2 control-label" for="comentarios:string">Comentarios</label>  
                            <div class="col-md-3">
                                <input id="comentarios:string" name="comentarios:string" type="text" placeholder="" class="form-control input-sm">

                            </div>-->
                        <!--</div>-->

                        <!-- Button (Double) -->
                        <div class="form-group">
                            <div class="col-md-8">
                                <button id="button1id" onclick="javascript:reload()"  name="button1id" type="button" class="btn btn-success">Aceptar</button>
                                <button id="button2id" name="button2id" type="button" class="btn btn-danger">Cancelar</button>
                            </div>
                        </div>

                    </fieldset>
                </form>
                <hr>
                <div id="tabla"  class="row col-md-offset-2 custyle">
                    <table id="example" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Descripcion</th>
                                <th  align="center">Accion</th>
                            </tr>
                        </thead>
                        <%  
                            List<String[]> lista = DBTrasactions.retList("select identificador, descripcion from actividades_barcaza");
                            for (int idx = 0; idx < lista.size(); idx++) {
                                String elem[] = lista.get(idx);
                                out.print(" <tr>");
                                for (int id = 0; id < elem.length; id++) {
                                    Object e = elem[id];
                                    out.print("<td>" + e + "</td>");
                                }
                                
                                String data = String.join("#",Arrays.asList(elem));
                                String botonBorrar = "<td class=\"text-center\">";
                                botonBorrar += "<button href=\"#\" onclick=\"javascript:borrarFila("+elem[0]+")\" class=\"btn btn-danger btn-xs buttoncancel\"></button>";
                                botonBorrar += "<button href=\"#\" onclick=\"javascript:modificar("+elem[0]+",'"+data+"')\" class=\"btn btn-info btn-xs buttonedit\"></button>";
                                botonBorrar += "</td>";
                                out.print(botonBorrar);
                                out.print("</tr>");

                            }

                        %>

                    </table>
                </div>
            </div>
        
    </body>
    <script>

        function InitOverviewDataTable()
        {

            var table = $('#example').dataTable(
                    {
                        "language": {
                        "url": "./inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 5
                    });
        }


        $(document).ready(function () {
            InitOverviewDataTable();

        });
        function borrarFila(id) {
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "deleteData",
                data: {
                    id: id,
                    class: "actividades_barcaza"
                },
                success: function (result) {
                    alert("Registro eliminado exitosamente.");

                    $.ajax({url: 'mantenimiento/mantenimientoActividadesBarcaza.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }

        function modificar(id, data) {
            var res = data.split("#");
            var c= 1;
            var c= 1;
            $('input[type=text], textarea, select, input[type=password]' ).each(function () {
                 $(this).val(res[c]) ;
                 c++;
            });
            $("#button1id").attr("onclick","updateReload("+res[0]+")");

        }

        
        function updateReload(id) {
            var text = "";
            var error = false;
            $('input[type=text], select, textarea, input[type=hidden]').each(function () {
                var valor = "null";
                if ($(this).val()){
                    valor = $(this).val() ;
                    
                }else{
                    var requiered = $(this).prop('required');
                    
                    if(requiered){
                        $(this).focus();
                        error = true;
                        return false;
                    }
                }
                text = text + $(this).attr('id') + ":" + valor+ ";";

            });          
            /*hasta  acá*/
            if(error){
                alert("Error al cargar campos requeridos")
                return false;
            }

            var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "processUpdate",
                data: {
                    values: encoded,
                    class: "actividades_barcaza",
                    id: id
                },
                success: function (result) {
                    alert("Registro agregado exitosamente.");

                    $.ajax({url: 'mantenimiento/mantenimientoActividadesBarcaza.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error:'+result);
                }
            });
        }
        
        
        function reload() {
             var text = "";
            var error = false;
            $('input[type=text]').each(function () {
                var valor = "null";
                if ($(this).val()){
                    valor = $(this).val() ;
                    
                }else{
                    var requiered = $(this).prop('required');
                    
                    if(requiered){
                        $(this).focus();
                        error = true;
                        return false;
                    }
                }
                
                text = text + $(this).attr('id') + ":" + valor+ ";";
            })
            /*desde acá*/
            $("select").each(function() { 
                   text = text + $(this).attr('id') + ":" + $(this).val() + ";";
            });
            $("textarea").each(function() { 
                   text = text + $(this).attr('id') + ":" + $(this).val() + ";";
            });
            $('input[type=hidden]').each(function () {
                text = text + $(this).attr('id') + ":" + $(this).val() + ";";
            });            
            /*hasta  acá*/
            if(error)
                return false;
            var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "processServlet",
                data: {
                    values: encoded,
                    class: "actividades_barcaza",
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente.");

                    $.ajax({url: 'mantenimiento/mantenimientoActividadesBarcaza.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }
    </script>

</html>

