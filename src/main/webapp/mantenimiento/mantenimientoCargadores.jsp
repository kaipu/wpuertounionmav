<%@page import="py.com.puertounion.utiles.DBTrasactions"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        HttpSession ss  = request.getSession();
        String user = ss.getAttribute("userlogon").toString();
        boolean esAdmin = DBTrasactions.retBoolean("SELECT * FROM cargadores where usuario ='"+user+"' and  admin = 1");
        
    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
                <link rel="stylesheet" href="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css">
                <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.min.css">
                <link href="nscript/css?family=Raleway:400,500,500i,700,800i" rel="stylesheet">
                <script src="inet/ajax.googleapis.com_ajax_libs_jquery_3.3.1_jquery.min.js"></script>
                <script src="inet/cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js"></script>
                <script src="inet/maxcdn.bootstrapcdn.com_bootstrap_4.1.3_js_bootstrap.min.js"></script>
                <!-- Include Bootstrap Datepicker -->
                <link rel="stylesheet" href="inet/cdnjs.cloudflare.com_ajax_libs_bootstrap-datepicker_1.8.0_css_bootstrap-datepicker.min.css" />
                <script src="inet/cdnjs.cloudflare.com_ajax_libs_bootstrap-datepicker_1.8.0_js_bootstrap-datepicker.min.js"></script>

                <link rel="stylesheet" type="text/css" href="inet/cdn.datatables.net_1.10.19_css_jquery.dataTables.css">
                <script type="text/javascript" charset="utf8" src="inet/cdn.datatables.net_1.10.19_js_jquery.dataTables.js"></script>
                <link href="inet/datepicker.css" rel="stylesheet" type="text/css" />
                <script src="inet/datepicker.js"></script>

    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        label{margin-left: 20px;}
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
        #datepicker{width:180px; margin: 0 20px 20px 20px;}
        #datepicker > span:hover{cursor: pointer;}
    </style>
    <body>
        <div class="container">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend style="color:rgb(83,156,239);font-size:20pt;font-family:Trebuchet MS">Operadores Administrativos</legend>

                    
                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="textinput">C.I. Nro.</label>  
                        <div class="col-md-3">
                            <input id="personas:nro_documento:string" name="textinput" class="form-control" placeholder="" type="text" required>

                        </div>
                        
                    </div>
                    

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="textinput">Nombre</label>  
                        <div class="col-md-3">
                            <input id="personas:denominacion:string" name="textinput" type="text" placeholder="" class="form-control input-sm" required>

                        </div>
                        <label class="col-md-2 control-label" for="textinput">Fecha Nac.</label>  
                        <div id="datepicker" class="col-md-3 input-group date" data-date-format="yyyy-mm-dd">
                            <input id="personas:fecha_nacimiento:string" class="form-control" type="text" readonly />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="textinput">RUC</label>  
                        <div class="col-md-3">
                            <input id="personas:ruc:string" name="textinput" type="text" placeholder="" class="form-control input-sm">
                        </div>
                        <label class="col-md-2 control-label" for="textinput">Dirección</label>  
                        <div class="col-md-3">
                            <input id="personas:direccion:string" name="textinput" type="text" placeholder="" class="form-control input-sm">

                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="textinput">Telefonos</label>  
                        <div class="col-md-3">
                            <input id="personas:telefonos:string" name="textinput" type="text" placeholder="" class="form-control input-sm">
                            <input id="personas:tipo_documento:string" value="CI" type="hidden">
                        </div>
                        <label class="col-md-2 control-label" for="textinput">Usuario</label>  
                        <div class="col-md-3">
                            <input id="cargadores:usuario:string" name="textinput" type="text" placeholder="" class="form-control input-sm">

                        </div>
                    </div>
                    <!-- Password input-->
                    <div class="input-group">
                      <label class="col-md-2 control-label" for="passwordinput">Contraseña</label>
                      <div class="col-md-3">
                        <input id="cargadores:contrasenha:string" name="passwordinput" type="password" placeholder="" class="form-control input-md">
                      </div>
                        <label class="col-md-2 control-label" for="textarea">Comentarios</label>
                        <div class="col-md-3">                     
                            <textarea class="form-control" id="cargadores:comentarios:string" name="textarea"></textarea>
                        </div>
                        <input id="cargadores:id_persona:int" value="LAST_INSERT_ID()" type="hidden">
                    </div>
           
                    <div class="input-group">
                      <label class="col-md-2 control-label" for="admin">Tipo de Usuario</label>
                      <div class="col-md-3">
                              <select class="form-control" id="cargadores:admin:int">
                                    <option value='0'>Usuario de Balanza</option>
                                    <option value='1'>Usuario Administrador</option>
                                  
                              </select>  
                      </div>
                        
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>
                        <div class="col-md-8">
                            <%
                                if(esAdmin)
                                    out.print("<button id=\"button1id\" onclick=\"javascript:reload()\"  name=\"button1id\" type=\"button\" class=\"btn btn-success\">Aceptar</button>");
                            %>
                            
                            <button id="button2id" name="button2id" type="button" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>

                </fieldset>
            </form>
            <hr>
            <div id="tabla"  class="row col-md-11 col-md-offset-1 custyle">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nro. Documento</th>
                            <th>Nombre</th>
                            <th>Fec.Nac.</th>
                            <th>RUC</th>
                            <th>Direccion</th>
                            <th>Telefonos</th>
                            <th>Usuario</th>
                            <th>Cont.</th>
                            <th>Comentarios</th>
                            <th>Administrador</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <%                        
                        List<String[]> lista = DBTrasactions.retList("select  b.identificador, b.nro_documento, "
                                                                    + "b.denominacion, b.fecha_nacimiento,"
                                                                    + "b.ruc, b.direccion,b.telefonos, "
                                                                    + "a.usuario, '', a.comentarios ,  coalesce(a.admin,0),  a.identificador, coalesce(a.contrasenha,'')  "+
                                                                     "from    cargadores a, personas b "+
                                                                     "where   a.id_persona = b.identificador ");
                        for (int idx = 0; idx < lista.size(); idx++) {
                            String elem[] = lista.get(idx);
                            out.print(" <tr>");
                            for (int id = 0; id < elem.length-2; id++) {
                                Object e = elem[id];
                                if(elem.length-3 == id){
                                    String val = (Integer.parseInt(e.toString())==1)?"SI":"NO";
                                    out.print("<td>" + val + "</td>");
                                }else{
                                    out.print("<td>" + e + "</td>");
                                }
                                

                            }
                             String data = String.join("#",Arrays.asList(elem));
                                String botonBorrar = "<td class=\"text-center\">";
                                if(esAdmin){
                                    botonBorrar += "<button href=\"#\" onclick=\"javascript:borrarFila("+elem[10]+")\" class=\"btn btn-danger btn-xs buttoncancel\"></button>";
                                    botonBorrar += "<button href=\"#\" onclick=\"javascript:modificar("+elem[0]+",'"+data+"',"+elem[11]+",'"+elem[10]+"')\" class=\"btn btn-info btn-xs buttonedit\"></button>";
                                }
                                botonBorrar += "</td>";
                                out.print(botonBorrar);
                                out.print("</tr>");

                        }

                    %>

                </table>
            </div>

        </div>

    </body>
    <script>
                    $(function () {
          $("#datepicker").datepicker({ 
                autoclose: true, 
                todayHighlight: true
          }).datepicker('update', new Date());
        });
        
        function InitOverviewDataTable()
        {

            var table = $('#example').dataTable(
                    {
                        "language": {
                        "url": "inet/Spanish.json"}, 
                        "bPaginate": true,
                        "bJQueryUI": true, // ThemeRoller-stöd
                        "bLengthChange": false,
                        "bFilter": true,
                        "bSort": false,
                        "bInfo": true,
                        "bAutoWidth": true,
                        "bProcessing": false,
                        "iDisplayLength": 5
                    });
        }
        
        $(document).ready(function () {
            InitOverviewDataTable();
            //$('#personas:fecha_nacimiento:string').datepicker();
        });
        function modificar(id, data, idOperarios, pass) {
            var res = data.split("#");
            var c= 1;
            var passs= "";
            $('input[type=text], textarea, select, input[type=password]' ).each(function () {
                //alert($(this)+" "+res[c]+" "+c);
                $(this).val(res[c]) ;
                 c++;
            })
            
            $("#button1id").attr("onclick","updateReload("+res[0]+","+idOperarios+",'"+passs+"',"+res[10]+")");

        }

        function updateReload(id,idOperarios,pass,admin) {
             var text = "";
            var error = false;
            $('input[type=text], select, textarea, input[type=hidden]').each(function () {
                var valor = "null";
                if ($(this).val()){
                    valor = $(this).val() ;
                    
                }else{
                    var requiered = $(this).prop('required');
                    
                    if(requiered){
                        $(this).focus();
                        error = true;
                        return false;
                    }
                }
                var id = $(this).attr('id'); 
                if(id.indexOf("personas") >=0 ){
                    text = text + id.substring(id.indexOf("personas")+9,id.length)+ ":" + valor+ ";";
                }
            });   
            $('input[type=password]').each(function () {
                pass = $(this).val() ;
            });   
            $('select').each(function () {
                admin = $(this).val() ;
            });         
            /*hasta  acá*/
            if(error)
                return false;
            var encoded = encodeURIComponent(text);
            var sqlencoded = encodeURIComponent("update cargadores set contrasenha = '"+pass+"', admin = "+admin+" where identificador = "+idOperarios+" ")
               //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "processUpdate",
                data: {
                    values: encoded,
                    class: "personas",
                    sql:sqlencoded ,
                    id: id
                },
                success: function (result) {
                    alert("Registro agregado exitosamente.");

                    $.ajax({url: 'mantenimiento/mantenimientoCargadores.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error:'+result);
                }
            });
        }

        function borrarFila(id){
            //var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "deleteData",
                data: {
                    id: id,
                    class: "cargadores"
                },
                success: function (result) {
                    alert("Registro eliminado exitosamente.");

                    $.ajax({url: 'mantenimiento/mantenimientoCargadores.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }

        function reload() {
            var text = "";
            var error = false;
            $('input[type=text]').each(function () {
                var valor = "null";
                if ($(this).val()){
                    valor = $(this).val() ;
                    
                }else{
                    var requiered = $(this).prop('required');
                    
                    if(requiered){
                        $(this).focus();
                        error = true;
                        return false;
                    }
                }
                
                text = text + $(this).attr('id') + ":" + valor+ ";";
            })
            $('input[type=password]').each(function () {
                var valor = "null";
                if ($(this).val()){
                    valor = $(this).val() ;
                    
                }else{
                    var requiered = $(this).prop('required');
                    
                    if(requiered){
                        $(this).focus();
                        error = true;
                        return false;
                    }
                }
                
                text = text + $(this).attr('id') + ":" + valor+ ";";
            })
            $("select").each(function() { 
                   text = text + $(this).attr('id') + ":" + $(this).val() + ";";
            });
            $("textarea").each(function() { 
                   text = text + $(this).attr('id') + ":" + $(this).val() + ";";
            });
            $('input[type=hidden]').each(function () {
                text = text + $(this).attr('id') + ":" + $(this).val() + ";";
            });
            
            if(error)
                return false;
             
            //alert(text);
            //alert(text);
            //alert(table);
            //alert(text);
            var encoded = encodeURIComponent(text);
            //$("#result").html("cargando");
            $.ajax({
                type: "POST",
                url: "processOperarios",
                data: {
                    values: encoded,
                    class: "personas,cargadores",
                    consulta: ""
                },
                success: function (result) {
                    alert("Registro agregado exitosamente.");

                    $.ajax({url: 'mantenimiento/mantenimientoCargadores.jsp',
                        beforeSend: function () {
                            $("#div1").show();
                            $('#div1').html("<img src='./images/loading.gif' />");
                        },
                        success: function (result) {
                            $("#div1").show();
                            $("#div1").html(result);
                        }});


                },
                error: function (result) {
                    alert('error');
                }
            });
        }

    </script>

</html>

