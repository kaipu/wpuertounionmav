<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");

    %>
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <style>
        /* Apply & remove to fix dynamic content scroll issues on iOS 9.0 */
        .modal-scrollfix.modal-scrollfix {
            overflow-y: hidden;
        }
        .modal-dialog{
            width: 80%;
            margin: auto;
        }
        .buttoncancel {
            background-image: url(images/trash-alt-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }
        .buttonedit {
            background-image: url(images/edit-solid.svg);
            background-repeat: no-repeat;
            background-position: 50% 50%;
            /* put the height and width of your image here */
            height: 20px;
            width: 40px;
            border: none;
        }

        button span {
            display: none;
        }
    </style>
    <body>
        <div class="container">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Tipos de Movimientos</legend>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="textinput">Descripcion</label>  
                        <div class="col-md-4">
                            <input id="textinput" name="textinput" type="text" placeholder="" class="form-control input-sm" required>

                        </div>
                        <label class="col-md-2 control-label" for="selectbasic">Operacion</label>
                        <div class="col-md-4">
                            <select id="selectbasic" name="selectbasic" class="form-control">
                                <option value="1">Suma a existencia</option>
                                <option value="2">Resta a existencia</option>
                            </select>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="input-group">
                        <label class="col-md-2 control-label" for="textinput">Comentarios</label>  
                        <div class="col-md-4">
                            <input id="textinput" name="textinput" type="text" placeholder="" class="form-control input-sm">

                        </div>
                    </div>

                    <!-- Button (Double) -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>
                        <div class="col-md-8">
                            <button id="button1id" name="button1id" class="btn btn-success">Aceptar</button>
                            <button id="button2id" name="button2id" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>

                </fieldset>
            </form>

        </div>

    </body>
    <script>


    </script>

</html>

