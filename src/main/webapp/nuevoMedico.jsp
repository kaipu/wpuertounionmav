<%-- 
    Document   : visa
    Created on : 09/06/2017, 08:14:53 AM
    Author     : ojo
--%>

<%@page import="java.util.List"%>
<%@page import="py.com.fleming.visas.pojos.Especialidad"%>
<%@page import="py.com.fleming.visas.beans.VisaBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
  
    <head>
        <title>Puerto Uni&oacute;n</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
    </head>

    <body>
        <div class="container">
            <form class="form-horizontal">
                <div  id="cabecera">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="ncodmed">Código</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ncodmed" value="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="nnommed">Nombre</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="nnommed">
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="nespecmed">Especialidad</label>
                                <div class="col-sm-6">
                                    <select id="nespecmed" data-live-search="true">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="ndep">Departamento</label>
                                <div class="col-sm-6">
                                    <select id="ndep" data-live-search="true">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>       
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-4 control-label" for="nzon">Zona</label>
                                <div class="col-sm-6">
                                    <select id="nzon" data-live-search="true">

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>                                    
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                 <label class="col-sm-4 control-label" for="ndirmed">Dirección</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ndirmed">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                 <label class="col-sm-4 control-label" for="ntelmed">Teléfono</label>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ntelmed">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group form-group-sm">
                                 <label class="col-sm-4 control-label">Prestador</label>
                                <div class="col-sm-2">
                                    <input class="form-control" type="text" id="ncodpresta">
                                </div>
                                <div class="col-sm-6">
                                    <input class="form-control" type="text" id="ndespresta">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </form>
        </div>


    </body>
</html>

